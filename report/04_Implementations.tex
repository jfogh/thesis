The implementations, raw data files, plots and the files used to create the report can be found at:
\\\\\url{https://bitbucket.org/jfogh/thesis/src}\\\\
All implementations are made in Java and compiled using Javac version $1.7.0\_17$.\\\\
The following command can be used from the \textbf{code/Thesis/src} directory to compile all relevant classes. The compiled files will reside in the bin folder. \\\\
\textbf{javac CorrectnessModule.java SuffixTreeUkkonen/*.java SuffixTreeNodes/*.java SuffixTray/*.java SuffixArray/*.java SimpleScan/*.java Experiments/*.java -d ../bin}\\\\
The following is a short overview of the relevant java classes and data folders.

\begin{description}
  \item \textbf{code/Thesis/src/CorrectnessModule.java}: Class containing methods to test the correctness of the suffix tree, array and tray structures. 
  By calling the main method without parameters, the tests are performed on the files registered with their respective filenames in the file \textit{texts/fileNames.txt},
  \item \textbf{code/Thesis/src/impleScan/SimpleScan.java}: Implementation of a simple search function that scans through a text from index 0 to $n$. 
  This algorithm is only used in the correctness check of the suffix tray, array and tree.
  The signature of the constructor is \textit{SimpleScan(String text)}.
  The signature of the search function is \textit{int search(String searchString)}. The int returned is the index of the found occurrence, and -1 if no occurrence is found.
  \item \textbf{code/Thesis/src/SuffixTreeUkkonen/SuffixTree.java}: Implementation of the suffix tree from subsection~\ref{suffix_tree_section}. Ukkonen's algorithm is used to construct the suffix tree. 
  The signature of the constructor is \textit{SuffixTree(String text, ChildrenStructures childrenStructure)}, where ChildrenStructures is an enum in the class \textit{NodeTypes.java} deciding how the children of a node is saved within the node.
  In all experiments in this thesis the enum \textit{BINARY} is used, indicating that the used suffix tree nodes are of the type \textit{SuffixTreeNodeBinary}.
  The signature of the search function is \textit{int search(String searchString)}. The int returned is the index of the found occurrence, and -1 if no occurrence is found.
  \item \textbf{code/Thesis/src/SuffixTreeNodes/SuffixTreeNodeBinary.java}: Is a suffix tree node where the pointers to the children are saved in a \textit{TreeMap}.
  \item \textbf{code/Thesis/src/SuffixArray/SuffixArray.java}: Implementation of a suffix array. The implementation is as the described suffix array in subsection~\ref{suffix_array_section}, with the difference that the lcp problem 
  is solved via a simple character by character comparison instead of a reduction to the NCA problem, as it was the case in subsection~\ref{suffix_array_section}.
  This solution has been chosen as the actual construction time is not the focus of this thesis.
  The constructor does not take any parameters, but the actual suffix array is first build, when the method with the signature \textit{construct\_SuffixArray\_from\_SuffixTree(SuffixTree suffix\_tree)} is called with the corresponding suffix tree.
  The signature of the search function is \textit{int search(String searchString)}. The int returned is the index of the found occurrence, and -1 if no occurrence is found.
  \item \textbf{code/Thesis/src/SuffixTray/SuffixTray.java}: Implementation of the suffix tray from subsection~\ref{suffix_tray_section}.
  The signature of the search function is \textit{int search(String searchString)}. The int returned is the index of the found occurrence, and -1 if no occurrence is found.
  The signature of the constructor is \textit{SuffixTray(SuffixTree suffix\_tree, SuffixArray suffix\_array))}, where the parameters are the corresponding suffix tree and suffix array.
  \item \textbf{code/Thesis/src/SuffixTray/SuffixTrayBranchingNode.java}: Branching $\sigma$-node implemented as described in subsection~\ref{suffix_tray_section}.
  \item \textbf{code/Thesis/src/SuffixTray/SuffixTrayNoneBranchingNode.java}: Non Branching $\sigma$-node implemented as described in subsection~\ref{suffix_tray_section}.
  \item \textbf{code/Thesis/src/SuffixTray/SuffixTrayInterval.java}: Suffix interval-node implemented as described in subsection~\ref{suffix_tray_section}.
  \item \textbf{code/Thesis/src/Experiments/}: The classes \textit{WorstCaseSuffixTreeExperiments.java}, \textit{WorstCaseSuffixTrayExperiments.java}, \textit{WorstCaseSuffixArrayExperiments.java} and \textit{CompareStructuresExperiments.java} contains the experiments performed in this thesis. All four classes have a main method which run the experiments from the thesis, if no parameters are parsed.
  In addition to the experiment classes, the folder contains different utility classes used in the post processing of the data. 
  \item \textbf{report}: Contains all latex files to create the report.
  \item \textbf{texts}: Contains the texts used in subsection~\ref{actual_time} as well as the texts used in the correctness module.\\
  The texts \textit{AV1611Bible\_2pow21.txt} and \textit{AV1611Bible\_2pow21\_lowerCase.txt} corresponds to Bib and BibLow in subsection~\ref{actual_time}, while \textit{genome1.fa} corresponds to Sgen.
  \item \textbf{data}: Contains all data used in the thesis as well as the plots and the gnuplot files to create the plots.\\
  Data files with a name containing \textit{Sorted} are sorted after the varying variable. \\
  Data files with a name containing \textit{Exp12Removed} are files where the two first runs have been removed. \\ 
  Data files with a name containing \textit{avgANDstdDev} are files containing average values and relative standard deviations.
\end{description}


\subsection{Correctness}
The correctness of the implementations was tested using the CorrectnessModule before any experiments were performed.
The structures were here tested on multiple texts, using both random substrings from the texts as well as strings not residing in the texts.
Furthermore it was tested that the structures complied with different invariants.
The invariants tested were:

\begin{description}
  \item Suffix tree: No more than $n-1$ inner nodes.
  \item Suffix tree: Exactly $n$ leaves.
  \item Suffix array: All suffixes have to be in lexicographical order.
  \item Suffix tray: No suffix intervals can be larger than $|\Sigma|^2$.
  \item Suffix tray: The tree structure must not contain more than $\frac{n}{|\Sigma|}$ branching $\sigma$-nodes.
\end{description}

In addition to the random tests, corner cases and their respective results have been controlled manually.
Last but not least, it was tested that the results of the search functions were as expected during all experiments. 
No tests showed bugs in the implementations.