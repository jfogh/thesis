\select@language {american}
\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Outline of Thesis}{2}{section.1.1}
\contentsline {section}{\numberline {1.2}Notations}{3}{section.1.2}
\contentsline {chapter}{\numberline {2}Data Structures}{4}{chapter.2}
\contentsline {section}{\numberline {2.1}Suffix Tree}{4}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}Outline of the suffix tree data structure}{4}{subsection.2.1.1}
\contentsline {subsection}{\numberline {2.1.2}Search and construction algorithms}{5}{subsection.2.1.2}
\contentsline {subsubsection}{Ukkonen's algorithm}{6}{section*.3}
\contentsline {section}{\numberline {2.2}Suffix Array}{8}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Outline of the suffix array data structure}{8}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}Search and construction algorithms}{8}{subsection.2.2.2}
\contentsline {subsubsection}{Solving the NCA problem}{10}{section*.4}
\contentsline {subsubsection}{Reduction from arbitrary trees to compressed trees}{13}{section*.5}
\contentsline {subsubsection}{Reduction from compressed trees to complete binary trees}{16}{section*.6}
\contentsline {section}{\numberline {2.3}Suffix Tray}{17}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}Outline of the suffix tray data structure}{17}{subsection.2.3.1}
\contentsline {subsection}{\numberline {2.3.2}The construction algorithm}{17}{subsection.2.3.2}
\contentsline {subsubsection}{Representation of the data structure}{18}{section*.7}
\contentsline {subsubsection}{Space and time complexity}{20}{section*.8}
\contentsline {subsection}{\numberline {2.3.3}The search algorithm}{21}{subsection.2.3.3}
\contentsline {chapter}{\numberline {3}Implementations}{22}{chapter.3}
\contentsline {subsection}{\numberline {3.0.4}Correctness}{24}{subsection.3.0.4}
\contentsline {chapter}{\numberline {4}Experiments}{25}{chapter.4}
\contentsline {section}{\numberline {4.1}Experimental setup}{25}{section.4.1}
\contentsline {section}{\numberline {4.2}Experiments to Determine the Worst Case Complexities}{26}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}Suffix Tree}{26}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}Suffix Array}{32}{subsection.4.2.2}
\contentsline {subsection}{\numberline {4.2.3}Suffix Tray}{37}{subsection.4.2.3}
\contentsline {section}{\numberline {4.3}Experiments to test the practical performance}{43}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}Suffix Tree}{45}{subsection.4.3.1}
\contentsline {subsection}{\numberline {4.3.2}Suffix Array}{48}{subsection.4.3.2}
\contentsline {subsection}{\numberline {4.3.3}Suffix Tray}{51}{subsection.4.3.3}
\contentsline {section}{\numberline {4.4}Comparison of the structures}{55}{section.4.4}
\contentsline {chapter}{\numberline {5}Conclusion}{57}{chapter.5}
\contentsline {subsection}{\numberline {5.0.1}Future work}{58}{subsection.5.0.1}
\contentsline {chapter}{\numberline {6}Appedix-Figures}{59}{chapter.6}
\contentsline {chapter}{Bibliography}{65}{figure.caption.20}
