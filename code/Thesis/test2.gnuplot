log2(x) = log(x)/log(2)

if (!exists("directory")) directory='./'

set terminal pdf enhanced font "Helvetica, 10" size 7,4
set output (directory . "testGnuplot2.pdf")

set xlabel "Index of first occurrence in T"
set xtics 0, 50 rotate
set offset 1, 1

set ylabel "Running time [milliseconds]"

set ytics nomirror tc lt 1


set xrange [50:2000]
set yrange [0:10000]

set grid mytics


set key vert left top reverse
set pointsize 2

set datafile separator ";"

stree(x) = 4.8*10**-5*x*log2(5)*1000000
sarray(x) = (3.28*10**-6*x+1.43*10**-5*log2(1852441))*1000000
sarray2(x) = (3.34*10**-6*x+3.43*10**-5*log2(1852441))*1000000
stray(x) = (1.06*10**-5*x+2.86*10**-5*log2(5))*1000000

plot (directory .  'genome1__250000.fa_compare_avgANDstdDev.csv') using ($3):($8) title 'Tree'  with linespoints ls 1,\
(directory .  'genome1__250000.fa_compare_avgANDstdDev.csv') using ($3):($11) title 'Array'  with linespoints ls 2,\
(directory .  'genome1__250000.fa_compare_avgANDstdDev.csv') using ($3):($14) title 'Tray'  with linespoints ls 3, \
stree(x) title 'WC-Tree'  with linespoints ls 4, \
sarray(x) title 'WC-Array'  with linespoints ls 5, \
sarray2(x) title 'WC-Array2'  with linespoints ls 7, \
stray(x) title 'WC-Tray'  with linespoints ls 6




