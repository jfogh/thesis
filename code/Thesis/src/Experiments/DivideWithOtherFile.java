package Experiments;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class DivideWithOtherFile {
	public static void main(String[] args) throws IOException{
		String divident  = "../../data/WC_SuffixTree/AvgTime+RST/WorstCase_SuffixTree_A(32)_N(3045062)_Sorted_Sorted_Exp12Removed_avgANDstdDev";
		  BufferedReader br_divident = new BufferedReader(new FileReader(divident + ".csv"));  
		  String divisor  = "../../data/WC_SuffixTree/AvgTime+RST/WorstCase_SuffixTree_A(2)_N(195002)_Sorted_Sorted_Exp12Removed_avgANDstdDev";
		  BufferedReader br_divisor = new BufferedReader(new FileReader(divisor + ".csv"));
		  
		  ArrayList<String> lines = new ArrayList<String>();
		    try {
				String filename = divident + "_dividedByA(2).csv";
				File file = new File(filename);
				BufferedWriter writer  = new BufferedWriter(new FileWriter(file));
				
				int lineNumber = 0;
				
				
		        String line = br_divident.readLine();
		        String line2 = br_divisor.readLine();	
		        while (line != null) {
		            if(lineNumber == 0){
		            	writer.write(line + "time/A(2); \n");
		            }else{
		            	double t1 = Double.parseDouble(line.split(";")[5].trim());
		            	double t2 = Double.parseDouble(line2.split(";")[5].trim());
		            	double t1_div_t2 = t1/t2;
		            	writer.write(line + t1_div_t2 + "; \n");
		            }
		            line = br_divident.readLine();
		            line2 = br_divisor.readLine();
		            lineNumber++;
		        }
		        
			    writer.flush();	
			    writer.close();	
		    } finally {
		    	br_divident.close();
		    	br_divisor.close();
		    }
	}
}
