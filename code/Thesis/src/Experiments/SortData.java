package Experiments;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class SortData {

	public static void main(String[] args) throws IOException{
		final int columnToSortAfter = 5;
		
//		  String inputFile = "../../data/WC_SuffixTree/TextPos_Dependency_SuffixTree";
		String inputFile = "bin/Alpha_Dependency_SuffixArray";
		 
		if(args.length > 0)
			inputFile = args[0];
			
		BufferedReader br = new BufferedReader(new FileReader(inputFile + ".csv"));
		  ArrayList<String> lines = new ArrayList<String>();
		    try {
				String filename = inputFile + "_Sorted.csv";
				File file = new File(filename);
				BufferedWriter writer  = new BufferedWriter(new FileWriter(file));
				
				int lineNumber = 0;
				
				
		        String line = br.readLine();

		        while (line != null) {
		            if(lineNumber == 0){
		            	writer.write(line + " \n");
		            }else{
		            	lines.add(line);
		            }
		            line = br.readLine();
		            lineNumber++;
		        }
		        
			    Collections.sort(lines, new Comparator<String>() {
		            public int compare(String o1, String o2) {
		                return Integer.parseInt(o1.split(";")[columnToSortAfter].trim()) - Integer.parseInt(o2.split(";")[columnToSortAfter].trim());
		           
			    }});
			    for(String s : lines)
			    	writer.write(s + " \n");	
			    
			    writer.flush();	
			    writer.close();	
		    } finally {
		        br.close();
		    }
	}
	
}
