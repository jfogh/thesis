package Experiments;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;

import SuffixArray.SuffixArray;
import SuffixTreeNodes.NodeTypes.ChildrenStructures;
import SuffixTreeUkkonen.SuffixTree;

public class WorstCaseSuffixTreeExperiments {
	
//	private static int experiment_repeats = 10;
	
	
	public static void main(String[] args) throws IOException{
		testNDependecy();
		testTextPositionDependecy();
		testAlphaAndMVariable();
	}
	
	public static void testTextPositionDependecy() throws IOException{
		int query_length = 100;
		int alphabetSize = 2;
		
		StringBuilder sb_q = new StringBuilder();
		for(int i=0; i< query_length; i++)
			sb_q.append('A');
		
		String query = sb_q.toString();
		int text_length = 200000;
		
		///////////////// File to write results in ///////////////////////
		String filename = "TextPos_Dependency_SuffixTree.csv";
		System.out.println(filename);
		
		File file = new File(filename);

		BufferedWriter writer  = new BufferedWriter(new FileWriter(file));
		writer.write("textSize; alphaSize; querySize; time; expCount; iterations; index; \n");

		//////////////////////////////////////////////////////////////////
		Random rnd = new Random();
		for(int exp_Count = 0; exp_Count < 7; exp_Count++){
			System.out.println("SUFFIX TREE - POSITION : EXPERIMENT NUMBER:" + exp_Count);
			for(int i=10000; i<text_length-query_length; i += 10000){
				StringBuilder sb = new StringBuilder();
				for(int j=0; j<i; j++){
					sb.append("B");
				}
				sb.append(query);
				sb.append("B");
				for(int j=i+query_length+1; j<text_length; j++){
					sb.append("B");
				}
				
			
				String text = sb.toString();

				SuffixTree suffix_tree = new SuffixTree(text, ChildrenStructures.BINARY);
				
				long running_time = 0;
				int numberOfIterations = 7500000;
				for(int iterations=0; iterations<numberOfIterations; iterations++){ 
					long start = System.currentTimeMillis();
					int result = suffix_tree.search(query);
					long estimatedTime = System.currentTimeMillis() - start;
					running_time += estimatedTime;
					if(result < 0){
						System.out.println("result:" + result);
						writer.close();
						throw new RuntimeException("Error in search");
					}
				}
				writer.write(text_length + "; " + alphabetSize + "; " + query_length + "; " + running_time + "; " + exp_Count + "; " + numberOfIterations + "; " + i + "; \n");
				writer.flush();	
			}
		}
		writer.close();
	}

	
	public static void testNDependecy() throws IOException{
		int query_length = 100; 
		int alphabetSize = 2;
		
		StringBuilder sb = new StringBuilder();
		for(int i = 0; i<query_length; i++){
			sb.append('A');
		}
		
		String query = sb.toString();
		
		///////////////// File to write results in ///////////////////////
		String filename = "N_Dependency_SuffixTree.csv";
		System.out.println(filename);
		
		File file = new File(filename);

		BufferedWriter writer  = new BufferedWriter(new FileWriter(file));
		writer.write("textSize; alphaSize; querySize; time; expCount; iterations; \n");

		//////////////////////////////////////////////////////////////////
		
		for(int exp_Count = 0; exp_Count < 7; exp_Count++){
			System.out.println("SUFFIX TREE - N DEPENDENCY: EXPERIMENT NUMBER:" + exp_Count);
			Random rnd = new Random();
			for(int text_length = 100000; text_length <= 2000000; text_length += 100000){ 
				StringBuilder sb_text = new StringBuilder();
				for(int i = 0; i<query_length; i++){
					sb_text.append('A');
				}
				sb_text.append("B");
				for(int i=0; i<text_length-query_length-1; i++){
					sb_text.append("B");
				}
	
				String text = sb_text.toString();
				
				SuffixTree suffix_tree = new SuffixTree(text, ChildrenStructures.BINARY);
				
				long running_time = 0;
				int numberOfIterations = 7500000;
				for(int iterations=0; iterations<numberOfIterations; iterations++){
					
					long start = System.currentTimeMillis();
					int result = suffix_tree.search(query);
					long estimatedTime = System.currentTimeMillis() - start;
					running_time += estimatedTime;
					if(result < 0){
						System.out.println("result:" + result);
						writer.close();
						throw new RuntimeException("Error in search");
					}
				}
				writer.write(text_length + "; " + alphabetSize + "; " + query_length + "; " + running_time + "; " + exp_Count + "; " + numberOfIterations + "; \n");
				writer.flush();	
			}	
		}
		writer.close();
	}
	
	public static void testAlphaAndMVariable(){
		try{
			for(int alphabetSize=2; alphabetSize < 65; alphabetSize = alphabetSize*2 )
			{
				testMVariable(alphabetSize);
			}
		}catch(Exception e){
			System.out.println(e);
			throw new RuntimeException("Error in filewriter");
		}
	}
	
	public static void testMVariable(int alphabetSize) throws IOException{
	
			StringBuilder sb = new StringBuilder();
			int charNumber = 1;
			//////////// Constructs Text and Suffix Tree //////////////
			for(int i=0; i<alphabetSize-1; i++){
				for(int j=0; j<=95000; j++){
					sb.append('0');
				}	
				sb.append(Character.toChars((int) '0' +charNumber));
				charNumber++;
			}
			Random rnd = new Random();
			int length_before_padding = sb.length();
			for(int j=0; j<100000; j++){
				int charNum = rnd.nextInt(alphabetSize-1)+1;
				sb.append(Character.toChars((int) '0' + charNum));
			}
			String text = sb.toString();
			SuffixTree suffix_tree = new SuffixTree(text, ChildrenStructures.BINARY);
			/////////////////////////////////////////////////////////////
			
			///////////////// File to write results in ///////////////////////
			String filename = "WorstCase_SuffixTree_A(" + alphabetSize + ")_N(" + text.length() + ").csv";
			System.out.println(filename);
			
			File file = new File(filename);

			BufferedWriter writer  = new BufferedWriter(new FileWriter(file));
			writer.write("textSize; alphaSize; querySize; time; expCount; iterations; \n");

			//////////////////////////////////////////////////////////////////
			
			for(int expCount=0; expCount<7; expCount++){
				System.out.println("SUFFIX TREE - M&ALFA: EXPERIMENT NUMBER:" + expCount);
				for(int query_length = 5000; query_length<=95000; query_length+= 5000){
					//////// Constrcucts search string /////////////
					StringBuilder sb_query = new StringBuilder();
					for(int i=0; i<query_length; i++){
						sb_query.append('0');
					}
					String query = sb_query.toString();
					////////////////////////////////////////////////
					
					long running_time = 0;
					int numberOfIterations = 2500; // 2500
					for(int iterations=0; iterations<numberOfIterations; iterations++){
						for(int i=0; i<5; i++){
							int temp = rnd.nextInt(50000);
							int beginIndex =  temp + length_before_padding;
							int length = rnd.nextInt(49999)+1;
							String cacheSearch = text.substring(beginIndex, beginIndex+length);
							suffix_tree.search(cacheSearch);
						}
						
						
						long start = System.currentTimeMillis();
						int result = suffix_tree.search(query);
						long estimatedTime = System.currentTimeMillis() - start;
						running_time += estimatedTime;
						if(result < 0){
							System.out.println("result:" + result);
							throw new RuntimeException("Error in search");
						}
					}
					writer.write(text.length() + "; " + alphabetSize + "; " + query_length + "; " + running_time + "; " + expCount + "; " + numberOfIterations + "; \n");
					writer.flush();	
				}
			}
			writer.close();
	}
	
	
}
