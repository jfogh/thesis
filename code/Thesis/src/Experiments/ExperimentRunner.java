package Experiments;

import java.io.IOException;

public class ExperimentRunner {

	public static void main(String[] args) throws IOException{
		WorstCaseSuffixArrayExperiments array_tests = new WorstCaseSuffixArrayExperiments();
		WorstCaseSuffixTreeExperiments tree_tests = new WorstCaseSuffixTreeExperiments();
		
		array_tests.main(args);
		tree_tests.main(args);
	}
}
