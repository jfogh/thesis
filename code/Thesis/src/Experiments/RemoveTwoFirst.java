package Experiments;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class RemoveTwoFirst {
	public static void main(String[] args) throws IOException{
		
//		  String inputFile = "../../data/WC_SuffixTree/TextPos_Dependency_SuffixTree_Sorted";
		String inputFile = "bin/Alpha_Dependency_SuffixArray_Sorted";
		if(args.length > 0)
			inputFile = args[0];
//			inputFile = args[0]+"_Sorted";
		
		  BufferedReader br = new BufferedReader(new FileReader(inputFile + ".csv"));
		  ArrayList<String> lines = new ArrayList<String>();
		    try {
				String filename = inputFile + "_Exp12Removed.csv";
				File file = new File(filename);
				BufferedWriter writer  = new BufferedWriter(new FileWriter(file));
				
				int lineNumber = 0;
				
				
		        String line = br.readLine();

		        while (line != null) {
		            if(lineNumber == 0){
		            	writer.write(line + " \n");
		            }else{
		            	lines.add(line);
		            }
		            line = br.readLine();
		            lineNumber++;
		        }
		        
			    for(String s : lines){
//			    	int expCount = Integer.parseInt(s.split(";")[4].trim()); 
//			    	int expCount = Integer.parseInt(s.split(";")[6].trim());
			    	int expCount = Integer.parseInt(s.split(";")[5].trim());
			    	if(expCount != 0 && expCount != 1)
			    		writer.write(s + " \n");
			    }
			    		
			    
			    writer.flush();	
			    writer.close();	
		    } finally {
		        br.close();
		    }
	}
}
