package Experiments;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;

public class AvgOfColumn {

	public static void main(String[] args) throws FileNotFoundException{
		int column = 6;
		  String inputFile = "bin/M_Dependency_None_BranchingNode_Sorted_Exp12Removed_avgANDstdDev";
		  BufferedReader br = new BufferedReader(new FileReader(inputFile + ".csv"));
		  ArrayList<String> lines = new ArrayList<String>();
		    try {
				String filename = inputFile + "_withColumnAvg.csv";
				File file = new File(filename);
				BufferedWriter writer  = new BufferedWriter(new FileWriter(file));
				
				int lineNumber = 0;
				
				
		        String line = br.readLine();

		        while (line != null) {
		            if(lineNumber == 0){
		            	writer.write(line + "graphAvg; \n");
		            }else{
		            	lines.add(line);
		            }
		            line = br.readLine();
		            lineNumber++;
		        }
		        
			    double sum = 0;
			    for(String s: lines){
			    	String[] parts = s.split(";");
			    	double t1 =  Double.parseDouble(parts[column].trim());
			    	double t2 = Double.parseDouble(parts[2].trim());
			    	sum += t1/t2;
			    }

			    double avgSum = (double) sum/ (double)lines.size();
			    for(int i=0; i<lines.size(); i++){
			    	writer.write(lines.get(i)+ avgSum + "; \n");
			    }
			    
			    writer.flush();
			    writer.close();
		        
		    }catch(Exception e){System.out.println("EXCEPTION");}
		    
	}
	
}
