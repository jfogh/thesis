package Experiments;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class AvgOfGradientAndConst {
	public static void main(String[] args) throws IOException{
//		  String inputFile = "../../data/WC_SuffixTree/TextPos_Dependency_SuffixTree_Sorted_Exp12Removed";
		  String inputFile = "bin/fitting_data_to_graph";
		  BufferedReader br = new BufferedReader(new FileReader(inputFile + ".csv"));
		  ArrayList<String> lines = new ArrayList<String>();
		    try {
				String filename = inputFile + "_withAvg.csv";
				File file = new File(filename);
				BufferedWriter writer  = new BufferedWriter(new FileWriter(file));
				
				int lineNumber = 0;
				
				
		        String line = br.readLine();

		        while (line != null) {
		            if(lineNumber == 0){
		            	writer.write(line + "gradientAVG; constantAVG; \n");
		            }else{
		            	lines.add(line);
		            }
		            line = br.readLine();
		            lineNumber++;
		        }
		        
			    double gradientSum = 0;
			    double constantSum = 0;
			    for(String s: lines){
			    	String[] parts = s.split(";");
			    	System.out.println(Double.parseDouble(parts[2].trim()));
			    	gradientSum += Double.parseDouble(parts[2].trim());
			    	constantSum += Double.parseDouble(parts[3].trim())/ log2(Double.parseDouble(parts[0].trim()));
			    	System.out.println("GRADIENS:" + gradientSum);
			    }
			    double avgGradient = (double) gradientSum/ (double)lines.size();
			    double avgConstant =(double) constantSum / (double)lines.size();
			    
			    for(int i=0; i<lines.size(); i++){
			    	writer.write(lines.get(i) + avgGradient + "; " + avgConstant + "; \n");
			    }
			    
			    writer.flush();
			    writer.close();
		        
		    }catch(Exception e){System.out.println("EXCEPTION");}
		    

		    
	}
	public static double log2(double x){
		return Math.log(x)/Math.log(2);
	}
	
}
