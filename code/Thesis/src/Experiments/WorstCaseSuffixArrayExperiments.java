package Experiments;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

import SuffixArray.SuffixArray;
import SuffixTreeNodes.NodeTypes.ChildrenStructures;
import SuffixTreeUkkonen.SuffixTree;

public class WorstCaseSuffixArrayExperiments {
	
	public static void main(String[] args) throws IOException{
		testTextPositionDependecy();
		testNAndMVariable();
		testAlphaDependency();
	}
	
	public static void testTextPositionDependecy() throws IOException{
		int query_length = 20000;
		StringBuilder sb_q = new StringBuilder();
		for(int i=0; i< query_length; i++)
			sb_q.append('A');
		
		String query = sb_q.toString();
		int text_length = 200000;
		
		///////////////// File to write results in ///////////////////////
		String filename = "TextPos_Dependency_SuffixArray.csv";
		System.out.println(filename);
		
		File file = new File(filename);

		BufferedWriter writer  = new BufferedWriter(new FileWriter(file));
		writer.write("textSize; alphaSize; querySize; time; expCount; index; \n");
		
		//////////////////////////////////////////////////////////////////2
		
		for(int exp_Count = 0; exp_Count < 7; exp_Count++){
			System.out.println("SUFFIX ARRAY - POSITION : EXPERIMENT NUMBER:" + exp_Count);
			for(int i=0; i<text_length-query_length-1; i += 10000){
				StringBuilder sb = new StringBuilder();
				for(int j=0; j<i; j++){
					sb.append('B');
				}
				sb.append(query);
				for(int j=i+query_length; j<text_length; j++){
					sb.append('B');
				}
				
				String text = sb.toString();
				
				SuffixTree suffix_tree = new SuffixTree(text, ChildrenStructures.BINARY);
				SuffixArray suffix_array = new SuffixArray();
				suffix_array.construct_SuffixArray_from_SuffixTree(suffix_tree);
				
				Random rnd = new Random();
				long running_time = 0;
				for(int iterations=0; iterations<75000; iterations++){
//					for(int k=0; k<5; k++){
//						int temp = rnd.nextInt(text_length);
//						int beginIndex =  temp;
//						int length = rnd.nextInt(text_length-temp)+1;
//						String cacheSearch = text.substring(beginIndex, beginIndex+length);
//						suffix_array.search(cacheSearch);
//					}
					
					
					long start = System.currentTimeMillis();
					int result = suffix_array.search(query);
					long estimatedTime = System.currentTimeMillis() - start;
					running_time += estimatedTime;
					if(result < 0){
						System.out.println("result:" + result);
						writer.close();
						throw new RuntimeException("Error in search");
					}
				}
				writer.write(text_length + "; " + 2 + "; " + query_length + "; " + running_time + "; " + exp_Count + "; " + i + "; \n");
				writer.flush();	
			}
		}
		writer.close();
	}
	

	public static void testAlphaDependency() throws IOException{
		int query_length = 20000;
		StringBuilder sb_q = new StringBuilder();
		for(int i=0; i< query_length; i++)
			sb_q.append('0');
		
		String query = sb_q.toString();
		int text_length = 200000;
		
		///////////////// File to write results in ///////////////////////
		String filename = "Alpha_Dependency_SuffixArray.csv";
		System.out.println(filename);
		
		File file = new File(filename);

		BufferedWriter writer  = new BufferedWriter(new FileWriter(file));
		writer.write("textSize; alphaSize; querySize; time; expCount; \n");

		//////////////////////////////////////////////////////////////////
		
		for(int exp_Count = 0; exp_Count < 7; exp_Count++){
			System.out.println("SUFFIX ARRAY - ALPHADEP: EXPERIMENT NUMBER:" + exp_Count);
			for(int alphaSize=2; alphaSize<65; alphaSize += 2){
				StringBuilder sb = new StringBuilder();
				sb.append(query);
				for(int i=0; i< alphaSize; i++){
					sb.append(Character.toChars((int) '0' + alphaSize));
				}
				
				Random rnd = new Random();
				for(int j=0; j<text_length-query_length-alphaSize; j++){
					int ch = rnd.nextInt(alphaSize-1)+1;
					sb.append(Character.toChars((int) '0' + ch));
				}
				
				String text = sb.toString();
				
				SuffixTree suffix_tree = new SuffixTree(text, ChildrenStructures.BINARY);
				SuffixArray suffix_array = new SuffixArray();
				suffix_array.construct_SuffixArray_from_SuffixTree(suffix_tree);
				
				
				long running_time = 0;
				int numberOfIterations = 75000;
				for(int iterations=0; iterations<numberOfIterations; iterations++){
//					for(int k=0; k<5; k++){
//						int temp = rnd.nextInt(text_length);
//						int beginIndex =  temp;
//						int length = rnd.nextInt(text_length-temp)+1;
//						String cacheSearch = text.substring(beginIndex, beginIndex+length);
//						suffix_array.search(cacheSearch);
//					}
					
					
					long start = System.currentTimeMillis();
					int result = suffix_array.search(query);
					long estimatedTime = System.currentTimeMillis() - start;
					running_time += estimatedTime;
					if(result < 0){
						System.out.println("result:" + result);
						writer.close();
						throw new RuntimeException("Error in search");
					}
				}
				writer.write(text_length + "; " + alphaSize + "; " + query_length + "; " + running_time + "; " + exp_Count  + "; " + numberOfIterations + "; \n");
				writer.flush();	
			}
		}
		writer.close();
	}
	
	
	public static void testNAndMVariable(){
		try{
		for(int n= (int) Math.pow(2, 7); n <= Math.pow(2, 21); n = n*2)
		{
			testMVariable(n);	
		}
		}catch(Exception e){
			System.out.println(e);
			throw new RuntimeException("Error in filewriter");
		}
	}
	
	public static void testMVariable(int n) throws IOException{
			int alphabetSize = 4;
//			int cacheres = 0;
			///////////////// File to write results in ///////////////////////
			String filename = "WorstCase_SuffixArray_N(" + n + ").csv";
			System.out.println(filename);
			
			File file = new File(filename);

			BufferedWriter writer  = new BufferedWriter(new FileWriter(file));
			writer.write("textSize; alphaSize; querySize; time; expCount; iterations; \n");

			//////////////////////////////////////////////////////////////////
			
//			int[] cacheArray = new int[2000000];
//			for(int i=0; i<2000000; i++){
//				cacheArray[i] = i%10;
//			}
//			
			for(int expCount=0; expCount<7; expCount++){
				System.out.println("SUFFIX Array - M&N: EXPERIMENT NUMBER:" + expCount);
				Random rnd = new Random();
				for(int query_length = 5; query_length<=200; query_length+= 5){
					//////// Constrcucts search string /////////////
					StringBuilder sb_query = new StringBuilder();
					for(int i=0; i<query_length; i++){
						sb_query.append('A');
					}
					String query = sb_query.toString();
					////////////////////////////////////////////////

					//////////// Constructs Text and Suffix Tree //////////////
					StringBuilder sb_text = new StringBuilder();
					sb_text.append(query);
//					for(int i=0; i<n-query_length; i++){
//						int c = rnd.nextInt(alphabetSize-1)+1;
//						sb_text.append(Character.toChars((int) 'A' + c));
//					}
					for(int i=0; i<n-query_length-2; i++){
						sb_text.append('B');
					}
					sb_text.append('C');
					sb_text.append('D');
					String text = sb_text.toString();
					SuffixTree suffix_tree = new SuffixTree(text, ChildrenStructures.BINARY);
					SuffixArray suffix_array = new SuffixArray();
					suffix_array.construct_SuffixArray_from_SuffixTree(suffix_tree);
					/////////////////////////////////////////////////////////////
					
					long running_time = 0;
					int numberOfIterations = 10000000;
					
					for(int iterations=0; iterations<numberOfIterations; iterations++){
						for(int i=0; i<1; i++){
//							int beginIndex = rnd.nextInt(n-query_length-50000)+query_length;
////							int length = rnd.nextInt(49999-1)+1;
//							int length = 30000;
//							String cacheSearch = text.substring(beginIndex, beginIndex+length);
//							suffix_tree.search(cacheSearch);
							suffix_tree.search("C");
						}
						//////////////////////////////
						
//						for(int i=200; i<2000000; i+=200){
//							cacheres += cacheArray[i];
//						}
						
						long start = System.currentTimeMillis();
						int result = suffix_array.search(query);
						long estimatedTime = System.currentTimeMillis() - start;
						running_time += estimatedTime;
						if(result < 0){
							System.out.println("result:" + result);
							throw new RuntimeException("Error in search");
						}
					}
					writer.write(text.length() + "; " + alphabetSize + "; " + query_length + "; " + running_time + "; " + expCount + "; " + numberOfIterations + "; \n");
					writer.flush();	
				}
			}
//			System.out.println(cacheres);
			writer.close();
	}
	
}
