package Experiments;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class AvgMultipleParameters {
	public static void main(String[] args) throws IOException{
//		  String inputFile = "../../data/WC_SuffixTree/TextPos_Dependency_SuffixTree_Sorted_Exp12Removed";
		  String inputFile = "bin/genome1.fa_0_compare_NotIn_65536";
			if(args.length > 0)
				inputFile = args[0]+"_Exp12Removed";
		  BufferedReader br = new BufferedReader(new FileReader(inputFile + ".csv"));
		  ArrayList<String> lines = new ArrayList<String>();
		    try {
				String filename = inputFile + "_avgANDstdDev.csv";
				File file = new File(filename);
				BufferedWriter writer  = new BufferedWriter(new FileWriter(file));
				
				int lineNumber = 0;
				
				
		        String line = br.readLine();

		        while (line != null) {
		            if(lineNumber == 0){
//		            	writer.write(line + "avgTimeTree; standardDeviationTree; relativeStdDevTree; avgTimeArray; standardDeviationArray; relativeStdDevArray; avgTimeTray; standardDeviationTray; relativeStdDevTray; \n");
		            	writer.write(line + "avgTimeArray; standardDeviationArray; relativeStdDevArray; avgTimeTray; standardDeviationTray; relativeStdDevTray; \n");
		            }else{
		            	lines.add(line);
		            }
		            line = br.readLine();
		            lineNumber++;
		        }
		        
		        
//		        for(int i=0; i<lines.size(); i+=2){
//		        	String[] l1 = lines.get(i).split(";");
//		        	String[] l2 = lines.get(i+1).split(";");
//		        	double tree1 = Double.parseDouble(l1[3].trim());
//		        	double tree2 = Double.parseDouble(l2[3].trim());
//		        	
//		        	double array1 = Double.parseDouble(l1[4].trim());
//		        	double array2 = Double.parseDouble(l2[4].trim());
//		        	
//		        	double tray1 = Double.parseDouble(l1[5].trim());
//		        	double tray2 = Double.parseDouble(l2[5].trim());
//		        	
//		        	double totalTimeTree = tree1+tree2;
//		        	double avgTimeTree = totalTimeTree/2;
//		        	double varianceTree = (Math.pow(tree1-avgTimeTree,2) + Math.pow(tree2-avgTimeTree,2))/2;
//		        	double deviationTree = Math.sqrt(varianceTree);
//		        	    	
//		        	double totalTimeArray = array1+array2;
//		        	double avgTimeArray = totalTimeArray/2;
//		        	double varianceArray = (Math.pow(array1-avgTimeArray,2) + Math.pow(array2-avgTimeArray,2))/2;
//		        	double deviationArray = Math.sqrt(varianceArray);
//		      
//		          	double totalTimeTray = tray1+tray2;
//		        	double avgTimeTray = totalTimeTray/2;
//		        	double varianceTray = (Math.pow(tray1-avgTimeTray,2) + Math.pow(tray2-avgTimeTray,2))/2;
//		        	double deviationTray = Math.sqrt(varianceTray);
//		        	
//		        	
//		        	writer.write(lines.get(i) + " " + avgTimeTree + "; " + deviationTree + ";  " + (deviationTree/avgTimeTree*100) + ";" + avgTimeArray + "; " + deviationArray + ";  " + (deviationArray/avgTimeArray*100) + ";" + avgTimeTray + "; " + deviationTray + ";  " + (deviationTray/avgTimeTray*100) + "; \n");	
//		        	
//		        }
		        
//		        for(int i=0; i<lines.size(); i+=5){
//		        	String[] l1 = lines.get(i).split(";");
//		        	String[] l2 = lines.get(i+1).split(";");
//		        	String[] l3 = lines.get(i+2).split(";");
//		        	String[] l4 = lines.get(i+3).split(";");
//		        	String[] l5 = lines.get(i+4).split(";");
//		        	double tree1 = Double.parseDouble(l1[3].trim());
//		        	double tree2 = Double.parseDouble(l2[3].trim());
//		        	double tree3 = Double.parseDouble(l3[3].trim());
//		        	double tree4 = Double.parseDouble(l4[3].trim());
//		        	double tree5 = Double.parseDouble(l5[3].trim());
//		        	
//		        	double array1 = Double.parseDouble(l1[4].trim());
//		        	double array2 = Double.parseDouble(l2[4].trim());
//		        	double array3 = Double.parseDouble(l3[4].trim());
//		        	double array4 = Double.parseDouble(l4[4].trim());
//		        	double array5 = Double.parseDouble(l5[4].trim());
//		        	
//		        	double tray1 = Double.parseDouble(l1[5].trim());
//		        	double tray2 = Double.parseDouble(l2[5].trim());
//		        	double tray3 = Double.parseDouble(l3[5].trim());
//		        	double tray4 = Double.parseDouble(l4[5].trim());
//		        	double tray5 = Double.parseDouble(l5[5].trim());
//		        	
//		        	double totalTimeTree = tree1+tree2+tree3+tree4+tree5;
//		        	double avgTimeTree = totalTimeTree/5;
//		        	double varianceTree = (Math.pow(tree1-avgTimeTree,2) + Math.pow(tree2-avgTimeTree,2) + Math.pow(tree3-avgTimeTree,2) + Math.pow(tree4-avgTimeTree,2) + Math.pow(tree5-avgTimeTree,2))/5;
//		        	double deviationTree = Math.sqrt(varianceTree);
//		        	    	
//		        	double totalTimeArray = array1+array2+array3+array4+array5;
//		        	double avgTimeArray = totalTimeArray/5;
//		        	double varianceArray = (Math.pow(array1-avgTimeArray,2) + Math.pow(array2-avgTimeArray,2) + Math.pow(array3-avgTimeArray,2) + Math.pow(array4-avgTimeArray,2) + Math.pow(array5-avgTimeArray,2))/5;
//		        	double deviationArray = Math.sqrt(varianceArray);
//		      
//		          	double totalTimeTray = tray1+tray2+tray3+tray4+tray5;
//		        	double avgTimeTray = totalTimeTray/5;
//		        	double varianceTray = (Math.pow(tray1-avgTimeTray,2) + Math.pow(tray2-avgTimeTray,2) + Math.pow(tray3-avgTimeTray,2) + Math.pow(tray4-avgTimeTray,2) + Math.pow(tray5-avgTimeTray,2))/5;
//		        	double deviationTray = Math.sqrt(varianceTray);
//		        	
//		        	
//		        	writer.write(lines.get(i) + " " + avgTimeTree + "; " + deviationTree + ";  " + (deviationTree/avgTimeTree*100) + ";" + avgTimeArray + "; " + deviationArray + ";  " + (deviationArray/avgTimeArray*100) + ";" + avgTimeTray + "; " + deviationTray + ";  " + (deviationTray/avgTimeTray*100) + "; \n");
//		        }
		        
		        for(int i=0; i<lines.size(); i+=5){
		        	String[] l1 = lines.get(i).split(";");
		        	String[] l2 = lines.get(i+1).split(";");
		        	String[] l3 = lines.get(i+2).split(";");
		        	String[] l4 = lines.get(i+3).split(";");
		        	String[] l5 = lines.get(i+4).split(";");
		        	
		        	double array1 = Double.parseDouble(l1[3].trim());
		        	double array2 = Double.parseDouble(l2[3].trim());
		        	double array3 = Double.parseDouble(l3[3].trim());
		        	double array4 = Double.parseDouble(l4[3].trim());
		        	double array5 = Double.parseDouble(l5[3].trim());
		        	
		        	double tray1 = Double.parseDouble(l1[4].trim());
		        	double tray2 = Double.parseDouble(l2[4].trim());
		        	double tray3 = Double.parseDouble(l3[4].trim());
		        	double tray4 = Double.parseDouble(l4[4].trim());
		        	double tray5 = Double.parseDouble(l5[4].trim());
		        		
		        	double totalTimeArray = array1+array2+array3+array4+array5;
		        	double avgTimeArray = totalTimeArray/5;
		        	double varianceArray = (Math.pow(array1-avgTimeArray,2) + Math.pow(array2-avgTimeArray,2) + Math.pow(array3-avgTimeArray,2) + Math.pow(array4-avgTimeArray,2) + Math.pow(array5-avgTimeArray,2))/5;
		        	double deviationArray = Math.sqrt(varianceArray);
		      
		          	double totalTimeTray = tray1+tray2+tray3+tray4+tray5;
		        	double avgTimeTray = totalTimeTray/5;
		        	double varianceTray = (Math.pow(tray1-avgTimeTray,2) + Math.pow(tray2-avgTimeTray,2) + Math.pow(tray3-avgTimeTray,2) + Math.pow(tray4-avgTimeTray,2) + Math.pow(tray5-avgTimeTray,2))/5;
		        	double deviationTray = Math.sqrt(varianceTray);
		        	
		        	
		        	writer.write(lines.get(i) + " " + avgTimeArray + "; " + deviationArray + ";  " + (deviationArray/avgTimeArray*100) + ";" + avgTimeTray + "; " + deviationTray + ";  " + (deviationTray/avgTimeTray*100) + "; \n");
		        }
		        
			    writer.flush();	
			    writer.close();	
		    } finally {
		        br.close();
		    }
	}
}
