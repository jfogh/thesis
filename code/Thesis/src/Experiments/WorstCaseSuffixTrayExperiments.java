package Experiments;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;

import javax.xml.stream.events.Characters;

import SuffixArray.SuffixArray;
import SuffixTray.SuffixTray;
import SuffixTreeNodes.NodeTypes.ChildrenStructures;
import SuffixTreeUkkonen.SuffixTree;

public class WorstCaseSuffixTrayExperiments {

	public static void main(String[] args) throws IOException, InterruptedException{
		// None Branching tests
		testNDependencyForNoneBranchingNodes();
		testPositionDependencyForNoneBranchingNodes(); 
		testAlphaDependencyForNoneBranchingNodes();
		
		// M dependencies
		testMDependencyForBranchingNodes(); 
		testMDependencyForNoneBranchingNodes(); 
		
		// Branching tests
		testNDependencyForBranchingNodes(); 
		testPositionDependencyForBranchingNodes();
		testAlphaDependencyForBranchingNodes();
	}
	
	public static void testNDependencyForBranchingNodes() throws IOException{
		int alpha_size = 2;
		int query_length = 50;
		StringBuilder sb_query = new StringBuilder();
		for(int i=0; i<query_length; i++){
			sb_query.append("A");
		}
		String query = sb_query.toString();
		
		///////////////// File to write results in ///////////////////////
		String filename = "N_Dependency_BranchingNode.csv";
		System.out.println(filename);
		
		File file = new File(filename);

		BufferedWriter writer  = new BufferedWriter(new FileWriter(file));
		writer.write("textSize; alphaSize; querySize; time; expCount; iterations; \n");


		//////////////////////////////////////////////////////////////////
		
		for(int exp_Count = 0; exp_Count < 7; exp_Count++){ 
			System.out.println("SUFFIX TRAY - N (BRANCHING): EXPERIMENT NUMBER:" + exp_Count);
			for(int text_length = 100000; text_length <= 2000000; text_length+=100000){ 
				StringBuilder sb_text = new StringBuilder();
				sb_text.append(query);
				sb_text.append("BB");
				sb_text.append(query);
				sb_text.append("BA");
				
				int missingLetters = text_length - sb_text.length();
				Random rnd = new Random();
				for(int i=0; i<missingLetters; i++){
					int c = rnd.nextInt(alpha_size);
					sb_text.append(Character.toChars((int) 'A' + c)); // Random characters are inserted to avoid stackoverflow, when recursively building the suffix tray from the suffix tree (Det er lige meget hvilke streng der bruges, da worst case if�lge teorien er en hvilken som helst streng)	
				}
	
						
				String text = sb_text.toString();
				
				SuffixTree st = new SuffixTree(text, ChildrenStructures.BINARY);
				SuffixArray ar = new SuffixArray();
				ar.construct_SuffixArray_from_SuffixTree(st);
				SuffixTray suffix_tray = new SuffixTray(st, ar);
	
				int numberOfIterations = 20000000; 
				long running_time = 0;
				for(int iterations = 0; iterations<numberOfIterations; iterations++){
	
					long start = System.currentTimeMillis();
					int result = suffix_tray.search(query);
					long estimatedTime = System.currentTimeMillis() - start;
					running_time += estimatedTime;
					if(result < 0){
						System.out.println("result:" + result);
						writer.close();
						throw new RuntimeException("Error in search");
					}
				}
				
				writer.write(text_length + "; " + alpha_size + "; " + query_length + "; " + running_time + "; " + exp_Count + "; " + numberOfIterations + "; \n");
				writer.flush();	
			}
		}
		writer.close();
	}

	public static void testNDependencyForNoneBranchingNodes() throws IOException{
		int alpha_size = 2;
		int query_length = 50;
		StringBuilder sb_query = new StringBuilder();
		for(int i=0; i<query_length; i++){
			sb_query.append("A");
		}
		String query = sb_query.toString();
		
		///////////////// File to write results in ///////////////////////
		String filename = "N_Dependency_None_BranchingNode.csv";
		System.out.println(filename);
		
		File file = new File(filename);

		BufferedWriter writer  = new BufferedWriter(new FileWriter(file));
		writer.write("textSize; alphaSize; querySize; time; expCount; iterations; \n");


		//////////////////////////////////////////////////////////////////
		
		for(int exp_Count = 0; exp_Count < 7; exp_Count++){
			System.out.println("SUFFIX TRAY - N (NONE BRANCHING): EXPERIMENT NUMBER:" + exp_Count);
			for(int text_length = 10000; text_length <= 250000; text_length+=10000){
				StringBuilder sb_text = new StringBuilder();
				sb_text.append(query);
				for(int i=0; i<alpha_size; i++){
					sb_text.append("A");
				}
				sb_text.append("AB");
				
				int missingLetters = text_length - sb_text.length();
				Random rnd = new Random();
				for(int i=0; i<missingLetters; i++){
					sb_text.append('A');
//					int c = rnd.nextInt(alpha_size);
//					sb_text.append(Character.toChars((int) 'A' + c)); // Random characters are inserted to avoid stackoverflow, when recursively building the suffix tray from the suffix tree (Det er lige meget hvilke streng der bruges, da worst case if�lge teorien er en hvilken som helst streng)	
				}
	
						
				String text = sb_text.toString();
				
				SuffixTree st = new SuffixTree(text, ChildrenStructures.BINARY);
				SuffixArray ar = new SuffixArray();
				ar.construct_SuffixArray_from_SuffixTree(st);
				SuffixTray suffix_tray = new SuffixTray(st, ar);
	
				int numberOfIterations = 20000000; 
				long running_time = 0;
				for(int iterations = 0; iterations<numberOfIterations; iterations++){
	
					long start = System.currentTimeMillis();
					int result = suffix_tray.search(query);
					long estimatedTime = System.currentTimeMillis() - start;
					running_time += estimatedTime;
					if(result < 0){
						System.out.println("result:" + result);
						writer.close();
						throw new RuntimeException("Error in search");
					}
				}
				
				writer.write(text_length + "; " + alpha_size + "; " + query_length + "; " + running_time + "; " + exp_Count + "; " + numberOfIterations + "; \n");
				writer.flush();	
			}
		}
		writer.close();
	}
	
	public static void testPositionDependencyForBranchingNodes() throws IOException{
		int alpha_size = 2;
		int query_length = 50;
		int text_length = 200000; 
		StringBuilder sb_query = new StringBuilder();
		for(int i=0; i<query_length; i++){
			sb_query.append("A");
		}
		String query = sb_query.toString();
		
		///////////////// File to write results in ///////////////////////
		String filename = "Position_Dependency_BranchingNode.csv";
		System.out.println(filename);
		
		File file = new File(filename);

		BufferedWriter writer  = new BufferedWriter(new FileWriter(file));
		writer.write("textSize; alphaSize; querySize; time; expCount; iterations; index; \n");

		//////////////////////////////////////////////////////////////////
		Random rnd = new Random();
		for(int exp_Count = 0; exp_Count < 7; exp_Count++){
			System.out.println("SUFFIX TRAY - Position (BRANCHING): EXPERIMENT NUMBER:" + exp_Count);
			for(int index = 0; index <= text_length-((query_length+2)*2); index+=10000){
				StringBuilder sb_text = new StringBuilder();
				for(int i=0; i<index; i++){
					int c = rnd.nextInt(alpha_size);
					sb_text.append(Character.toChars((int) 'A' + c));  // dette er ok, da der er t�t p� 0 sandsynlighed for at strengen p� 50 tegn, kommer f�r (2^50)
				}
				
				sb_text.append(query);
				sb_text.append("BB");
				sb_text.append(query);
				sb_text.append("BA");
				
				int missingLetters = text_length - sb_text.length();
				for(int i=0; i<missingLetters; i++){
					int c = rnd.nextInt(alpha_size);
					sb_text.append(Character.toChars((int) 'A' + c)); 	
				}
	
						
				String text = sb_text.toString();
				
				SuffixTree st = new SuffixTree(text, ChildrenStructures.BINARY);
				SuffixArray ar = new SuffixArray();
				ar.construct_SuffixArray_from_SuffixTree(st);
				SuffixTray suffix_tray = new SuffixTray(st, ar);
	
				int numberOfIterations = 20000000; 
				long running_time = 0;
				for(int iterations = 0; iterations<numberOfIterations; iterations++){
	
					long start = System.currentTimeMillis();
					int result = suffix_tray.search(query);
					long estimatedTime = System.currentTimeMillis() - start;
					running_time += estimatedTime;
					if(result < 0){
						System.out.println("result:" + result);
						writer.close();
						throw new RuntimeException("Error in search");
					}
					
				}
				
				writer.write(text_length + "; " + alpha_size + "; " + query_length + "; " + running_time + "; " + exp_Count + "; " + numberOfIterations + "; " + index + "; \n");
				writer.flush();	
			}
		}
		writer.close();
	}
	
	public static void testPositionDependencyForNoneBranchingNodes() throws IOException, InterruptedException{
		int alpha_size = 2;
		int query_length = 50; 
		int text_length = 200000;  
		StringBuilder sb_query = new StringBuilder();
		for(int i=0; i<query_length; i++){
			sb_query.append("A");
		}
		String query = sb_query.toString();
		
		///////////////// File to write results in ///////////////////////
		String filename = "Position_Dependency_None_BranchingNode.csv";
		System.out.println(filename);
		
		File file = new File(filename);

		BufferedWriter writer  = new BufferedWriter(new FileWriter(file));
		writer.write("textSize; alphaSize; querySize; time; expCount; iterations; index; \n");

		//////////////////////////////////////////////////////////////////
		Random rnd = new Random();
		for(int exp_Count = 0; exp_Count < 7; exp_Count++){
			System.out.println("SUFFIX TRAY - Position (NONE BRANCHING): EXPERIMENT NUMBER:" + exp_Count);
			for(int index = 0; index <= text_length-(query_length+4); index+=10000){
				StringBuilder sb_text = new StringBuilder();
				for(int i=0; i<index; i++){
					sb_text.append('A');
				}

				sb_text.append(query);
				sb_text.append("AAAB");
				
				int missingLetters = text_length - sb_text.length();
				for(int i=0; i<missingLetters; i++){
					sb_text.append('A');
				}
	
						
				String text = sb_text.toString();
				SuffixTree st = new SuffixTree(text, ChildrenStructures.BINARY);
				SuffixArray ar = new SuffixArray();
				ar.construct_SuffixArray_from_SuffixTree(st);
				SuffixTray suffix_tray = new SuffixTray(st, ar);
	
				int numberOfIterations = 20000000; 
				long running_time = 0;
				for(int iterations = 0; iterations<numberOfIterations; iterations++){
	
					long start = System.currentTimeMillis();
					int result = suffix_tray.search(query);
					long estimatedTime = System.currentTimeMillis() - start;
					running_time += estimatedTime;
					if(result < 0){
						System.out.println("result:" + result);
						writer.close();
						throw new RuntimeException("Error in search");
					}
					
				}
				
				writer.write(text_length + "; " + alpha_size + "; " + query_length + "; " + running_time + "; " + exp_Count + "; " + numberOfIterations + "; " + index + "; \n");
				writer.flush();	
			}
		}
		writer.close();
	}
	
	public static void testAlphaDependencyForBranchingNodes() throws IOException{
		int text_length = 200000;
		int query_length = 50;
		StringBuilder sb_query = new StringBuilder();
		for(int i=0; i<query_length; i++){
			sb_query.append("0");
		}
		String query = sb_query.toString();
		
		///////////////// File to write results in ///////////////////////
		String filename = "Alpha_Dependency_BranchingNode.csv";
		System.out.println(filename);
		
		File file = new File(filename);

		BufferedWriter writer  = new BufferedWriter(new FileWriter(file));
		writer.write("textSize; alphaSize; querySize; time; expCount; iterations; \n");


		//////////////////////////////////////////////////////////////////
		
		for(int exp_Count = 0; exp_Count < 7; exp_Count++){
			System.out.println("SUFFIX TRAY - ALPHA (BRANCHING): EXPERIMENT NUMBER:" + exp_Count);
			for(int alpha_size = 2; alpha_size <= 64; alpha_size+=2){ 
				StringBuilder sb_text = new StringBuilder();
				for(int i=0; i<alpha_size; i++){
					sb_text.append(query);
					sb_text.append("1");
					sb_text.append(Character.toChars((int) '0' + i));
				}

				int missingLetters = text_length - sb_text.length();
				Random rnd = new Random();
				for(int i=0; i<missingLetters; i++){
					int c = rnd.nextInt(alpha_size);
					sb_text.append(Character.toChars((int) '0' + c)); // Random characters are inserted to avoid stackoverflow, when recursively building the suffix tray from the suffix tree (Det er lige meget hvilke streng der bruges, da worst case if�lge teorien er en hvilken som helst streng)	
				}
	
						
				String text = sb_text.toString();
				
				SuffixTree st = new SuffixTree(text, ChildrenStructures.BINARY);
				SuffixArray ar = new SuffixArray();
				ar.construct_SuffixArray_from_SuffixTree(st);
				SuffixTray suffix_tray = new SuffixTray(st, ar);
	
				int numberOfIterations = 20000000; 
				long running_time = 0;
				for(int iterations = 0; iterations<numberOfIterations; iterations++){

					long start = System.currentTimeMillis();
					int result = suffix_tray.search(query);
					long estimatedTime = System.currentTimeMillis() - start;
					running_time += estimatedTime;
					
					if(result < 0){
						System.out.println("result:" + result);
						writer.close();
						throw new RuntimeException("Error in search");
					}
					
				}
				
				writer.write(text_length + "; " + alpha_size + "; " + query_length + "; " + running_time + "; " + exp_Count + "; " + numberOfIterations + "; \n");
				writer.flush();	
			}
		}
		writer.close();
	}

	public static void testAlphaDependencyForNoneBranchingNodes() throws IOException{
		int text_length = 200000;
		int query_length = 50;
		StringBuilder sb_query = new StringBuilder();
		for(int i=0; i<query_length; i++){
			sb_query.append("0");
		}
		String query = sb_query.toString();
		
		///////////////// File to write results in ///////////////////////
		String filename = "Alpha_Dependency_None_BranchingNode.csv";
		System.out.println(filename);
		
		File file = new File(filename);

		BufferedWriter writer  = new BufferedWriter(new FileWriter(file));
		writer.write("textSize; alphaSize; querySize; time; expCount; iterations; \n");


		//////////////////////////////////////////////////////////////////
		
		for(int exp_Count = 0; exp_Count < 7; exp_Count++){ 
			System.out.println("SUFFIX TRAY - ALPHA (NONE BRANCHING): EXPERIMENT NUMBER:" + exp_Count);
			for(int alpha_size = 2; alpha_size <= 64; alpha_size+=2){ 
				StringBuilder sb_text = new StringBuilder();
				sb_text.append(query);

				for(int i=0; i<alpha_size; i++){
					sb_text.append(Character.toChars((int) '0' + i));
				}

				int missingLetters = text_length - sb_text.length();
				Random rnd = new Random();
				for(int i=0; i<missingLetters; i++){
					sb_text.append("0");
				}
				String text = sb_text.toString();

				SuffixTree st = new SuffixTree(text, ChildrenStructures.BINARY);
				SuffixArray ar = new SuffixArray();
				ar.construct_SuffixArray_from_SuffixTree(st);
				SuffixTray suffix_tray = new SuffixTray(st, ar);
	
				int numberOfIterations = 20000000; 
				long running_time = 0;
				for(int iterations = 0; iterations<numberOfIterations; iterations++){
					
					long start = System.currentTimeMillis();
					int result = suffix_tray.search(query);
					long estimatedTime = System.currentTimeMillis() - start;
					running_time += estimatedTime;
					
					if(result < 0){
						System.out.println("result:" + result);
						writer.close();
						throw new RuntimeException("Error in search");
					}
					
				}
				
				writer.write(text_length + "; " + alpha_size + "; " + query_length + "; " + running_time + "; " + exp_Count + "; " + numberOfIterations + "; \n");
				writer.flush();	
			}
		}
		writer.close();
	}

	public static void testMDependencyForBranchingNodes() throws IOException{
		int alpha_size = 2;
		int text_length = 200000;
		int max_query_length = 1500;
		
		///////////////// File to write results in ///////////////////////
		String filename = "M_Dependency_BranchingNode.csv";
		System.out.println(filename);
		
		File file = new File(filename);

		BufferedWriter writer  = new BufferedWriter(new FileWriter(file));
		writer.write("textSize; alphaSize; querySize; time; expCount; iterations; \n");


		//////////////////////////////////////////////////////////////////
		StringBuilder sb_text = new StringBuilder();
		for(int i = 0; i<max_query_length; i++)
		{
			sb_text.append("A");
		}
		sb_text.append("BB");
		for(int i = 0; i<max_query_length; i++)
		{
			sb_text.append("A");
		}
		sb_text.append("BA");	
		int missingLetters = text_length - sb_text.length();
		Random rnd = new Random();
		for(int i=0; i<missingLetters; i++){
			int c = rnd.nextInt(alpha_size);
			sb_text.append(Character.toChars((int) 'A' + c)); // Random characters are inserted to avoid stackoverflow, when recursively building the suffix tray from the suffix tree (Det er lige meget hvilke streng der bruges, da worst case if�lge teorien er en hvilken som helst streng)	
		}

		String text = sb_text.toString();
		SuffixTree st = new SuffixTree(text, ChildrenStructures.BINARY);
		SuffixArray ar = new SuffixArray();
		ar.construct_SuffixArray_from_SuffixTree(st);
		SuffixTray suffix_tray = new SuffixTray(st, ar);
				
		for(int exp_Count = 0; exp_Count < 7; exp_Count++){
			System.out.println("SUFFIX TRAY - M (BRANCHING): EXPERIMENT NUMBER:" + exp_Count);
			for(int query_length = 100; query_length <= 100; query_length+=50){
				StringBuilder sb_query = new StringBuilder();
				for(int i=0; i<query_length; i++){
					sb_query.append("A");
				}
				String query = sb_query.toString();
				
				int numberOfIterations = 4000000; 
				long running_time = 0;
				for(int iterations = 0; iterations<numberOfIterations; iterations++){
					for(int k=0; k<5; k++){
						int temp = rnd.nextInt(text_length-10);
						int beginIndex =  temp;
						int length = (rnd.nextInt(text_length-temp)%2000)+1;
						String cacheSearch = text.substring(beginIndex, beginIndex+length);
						suffix_tray.search(cacheSearch);
					}
					
					long start = System.currentTimeMillis();
					int result = suffix_tray.search(query);
					long estimatedTime = System.currentTimeMillis() - start;
					running_time += estimatedTime;
					if(result < 0){
						System.out.println("result:" + result);
						writer.close();
						throw new RuntimeException("Error in search");
					}
				}
				
				writer.write(text_length + "; " + alpha_size + "; " + query_length + "; " + running_time + "; " + exp_Count + "; " + numberOfIterations + "; \n");
				writer.flush();	
			}
		}
		writer.close();
	}

	public static void testMDependencyForNoneBranchingNodes() throws IOException{
		int alpha_size = 2;
		int text_length = 200000;
		int max_query_length = 1500;
		
		///////////////// File to write results in ///////////////////////
		String filename = "M_Dependency_None_BranchingNode.csv";
		System.out.println(filename);
		
		File file = new File(filename);

		BufferedWriter writer  = new BufferedWriter(new FileWriter(file));
		writer.write("textSize; alphaSize; querySize; time; expCount; iterations; \n");


		//////////////////////////////////////////////////////////////////
		
		StringBuilder sb_text = new StringBuilder();
		for(int i=0; i<max_query_length; i++){
			sb_text.append("A");
		}
		sb_text.append("AAAB");
		
		int missingLetters = text_length - sb_text.length();
		for(int i=0; i<missingLetters; i++){
			sb_text.append('A');
		}		
		String text = sb_text.toString();
		
		SuffixTree st = new SuffixTree(text, ChildrenStructures.BINARY);
		SuffixArray ar = new SuffixArray();
		ar.construct_SuffixArray_from_SuffixTree(st);
		SuffixTray suffix_tray = new SuffixTray(st, ar);

		
		for(int exp_Count = 0; exp_Count < 7; exp_Count++){
			System.out.println("SUFFIX TRAY - M (NONE BRANCHING): EXPERIMENT NUMBER:" + exp_Count);
			for(int query_length = 50; query_length <= max_query_length; query_length+=50){
				StringBuilder sb_query = new StringBuilder();
				for(int i=0; i<query_length; i++){
					sb_query.append("A");
				}
				String query = sb_query.toString();
	
				int numberOfIterations = 4000000; 
				long running_time = 0;
				Random rnd = new Random();
				for(int iterations = 0; iterations<numberOfIterations; iterations++){
					for(int k=0; k<5; k++){
						int temp = rnd.nextInt(text_length-10);
						int beginIndex =  temp;
						int length = (rnd.nextInt(text_length-temp)%2000)+1;
						String cacheSearch = text.substring(beginIndex, beginIndex+length);
						suffix_tray.search(cacheSearch);
					}
					
					long start = System.currentTimeMillis();
					int result = suffix_tray.search(query);
					long estimatedTime = System.currentTimeMillis() - start;
					running_time += estimatedTime;
					if(result < 0){
						System.out.println("result:" + result);
						writer.close();
						throw new RuntimeException("Error in search");
					}
				}
				
				writer.write(text_length + "; " + alpha_size + "; " + query_length + "; " + running_time + "; " + exp_Count + "; " + numberOfIterations + "; \n");
				writer.flush();	
			}
		}
		writer.close();
	}
}
