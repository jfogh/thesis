package Experiments;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class AverageWithVariance {
	public static void main(String[] args) throws IOException{
//		  String inputFile = "../../data/WC_SuffixTree/TextPos_Dependency_SuffixTree_Sorted_Exp12Removed";
		  String inputFile = "bin/Alpha_Dependency_SuffixArray_Sorted_Exp12Removed";
			if(args.length > 0)
				inputFile = args[0]+ "_Sorted_Exp12Removed";
		  BufferedReader br = new BufferedReader(new FileReader(inputFile + ".csv"));
		  ArrayList<String> lines = new ArrayList<String>();
		    try {
				String filename = inputFile + "_avgANDstdDev.csv";
				File file = new File(filename);
				BufferedWriter writer  = new BufferedWriter(new FileWriter(file));
				
				int lineNumber = 0;
				
				
		        String line = br.readLine();

		        while (line != null) {
		            if(lineNumber == 0){
//		            	writer.write(line + "ConstantAvgTime; ConstantStandardDeviation; ConstantRelativeStdDev; \n");
		            	writer.write(line + "avgTime; standardDeviation; relativeStdDev; \n");
		            }else{
		            	lines.add(line);
		            }
		            line = br.readLine();
		            lineNumber++;
		        }
		        
		        
//		        for(int i=0; i<lines.size(); i+=3){
//		        	String[] l1 = lines.get(i).split(";");
//		        	String[] l2 = lines.get(i+1).split(";");
//		        	String[] l3 = lines.get(i+2).split(";");
//		        	double t1 = Double.parseDouble(l1[3].trim());
//		        	double t2 = Double.parseDouble(l2[3].trim());
//		        	double t3 = Double.parseDouble(l3[3].trim());
//		        	
//		        	double totalTime = t1+t2+t3;
//		        	double avgTime = totalTime/3;
//		        	double variance = (Math.pow(t1-avgTime,2) + Math.pow(t2-avgTime,2) + Math.pow(t3-avgTime,2))/3;
//		        	double deviation = Math.sqrt(variance);
//		        	
//		        	int numbOutsideDev = 0;
//		        	
//		        	if(Math.abs(t1-avgTime)-deviation > 0 )
//		        		numbOutsideDev++;
//		        	if(Math.abs(t2-avgTime)-deviation > 0 )
//		        		numbOutsideDev++;
//		        	if(Math.abs(t3-avgTime)-deviation > 0 )
//		        		numbOutsideDev++;
//		        	
//		        	
//		        	writer.write(lines.get(i) + " " + avgTime + "; " + deviation + ";  " + (deviation/avgTime*100) + "; \n");	
//		        	
//		        }
		        
		        for(int i=0; i<lines.size(); i+=5){
		        	String[] l1 = lines.get(i).split(";");
		        	String[] l2 = lines.get(i+1).split(";");
		        	String[] l3 = lines.get(i+2).split(";");
		        	String[] l4 = lines.get(i+3).split(";");
		        	String[] l5 = lines.get(i+4).split(";");
		        	double t1 = Double.parseDouble(l1[3].trim());
		        	double t2 = Double.parseDouble(l2[3].trim());
		        	double t3 = Double.parseDouble(l3[3].trim());
		        	double t4 = Double.parseDouble(l4[3].trim());
		        	double t5 = Double.parseDouble(l5[3].trim());
		        	
		        	double totalTime = t1+t2+t3+t4+t5;
		        	double avgTime = totalTime/5;
		        	double variance = (Math.pow(t1-avgTime,2) + Math.pow(t2-avgTime,2) + Math.pow(t3-avgTime,2) + Math.pow(t4-avgTime,2) + Math.pow(t5-avgTime,2))/5;
		        	double deviation = Math.sqrt(variance);
		        	
		        	int numbOutsideDev = 0;
		        	
		        	if(Math.abs(t1-avgTime)-deviation > 0 )
		        		numbOutsideDev++;
		        	if(Math.abs(t2-avgTime)-deviation > 0 )
		        		numbOutsideDev++;
		        	if(Math.abs(t3-avgTime)-deviation > 0 )
		        		numbOutsideDev++;
		        	if(Math.abs(t4-avgTime)-deviation > 0 )
		        		numbOutsideDev++;
		        	if(Math.abs(t5-avgTime)-deviation > 0 )
		        		numbOutsideDev++;
		        	
		        	writer.write(lines.get(i) + " " + avgTime + "; " + deviation + ";  " + (deviation/avgTime*100) + "; \n");	
		        	
		        }
		        
//		        // For average of averages
//		        for(int i=0; i<lines.size(); i+=6){
//		        	String[] l1 = lines.get(i).split(";");
//		        	String[] l2 = lines.get(i+1).split(";");
//		        	String[] l3 = lines.get(i+2).split(";");
//		        	String[] l4 = lines.get(i+3).split(";");
//		        	String[] l5 = lines.get(i+4).split(";");
//		        	String[] l6 = lines.get(i+5).split(";");
//		        	double t1 = Double.parseDouble(l1[5].trim())/(Double.parseDouble(l1[2].trim())*log2(Double.parseDouble(l1[1].trim())));
//		        	double t2 = Double.parseDouble(l2[5].trim())/(Double.parseDouble(l2[2].trim())*log2(Double.parseDouble(l2[1].trim())));
//		        	double t3 = Double.parseDouble(l3[5].trim())/(Double.parseDouble(l3[2].trim())*log2(Double.parseDouble(l3[1].trim())));
//		        	double t4 = Double.parseDouble(l4[5].trim())/(Double.parseDouble(l4[2].trim())*log2(Double.parseDouble(l4[1].trim())));
//		        	double t5 = Double.parseDouble(l5[5].trim())/(Double.parseDouble(l5[2].trim())*log2(Double.parseDouble(l5[1].trim())));
//		        	double t6 = Double.parseDouble(l6[5].trim())/(Double.parseDouble(l6[2].trim())*log2(Double.parseDouble(l6[1].trim())));
//		        	
//		        	double totalTime = t1+t2+t3+t4+t5+t6;
//		        	double avgTime = totalTime/6;
//		        	double variance = (Math.pow(t1-avgTime,2) + Math.pow(t2-avgTime,2) + Math.pow(t3-avgTime,2) + Math.pow(t4-avgTime,2) + Math.pow(t5-avgTime,2) + Math.pow(t6-avgTime,2))/6;
//		        	double deviation = Math.sqrt(variance);
//		        	
//		        	int numbOutsideDev = 0;
//		        	
//		        	if(Math.abs(t1-avgTime)-deviation > 0 )
//		        		numbOutsideDev++;
//		        	if(Math.abs(t2-avgTime)-deviation > 0 )
//		        		numbOutsideDev++;
//		        	if(Math.abs(t3-avgTime)-deviation > 0 )
//		        		numbOutsideDev++;
//		        	if(Math.abs(t4-avgTime)-deviation > 0 )
//		        		numbOutsideDev++;
//		        	if(Math.abs(t5-avgTime)-deviation > 0 )
//		        		numbOutsideDev++;
//		        	
//		        	writer.write(lines.get(i) + " " + avgTime + "; " + deviation + ";  " + (deviation/avgTime*100) + "; \n");	
//		        	
//		        }
			    
//		        for(int i=0; i<lines.size(); i+=10){
//		        	String[] l1 = lines.get(i).split(";");
//		        	String[] l2 = lines.get(i+1).split(";");
//		        	String[] l3 = lines.get(i+2).split(";");
//		        	String[] l4 = lines.get(i+3).split(";");
//		        	String[] l5 = lines.get(i+4).split(";");
//		        	String[] l6 = lines.get(i+5).split(";");
//		        	String[] l7 = lines.get(i+6).split(";");
//		        	String[] l8 = lines.get(i+7).split(";");
//		        	String[] l9 = lines.get(i+8).split(";");
//		        	String[] l10 = lines.get(i+9).split(";");
//		        	
//		        	double t1 = Double.parseDouble(l1[3].trim());
//		        	double t2 = Double.parseDouble(l2[3].trim());
//		        	double t3 = Double.parseDouble(l3[3].trim());
//		        	double t4 = Double.parseDouble(l4[3].trim());
//		        	double t5 = Double.parseDouble(l5[3].trim());
//		        	double t6 = Double.parseDouble(l6[3].trim());
//		        	double t7 = Double.parseDouble(l7[3].trim());
//		        	double t8 = Double.parseDouble(l8[3].trim());
//		        	double t9 = Double.parseDouble(l9[3].trim());
//		        	double t10 = Double.parseDouble(l10[3].trim());
//		        	
//		        	double totalTime = t1+t2+t3+t4+t5+t6+t7+t8+t9+t10;
//		        	double avgTime = totalTime/10;
//		        	double variance1 = (Math.pow(t1-avgTime,2) + Math.pow(t2-avgTime,2) + Math.pow(t3-avgTime,2) + Math.pow(t4-avgTime,2) + Math.pow(t5-avgTime,2));
//		        	double variance2 = (Math.pow(t6-avgTime,2) + Math.pow(t7-avgTime,2) + Math.pow(t8-avgTime,2) + Math.pow(t9-avgTime,2) + Math.pow(t10-avgTime,2));
//		        	double variance = (variance1+variance2)/10;
//		        	double deviation = Math.sqrt(variance);
//		        	
//		        	int numbOutsideDev = 0;
//		        	
//		        	if(Math.abs(t1-avgTime)-deviation > 0 )
//		        		numbOutsideDev++;
//		        	if(Math.abs(t2-avgTime)-deviation > 0 )
//		        		numbOutsideDev++;
//		        	if(Math.abs(t3-avgTime)-deviation > 0 )
//		        		numbOutsideDev++;
//		        	if(Math.abs(t4-avgTime)-deviation > 0 )
//		        		numbOutsideDev++;
//		        	if(Math.abs(t5-avgTime)-deviation > 0 )
//		        		numbOutsideDev++;
//		        	
//		        	writer.write(lines.get(i) + " " + avgTime + "; " + deviation + ";  " + (deviation/avgTime*100) + "; \n");	
//		        	
//		        }
		        
//		        for(int i=0; i<lines.size(); i+=15){
//	        	String[] l1 = lines.get(i).split(";");
//	        	String[] l2 = lines.get(i+1).split(";");
//	        	String[] l3 = lines.get(i+2).split(";");
//	        	String[] l4 = lines.get(i+3).split(";");
//	        	String[] l5 = lines.get(i+4).split(";");
//	        	String[] l6 = lines.get(i+5).split(";");
//	        	String[] l7 = lines.get(i+6).split(";");
//	        	String[] l8 = lines.get(i+7).split(";");
//	        	String[] l9 = lines.get(i+8).split(";");
//	        	String[] l10 = lines.get(i+9).split(";");
//	        	String[] l11 = lines.get(i+10).split(";");
//	        	String[] l12 = lines.get(i+11).split(";");
//	        	String[] l13 = lines.get(i+12).split(";");
//	        	String[] l14 = lines.get(i+13).split(";");
//	        	String[] l15 = lines.get(i+14).split(";");
//	        	
//	        	double t1 = Double.parseDouble(l1[3].trim());
//	        	double t2 = Double.parseDouble(l2[3].trim());
//	        	double t3 = Double.parseDouble(l3[3].trim());
//	        	double t4 = Double.parseDouble(l4[3].trim());
//	        	double t5 = Double.parseDouble(l5[3].trim());
//	        	double t6 = Double.parseDouble(l6[3].trim());
//	        	double t7 = Double.parseDouble(l7[3].trim());
//	        	double t8 = Double.parseDouble(l8[3].trim());
//	        	double t9 = Double.parseDouble(l9[3].trim());
//	        	double t10 = Double.parseDouble(l10[3].trim());
//	        	double t11 = Double.parseDouble(l11[3].trim());
//	        	double t12 = Double.parseDouble(l12[3].trim());
//	        	double t13 = Double.parseDouble(l13[3].trim());
//	        	double t14 = Double.parseDouble(l14[3].trim());
//	        	double t15 = Double.parseDouble(l15[3].trim());
//	        	
//	        	double totalTime = t1+t2+t3+t4+t5+t6+t7+t8+t9+t10+t11+t12+t13+t14+t15;
//	        	double avgTime = totalTime/15;
//	        	double variance1 = (Math.pow(t1-avgTime,2) + Math.pow(t2-avgTime,2) + Math.pow(t3-avgTime,2) + Math.pow(t4-avgTime,2) + Math.pow(t5-avgTime,2));
//	        	double variance2 = (Math.pow(t6-avgTime,2) + Math.pow(t7-avgTime,2) + Math.pow(t8-avgTime,2) + Math.pow(t9-avgTime,2) + Math.pow(t10-avgTime,2));
//	        	double variance3 = (Math.pow(t11-avgTime,2) + Math.pow(t12-avgTime,2) + Math.pow(t13-avgTime,2) + Math.pow(t14-avgTime,2) + Math.pow(t15-avgTime,2));
//	        	double variance = (variance1+variance2+variance3)/15;
//	        	double deviation = Math.sqrt(variance);
//	        	
//	        	int numbOutsideDev = 0;
//	        	
//	        	if(Math.abs(t1-avgTime)-deviation > 0 )
//	        		numbOutsideDev++;
//	        	if(Math.abs(t2-avgTime)-deviation > 0 )
//	        		numbOutsideDev++;
//	        	if(Math.abs(t3-avgTime)-deviation > 0 )
//	        		numbOutsideDev++;
//	        	if(Math.abs(t4-avgTime)-deviation > 0 )
//	        		numbOutsideDev++;
//	        	if(Math.abs(t5-avgTime)-deviation > 0 )
//	        		numbOutsideDev++;
//	        	
//	        	writer.write(lines.get(i) + " " + avgTime + "; " + deviation + ";  " + (deviation/avgTime*100) + "; \n");	
//	        	
//	        }
		        
			    writer.flush();	
			    writer.close();	
		    } finally {
		        br.close();
		    }
	}
	
	public static double log2(double num){
		return Math.log(num)/Math.log(2);
	}
}
