package Experiments;

import java.io.IOException;

public class CombinePostProcess {

	
	public static void main(String[] args) throws IOException{
		SortData sort = new SortData();
		RemoveTwoFirst removeTwo = new RemoveTwoFirst();
		AverageWithVariance avg = new AverageWithVariance();
		AvgMultipleParameters avgMultiParam = new AvgMultipleParameters();
		
//		WorstCase_SuffixArray_N(1024).csv
//		WorstCase_SuffixArray_N(4096).csv
//		WorstCase_SuffixArray_N(16384).csv
//		WorstCase_SuffixArray_N(65536).csv
//		WorstCase_SuffixArray_N(262144).csv
//		WorstCase_SuffixArray_N(1048576).csv
		
//		WorstCase_SuffixArray_N(256).csv
//		WorstCase_SuffixArray_N(1024).csv
//		WorstCase_SuffixArray_N(4096).csv
//		WorstCase_SuffixArray_N(16384).csv
//		WorstCase_SuffixArray_N(65536).csv
//		WorstCase_SuffixArray_N(262144).csv
		
		
//		WorstCase_SuffixArray_N(128).csv
//		WorstCase_SuffixArray_N(256).csv
//		WorstCase_SuffixArray_N(512).csv
//		WorstCase_SuffixArray_N(1024).csv
//		WorstCase_SuffixArray_N(2048).csv
//		WorstCase_SuffixArray_N(4096).csv
//		WorstCase_SuffixArray_N(8192).csv
//		WorstCase_SuffixArray_N(16384).csv
//		WorstCase_SuffixArray_N(32768).csv
//		WorstCase_SuffixArray_N(65536).csv
//		WorstCase_SuffixArray_N(131072).csv
//		WorstCase_SuffixArray_N(262144).csv
//		WorstCase_SuffixArray_N(524288).csv
//		WorstCase_SuffixArray_N(1048576).csv

		
		String filename = "bin/AV1611Bible_2pow21.txt_2_compare_NotIn_65536";
		
		String[] param = new String[]{filename};
		
//		sort.main(param);
		removeTwo.main(param);
		avgMultiParam.main(param);
//		avg.main(param);
		
	}
}
