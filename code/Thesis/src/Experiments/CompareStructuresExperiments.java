package Experiments;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

import javax.xml.stream.events.Characters;

import SuffixArray.SuffixArray;
import SuffixTray.SuffixTray;
import SuffixTreeNodes.NodeTypes.ChildrenStructures;
import SuffixTreeUkkonen.SuffixTree;

public class CompareStructuresExperiments {

	public static String text;
	public static int[][] testStrings;
	public static char[] charNotIn;
	
	
	public static void main(String[] args) throws IOException{
		ArrayList<String> filenames = new ArrayList<String>();
//		filenames.add("../../texts/genome1__250000.fa");
//		filenames.add("../../../texts/genome1.fa");
//		filenames.add("../../../texts/genome2.fa");
//		filenames.add("../../../texts/genome3.fa");
//		filenames.add("../../../texts/genome4.fa");
//		filenames.add("../../../texts/genome5.fa");
//		filenames.add("../../../texts/genome6.fa");
		filenames.add("../../../texts/AV1611Bible_2pow21.txt");
		filenames.add("../../../texts/AV1611Bible_2pow21_lowerCase.txt");
//		filenames.add("../../../texts/AV1611Bible_rest.txt");
//		filenames.add("../../../texts/AV1611Bible_rest_lowerCase.txt");
		
		int[] textSizes = new int[]{(int) Math.pow(2, 16),(int) Math.pow(2, 18)};
		
		text = readInText("../../../texts/genome1.fa", false, (int) Math.pow(2, 18));
		NOT_IN_test2000TO60000("../../../texts/genome1.fa");
		
		for(String filename : filenames){
			text = readInText(filename, true, 0);
//			NOT_IN_test0TO100(filename);
//			NOT_IN_test100TO2000(filename);
			NOT_IN_test2000TO60000(filename);
//			test0TO100(filename);
//			test100TO2000(filename);
//			test2000TO60000(filename);
			for(int size=0; size<textSizes.length; size++){
				text = readInText(filename, false, textSizes[size]);
//				NOT_IN_test0TO100(filename);
//				NOT_IN_test100TO2000(filename);
				NOT_IN_test2000TO60000(filename);
//				test0TO100(filename);
//				test100TO2000(filename);
//				test2000TO60000(filename);	
			}
		}
		
	}
	
	public static void createTestStrings(int amount, int length){
		testStrings = new int[amount][2];
		Random rnd = new Random();
		for(int i=0; i<amount; i++){
			int startIndex = rnd.nextInt(text.length()-length+1);
			testStrings[i] = new int[]{startIndex, startIndex+length};
		}
	}

	public static void createTestStringsNotIn(int amount, int length, ArrayList<Character> alphabet, SuffixTree suffix_tree){
		testStrings = new int[amount][2];
		charNotIn = new char[amount];
		int alphabet_size = alphabet.size();
		Random rnd = new Random();
		for(int i=0; i<amount; i++){
			int startIndex = rnd.nextInt(text.length()-length+1);
			int c = rnd.nextInt(alphabet_size);
			String test = text.substring(startIndex, startIndex+length-1) + alphabet.get(c);
			while(suffix_tree.search(test) >= 0){
				startIndex = rnd.nextInt(text.length()-length+1);
				c = rnd.nextInt(alphabet_size);
				test = text.substring(startIndex, startIndex+length-1) + alphabet.get(c);
			}
			testStrings[i] = new int[]{startIndex, startIndex+length-1};
			charNotIn[i] = alphabet.get(c);
		}
	}
	
	public static String readInText(String filename, boolean fullSize, int size) throws IOException{
		  BufferedReader br = new BufferedReader(new FileReader(filename));
		  String text = "";
		    try {
		        StringBuilder sb = new StringBuilder();
		        String line = br.readLine();

		        while (line != null) {
		        	if(!line.startsWith(">"))
		        		sb.append(line);
		            line = br.readLine();
		        }
		        text = sb.toString();
		    } finally {
		        br.close();
		    }
		    System.out.println("TEST:" + text.length());
		    if(fullSize)
		    	return text;
		    else
		    	return text.substring(0,size);
	}
	
	public static void NOT_IN_test0TO100(String textName) throws IOException{
		int amountOfTests = 500000; 
		System.out.println("STARTING TESTS");
		SuffixTree suffix_tree = new SuffixTree(text, ChildrenStructures.BINARY);
		SuffixArray suffix_array = new SuffixArray();
		suffix_array.construct_SuffixArray_from_SuffixTree(suffix_tree);
		SuffixTray suffix_tray = new SuffixTray(suffix_tree, suffix_array);
		
		ArrayList<Character> alphabet = new ArrayList<Character>();
		int[] alphabet_array = suffix_tray.alphabet_array;
		for(int i=0; i<alphabet_array.length; i++){
			if(alphabet_array[i] != 0){
				alphabet.add((char) i);
			}
		}
		
		///////////////// File to write results in ///////////////////////
//		String filename = textName.substring(12) + "0_compare_" + text.length() + ".csv";
		String filename = textName.substring(15) + "_0_compare_NotIn_" + text.length() + ".csv";
		System.out.println(filename);
		
		File file = new File(filename);

		BufferedWriter writer  = new BufferedWriter(new FileWriter(file));
		writer.write("textSize; alphaSize; querySize; timeArray; timeTray; expCount; iterations;\n");
		
		//////////////////////////////////////////////////////////////////
		
		for(int query_length = 10; query_length <= 100; query_length+=5)
		{
			System.out.println("COMPARING STRUCTURES FILE: " + textName + "  : QUERY LENGTH:" + query_length);
			createTestStringsNotIn(amountOfTests, query_length, alphabet, suffix_tree);
			for(int exp_Count=0; exp_Count<7; exp_Count++)
			{
				int result;
				
				long running_time_array = 0;
				for(int i=0; i<testStrings.length; i++){
					String query = text.substring(testStrings[i][0], testStrings[i][1]) + charNotIn[i];
					
					long start = System.currentTimeMillis();
					result = suffix_array.search(query);
					long estimatedTime = System.currentTimeMillis() - start;
					running_time_array += estimatedTime;
					
					if(result >= 0){
						System.out.println("result:" + result);
						writer.close();
						throw new RuntimeException("Error in search - suffix array");
					}
				}
				
				long running_time_tray = 0;
				for(int i=0; i<testStrings.length; i++){
					String query = text.substring(testStrings[i][0], testStrings[i][1]) + charNotIn[i];
					
					long start = System.currentTimeMillis();
					result = suffix_tray.search(query);
					long estimatedTime = System.currentTimeMillis() - start;
					running_time_tray += estimatedTime;
					
					if(result >= 0){
						System.out.println("result:" + result);
						writer.close();
						throw new RuntimeException("Error in search - suffix tray");
					}
				}
				
				writer.write(text.length() + "; " + suffix_tray.alphabet_size + "; " + query_length + "; " + running_time_array + "; " + running_time_tray  + "; " + exp_Count + "; " + testStrings.length + "; \n");
				writer.flush();	
			}
		}
		writer.close();
	}

	public static void NOT_IN_test100TO2000(String textName) throws IOException{
		int amountOfTests = 250000; 
		System.out.println("STARTING TESTS");
		SuffixTree suffix_tree = new SuffixTree(text, ChildrenStructures.BINARY);
		SuffixArray suffix_array = new SuffixArray();
		suffix_array.construct_SuffixArray_from_SuffixTree(suffix_tree);
		SuffixTray suffix_tray = new SuffixTray(suffix_tree, suffix_array);
		
		ArrayList<Character> alphabet = new ArrayList<Character>();
		int[] alphabet_array = suffix_tray.alphabet_array;
		for(int i=0; i<alphabet_array.length; i++){
			if(alphabet_array[i] != 0){
				alphabet.add((char)alphabet_array[i]);
			}
		}
		
		///////////////// File to write results in ///////////////////////
//		String filename = textName.substring(12) + "_compare_" + text.length() + ".csv";
		String filename = textName.substring(15) + "_compare_NotIn_" + text.length() + ".csv";
		System.out.println(filename);
		
		File file = new File(filename);

		BufferedWriter writer  = new BufferedWriter(new FileWriter(file));
		writer.write("textSize; alphaSize; querySize; timeArray; timeTray; expCount; iterations;\n");
		
		//////////////////////////////////////////////////////////////////
		
		for(int query_length = 100; query_length <= 2000; query_length+=100)
		{
			System.out.println("COMPARING STRUCTURES FILE: " + textName + "  : QUERY LENGTH:" + query_length);
			createTestStringsNotIn(amountOfTests, query_length, alphabet, suffix_tree);
			for(int exp_Count=0; exp_Count<7; exp_Count++)
			{
				int result;

				long running_time_array = 0;
				for(int i=0; i<testStrings.length; i++){
					String query = text.substring(testStrings[i][0], testStrings[i][1]) + charNotIn[i];
					
					long start = System.currentTimeMillis();
					result = suffix_array.search(query);
					long estimatedTime = System.currentTimeMillis() - start;
					running_time_array += estimatedTime;
					
					if(result >= 0){
						System.out.println("result:" + result);
						writer.close();
						throw new RuntimeException("Error in search - suffix array");
					}
				}
				
				long running_time_tray = 0;
				for(int i=0; i<testStrings.length; i++){
					String query = text.substring(testStrings[i][0], testStrings[i][1]) + charNotIn[i];
					
					long start = System.currentTimeMillis();
					result = suffix_tray.search(query);
					long estimatedTime = System.currentTimeMillis() - start;
					running_time_tray += estimatedTime;
					
					if(result >= 0){
						System.out.println("result:" + result);
						writer.close();
						throw new RuntimeException("Error in search - suffix tray");
					}
				}
				
				writer.write(text.length() + "; " + suffix_tray.alphabet_size + "; " + query_length + "; " + running_time_array + "; " + running_time_tray  + "; " + exp_Count + "; " + testStrings.length + "; \n");
				writer.flush();	
			}
		}
		writer.close();
	}

	public static void NOT_IN_test2000TO60000(String textName) throws IOException{
		int amountOfTests = 62500;
		System.out.println("STARTING TESTS 2");
		SuffixTree suffix_tree = new SuffixTree(text, ChildrenStructures.BINARY);
		SuffixArray suffix_array = new SuffixArray();
		suffix_array.construct_SuffixArray_from_SuffixTree(suffix_tree);
		SuffixTray suffix_tray = new SuffixTray(suffix_tree, suffix_array);
		
		ArrayList<Character> alphabet = new ArrayList<Character>();
		int[] alphabet_array = suffix_tray.alphabet_array;
		for(int i=0; i<alphabet_array.length; i++){
			if(alphabet_array[i] != 0){
				alphabet.add((char)alphabet_array[i]);
			}
		}
		
		///////////////// File to write results in ///////////////////////
//		String filename = textName.substring(12) + "_compare_" + text.length() + ".csv";
		String filename = textName.substring(15) + "_2_compare_NotIn_" + text.length() + ".csv";
		System.out.println(filename);
		
		File file = new File(filename);

		BufferedWriter writer  = new BufferedWriter(new FileWriter(file));
		writer.write("textSize; alphaSize; querySize; timeArray; timeTray; expCount; iterations; \n");
		
		//////////////////////////////////////////////////////////////////
		
		for(int query_length = 4000; query_length <= 60000; query_length+=4000)
		{
			System.out.println("COMPARING STRUCTURES FILE: " + textName + "  : QUERY LENGTH:" + query_length);
			createTestStringsNotIn(amountOfTests, query_length, alphabet, suffix_tree);
			for(int exp_Count=0; exp_Count<7; exp_Count++)
			{
				int result;

				long running_time_array = 0;
				for(int i=0; i<testStrings.length; i++){
					String query = text.substring(testStrings[i][0], testStrings[i][1]) + charNotIn[i];
					
					long start = System.currentTimeMillis();
					result = suffix_array.search(query);
					long estimatedTime = System.currentTimeMillis() - start;
					running_time_array += estimatedTime;
					
					if(result >= 0){
						System.out.println("result:" + result);
						writer.close();
						throw new RuntimeException("Error in search - suffix array");
					}
				}
				
				long running_time_tray = 0;
				for(int i=0; i<testStrings.length; i++){
					String query = text.substring(testStrings[i][0], testStrings[i][1]) + charNotIn[i];
					
					long start = System.currentTimeMillis();
					result = suffix_tray.search(query);
					long estimatedTime = System.currentTimeMillis() - start;
					running_time_tray += estimatedTime;
					
					if(result >= 0){
						System.out.println("result:" + result);
						writer.close();
						throw new RuntimeException("Error in search - suffix tray");
					}
				}
				
				writer.write(text.length() + "; " + suffix_tray.alphabet_size + "; " + query_length + "; "+ running_time_array + "; " + running_time_tray  + "; " + exp_Count + "; " + testStrings.length + "; \n");
				writer.flush();	
			}
		}
		writer.close();
	}
	
	public static void test0TO100(String textName) throws IOException{
		int amountOfTests = 500000; 
		System.out.println("STARTING TESTS");
		SuffixTree suffix_tree = new SuffixTree(text, ChildrenStructures.BINARY);
		SuffixArray suffix_array = new SuffixArray();
		suffix_array.construct_SuffixArray_from_SuffixTree(suffix_tree);
		SuffixTray suffix_tray = new SuffixTray(suffix_tree, suffix_array);
		///////////////// File to write results in ///////////////////////
//		String filename = textName.substring(12) + "0_compare_" + text.length() + ".csv";
		String filename = textName.substring(15) + "_0_compare_" + text.length() + ".csv";
		System.out.println(filename);
		
		File file = new File(filename);

		BufferedWriter writer  = new BufferedWriter(new FileWriter(file));
		writer.write("textSize; alphaSize; querySize; timeTree; timeArray; timeTray; expCount; iterations;\n");
		
		//////////////////////////////////////////////////////////////////
		
		for(int query_length = 5; query_length <= 100; query_length+=5)
		{
			System.out.println("COMPARING STRUCTURES FILE: " + textName + "  : QUERY LENGTH:" + query_length);
			createTestStrings(amountOfTests, query_length);
			for(int exp_Count=0; exp_Count<7; exp_Count++)
			{
				int result;

				long running_time_tree = 0;
				for(int i=0; i<testStrings.length; i++){
					String query = text.substring(testStrings[i][0], testStrings[i][1]);
					long start = System.currentTimeMillis();
					result = suffix_tree.search(query);
					long estimatedTime = System.currentTimeMillis() - start;
					running_time_tree += estimatedTime;
					
					if(result < 0){
						System.out.println("result:" + result);
						writer.close();
						throw new RuntimeException("Error in search - suffix tree");
					}
				}
		
				long running_time_array = 0;
				for(int i=0; i<testStrings.length; i++){
					String query = text.substring(testStrings[i][0], testStrings[i][1]);
					
					long start = System.currentTimeMillis();
					result = suffix_array.search(query);
					long estimatedTime = System.currentTimeMillis() - start;
					running_time_array += estimatedTime;
					
					if(result < 0){
						System.out.println("result:" + result);
						writer.close();
						throw new RuntimeException("Error in search - suffix array");
					}
				}
				
				long running_time_tray = 0;
				for(int i=0; i<testStrings.length; i++){
					String query = text.substring(testStrings[i][0], testStrings[i][1]);
					
					long start = System.currentTimeMillis();
					result = suffix_tray.search(query);
					long estimatedTime = System.currentTimeMillis() - start;
					running_time_tray += estimatedTime;
					
					if(result < 0){
						System.out.println("result:" + result);
						writer.close();
						throw new RuntimeException("Error in search - suffix tray");
					}
				}
				
				writer.write(text.length() + "; " + suffix_tray.alphabet_size + "; " + query_length + "; " + running_time_tree + "; " + running_time_array + "; " + running_time_tray  + "; " + exp_Count + "; " + testStrings.length + "; \n");
				writer.flush();	
			}
		}
		writer.close();
	}
	
	public static void test100TO2000(String textName) throws IOException{
		int amountOfTests = 250000; 
		System.out.println("STARTING TESTS");
		SuffixTree suffix_tree = new SuffixTree(text, ChildrenStructures.BINARY);
		SuffixArray suffix_array = new SuffixArray();
		suffix_array.construct_SuffixArray_from_SuffixTree(suffix_tree);
		SuffixTray suffix_tray = new SuffixTray(suffix_tree, suffix_array);
		///////////////// File to write results in ///////////////////////
//		String filename = textName.substring(12) + "_compare_" + text.length() + ".csv";
		String filename = textName.substring(15) + "_compare_" + text.length() + ".csv";
		System.out.println(filename);
		
		File file = new File(filename);

		BufferedWriter writer  = new BufferedWriter(new FileWriter(file));
		writer.write("textSize; alphaSize; querySize; timeTree; timeArray; timeTray; expCount; iterations;\n");
		
		//////////////////////////////////////////////////////////////////
		
		for(int query_length = 100; query_length <= 2000; query_length+=100)
		{
			System.out.println("COMPARING STRUCTURES FILE: " + textName + "  : QUERY LENGTH:" + query_length);
			createTestStrings(amountOfTests, query_length);
			for(int exp_Count=0; exp_Count<7; exp_Count++)
			{
				int result;

				long running_time_tree = 0;
				for(int i=0; i<testStrings.length; i++){
					String query = text.substring(testStrings[i][0], testStrings[i][1]);
					long start = System.currentTimeMillis();
					result = suffix_tree.search(query);
					long estimatedTime = System.currentTimeMillis() - start;
					running_time_tree += estimatedTime;
					
					if(result < 0){
						System.out.println("result:" + result);
						writer.close();
						throw new RuntimeException("Error in search - suffix tree");
					}
				}
		
				long running_time_array = 0;
				for(int i=0; i<testStrings.length; i++){
					String query = text.substring(testStrings[i][0], testStrings[i][1]);
					
					long start = System.currentTimeMillis();
					result = suffix_array.search(query);
					long estimatedTime = System.currentTimeMillis() - start;
					running_time_array += estimatedTime;
					
					if(result < 0){
						System.out.println("result:" + result);
						writer.close();
						throw new RuntimeException("Error in search - suffix array");
					}
				}
				
				long running_time_tray = 0;
				for(int i=0; i<testStrings.length; i++){
					String query = text.substring(testStrings[i][0], testStrings[i][1]);
					
					long start = System.currentTimeMillis();
					result = suffix_tray.search(query);
					long estimatedTime = System.currentTimeMillis() - start;
					running_time_tray += estimatedTime;
					
					if(result < 0){
						System.out.println("result:" + result);
						writer.close();
						throw new RuntimeException("Error in search - suffix tray");
					}
				}
				
				writer.write(text.length() + "; " + suffix_tray.alphabet_size + "; " + query_length + "; " + running_time_tree + "; " + running_time_array + "; " + running_time_tray  + "; " + exp_Count + "; " + testStrings.length + "; \n");
				writer.flush();	
			}
		}
		writer.close();
	}
	
	public static void test2000TO60000(String textName) throws IOException{
		int amountOfTests = 62500;
		System.out.println("STARTING TESTS 2");
		SuffixTree suffix_tree = new SuffixTree(text, ChildrenStructures.BINARY);
		SuffixArray suffix_array = new SuffixArray();
		suffix_array.construct_SuffixArray_from_SuffixTree(suffix_tree);
		SuffixTray suffix_tray = new SuffixTray(suffix_tree, suffix_array);
		///////////////// File to write results in ///////////////////////
//		String filename = textName.substring(12) + "_compare_" + text.length() + ".csv";
		String filename = textName.substring(15) + "_2_compare_" + text.length() + ".csv";
		System.out.println(filename);
		
		File file = new File(filename);

		BufferedWriter writer  = new BufferedWriter(new FileWriter(file));
		writer.write("textSize; alphaSize; querySize; timeTree; timeArray; timeTray; expCount; iterations; \n");
		
		//////////////////////////////////////////////////////////////////
		
		for(int query_length = 4000; query_length <= 60000; query_length+=4000)
		{
			System.out.println("COMPARING STRUCTURES FILE: " + textName + "  : QUERY LENGTH:" + query_length);
			createTestStrings(amountOfTests, query_length);
			for(int exp_Count=0; exp_Count<7; exp_Count++)
			{
				int result;

				long running_time_tree = 0;
				for(int i=0; i<testStrings.length; i++){
					String query = text.substring(testStrings[i][0], testStrings[i][1]);
					long start = System.currentTimeMillis();
					result = suffix_tree.search(query);
					long estimatedTime = System.currentTimeMillis() - start;
					running_time_tree += estimatedTime;
					
					if(result < 0){
						System.out.println("result:" + result);
						writer.close();
						throw new RuntimeException("Error in search - suffix tree");
					}
				}
		
				long running_time_array = 0;
				for(int i=0; i<testStrings.length; i++){
					String query = text.substring(testStrings[i][0], testStrings[i][1]);
					
					long start = System.currentTimeMillis();
					result = suffix_array.search(query);
					long estimatedTime = System.currentTimeMillis() - start;
					running_time_array += estimatedTime;
					
					if(result < 0){
						System.out.println("result:" + result);
						writer.close();
						throw new RuntimeException("Error in search - suffix array");
					}
				}
				
				long running_time_tray = 0;
				for(int i=0; i<testStrings.length; i++){
					String query = text.substring(testStrings[i][0], testStrings[i][1]);
					
					long start = System.currentTimeMillis();
					result = suffix_tray.search(query);
					long estimatedTime = System.currentTimeMillis() - start;
					running_time_tray += estimatedTime;
					
					if(result < 0){
						System.out.println("result:" + result);
						writer.close();
						throw new RuntimeException("Error in search - suffix tray");
					}
				}
				
				writer.write(text.length() + "; " + suffix_tray.alphabet_size + "; " + query_length + "; " + running_time_tree + "; " + running_time_array + "; " + running_time_tray  + "; " + exp_Count + "; " + testStrings.length + "; \n");
				writer.flush();	
			}
		}
		writer.close();
	}
	
}
