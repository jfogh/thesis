package SuffixTreeUkkonen;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Stack;

import SuffixTreeNodes.NodeTypes.ChildrenStructures;
import SuffixTreeNodes.SuffixTreeNodeAbstract;
import SuffixTreeNodes.SuffixTreeNodeBinary;
import SuffixTreeNodes.SuffixTreeNodeHashMap;
import SuffixTreeNodes.SuffixTreeNodeLinear;

public class SuffixTree {

	public String text;
	private int text_length;
	public SuffixTreeNodeAbstract root_node;
	private int innerNodeId = -1;
	public int[] leafListIndex_to_nodeId;
	public int[] nodeId_to_leafListIndex;
	private int leafIndex = 0;
	private ChildrenStructures children_structure = null;
	
	public SuffixTree(String _text, ChildrenStructures children_structure){
		text = _text + "$";
		text_length = text.length();
		leafListIndex_to_nodeId = new int[text_length];
		nodeId_to_leafListIndex = new int[text_length+1];
		this.children_structure = children_structure;
		
		generateTreeLinear();
		
//		System.out.println("DONE CONSTRUCTING");
//		long start = System.currentTimeMillis();
//		updateLeafListsRecursive(root_node);
		updateLeafListsIterative();
//		long end = System.currentTimeMillis();
//		System.out.println("RECURSIVE TIME:" + (end-start));
//		System.out.println("DONE UPDATING LEAVES");
	}

	public ArrayList<Integer> searchAll(String searchString){
		ArrayList<Integer> results = new ArrayList<Integer>();
		SuffixTreeNodeAbstract current = root_node;
		int search_string_index = 0;
		while(true){
			current = current.findMatchingChild(searchString.charAt(search_string_index));
			
			if(current != null){
				search_string_index++;
				if(search_string_index == searchString.length()){
					for(int index = current.leaf_list_start; index <= current.leaf_list_end; index++){
						results.add(leafListIndex_to_nodeId[index]-1);
					}
					return results;	
				}
				for(int i=1; i<current.edge_end_index-current.edge_start_index; i++){
					if(text.charAt(current.edge_start_index+i) != searchString.charAt(search_string_index)){
						return results;
					}else{
						search_string_index++;
						if(search_string_index == searchString.length()){
							for(int index = current.leaf_list_start; index <= current.leaf_list_end; index++){
								results.add(leafListIndex_to_nodeId[index]-1);
							}
							return results;	
						}
					}
				}
			}else{
				return results;
			}
		}		
	}
	
	public int search(String searchString){
		SuffixTreeNodeAbstract current = root_node;
		int search_string_index = 0;
		while(true){
			current = current.findMatchingChild(searchString.charAt(search_string_index));

			if(current != null){
				search_string_index++;
				if(search_string_index == searchString.length())
					return leafListIndex_to_nodeId[current.leaf_list_start];
				for(int i=1; i<current.edge_end_index-current.edge_start_index; i++){
					if(text.charAt(current.edge_start_index+i) != searchString.charAt(search_string_index)){
						return -1;
					}else{
						search_string_index++;
						if(search_string_index == searchString.length())
							return leafListIndex_to_nodeId[current.leaf_list_start];
					}
				}
			}else{
				return -1;
			}
		}		
	}
		
	// Using suffix-link - should be O(n) time - If this should be used, something should be uncommented in SuffixTreeNodeAbstract..... - this is not used as it hits outofmemoryeror, when using HashMap as datastructure
	public void generateTreeLinear(){
		SuffixTreeNodeAbstract root_node_parent = createSuffixTreeNode(0, null, -1, -1, ' ', 0);
		root_node = createSuffixTreeNode(0, root_node_parent, -1, -1, ' ', 0);
		root_node.suffixLink = root_node;
		root_node_parent.suffixLink = root_node;
		int jL = 0;
		
		
//		int max_depth  = 0;
//		int total_depth = 0;
		
		// Holds inner node that needs a suffix link in the next iteration
		SuffixTreeNodeAbstract need_suffix_link = null;
		// Holds the startpoint for the next iteration
		SuffixTreeNodeAbstract start_point = root_node;
		
		// iteration 0 er bootstrapped
		char firstNode_char = text.charAt(0);
		SuffixTreeNodeAbstract firstNode = createSuffixTreeNode(1, root_node, 0, text_length, firstNode_char, text_length-0);
		root_node.addChild(firstNode, firstNode_char);
		
		for(int i=0; i<text_length-1; i++){
			char c = text.charAt(i+1);
			
			for(int j= jL+1; j<=i; j++){
				// find j...i suffixes og tilf�j i+1 (ved at starte fra jL+1 undg�r vi at se p� leafs)
				SuffixTreeNodeAbstract current = start_point;
				int searchStringLength = i-j+1-start_point.depth;
				int looking_at_char = j+start_point.depth;
				boolean found = false;
//				int depth = 0;
				
				while(!found){
//					depth++;
//					total_depth++;
					SuffixTreeNodeAbstract matchingChild = current.findMatchingChild(text.charAt(looking_at_char));
					if(matchingChild != null){
						int edge_length = matchingChild.edge_end_index - matchingChild.edge_start_index;
						if(edge_length > searchStringLength){ // Ending on an edge
							if(c == text.charAt(matchingChild.edge_start_index + searchStringLength)){ 
								// Is allready in the tree!!!
								j = text_length+1; // The same as using jR, as we know the rest also exists.
								if(need_suffix_link != null){
									need_suffix_link.suffixLink = matchingChild;
									need_suffix_link = null;
								}
								start_point = current;
								found = true;
							}else{
								// inds�t indernode her and change children on nodes
								SuffixTreeNodeAbstract innerNode = createSuffixTreeNode(innerNodeId, current, matchingChild.edge_start_index, matchingChild.edge_start_index+searchStringLength, matchingChild.edge_char, current.depth+searchStringLength);
								innerNodeId--;
								if(need_suffix_link != null)
									need_suffix_link.suffixLink = innerNode;
								need_suffix_link = innerNode;
								start_point = innerNode.parent.suffixLink;
								SuffixTreeNodeAbstract newLeaf = createSuffixTreeNode(j+1, innerNode, i+1, text_length, c, innerNode.depth + text_length - (i+1));
								jL = j;
								innerNode.addChild(newLeaf, c); // Tilf�jer ny knude som barn til nye innerNode
								matchingChild.edge_char = text.charAt(matchingChild.edge_start_index + searchStringLength);
								matchingChild.edge_start_index = innerNode.edge_end_index;
								innerNode.addChild(matchingChild, matchingChild.edge_char); // Tilf�jer det matchede child, til innerNoden
								matchingChild.parent = innerNode;
								current.removeChild(matchingChild, matchingChild.edge_char);   // Fjerner det matchede child fra dens gamle parrent
								current.addChild(innerNode, text.charAt(innerNode.edge_start_index)); // Tilf�jer den nye innerNode til den gamle parrent.
								found = true;
							}
						}else if(edge_length == searchStringLength){ // Ending in a node
							SuffixTreeNodeAbstract matchingGrandChild = matchingChild.findMatchingChild(c);
							if(matchingGrandChild == null){ // does not allready exists
								SuffixTreeNodeAbstract newLeaf = createSuffixTreeNode(j+1, matchingChild, i+1, text_length, c, matchingChild.depth+text_length-(i+1));
								jL = j;
								matchingChild.addChild(newLeaf, c);
								if(need_suffix_link != null){
									need_suffix_link.suffixLink = current;
									need_suffix_link = null;
								}
								start_point = current.suffixLink;
								found = true;
							}else{ // allready exists in a grandchild node
								j = text_length+1; // The same as using jR, as we know the rest also exists.
								if(need_suffix_link != null){
									need_suffix_link.suffixLink = matchingChild;
									need_suffix_link = null;
								}
								start_point = matchingChild;
								found = true;
							}
						}else{ // Has to search through the child nodes
							searchStringLength -= edge_length;
							current = matchingChild;
							looking_at_char = looking_at_char + edge_length;
						}
					}else{ // No matching child, -> does not exists and end on an node - should be hint in the case with "grandchild"
						SuffixTreeNodeAbstract newLeaf = createSuffixTreeNode(j+1, current, i+1, text_length, c, current.depth + text_length - (i+1));
						current.addChild(newLeaf, c);
						jL = j;
						found = true;
						System.out.println("THIS CODE SHOULD NEVER BE REACHED!!!!!!!!");
						throw new RuntimeException("THIS CODE SHOULD NEVER BE REACHED!!!!!!!!");
					}
				}
//				max_depth = Math.max(max_depth, depth);
			}
			
			if(jL == i){
				// append i+1 (allready i+1, as it has leaved the for loop
				SuffixTreeNodeAbstract matchingChildInRoot = root_node.findMatchingChild(c);
				if(matchingChildInRoot == null){
					SuffixTreeNodeAbstract newLeaf = createSuffixTreeNode(i+2, root_node, i+1, text_length, c, text_length - (i+1));
					root_node.addChild(newLeaf, c);
					jL = i+1;
				}
				if(need_suffix_link != null){
					need_suffix_link.suffixLink = root_node;
					need_suffix_link = null;
				}
				start_point = root_node;
			}
		}
//		System.out.println("MAX DEPTH:" + max_depth);
//		System.out.println("TOTAL DEPTH:" + total_depth);
	}
	
	public void updateLeafListsRecursive(SuffixTreeNodeAbstract node){
	    List<SuffixTreeNodeAbstract> sortedChildren = new ArrayList<SuffixTreeNodeAbstract>(node.getChildren());
		Collections.sort(sortedChildren);
		for(int i=0; i<sortedChildren.size(); i++){
			SuffixTreeNodeAbstract child = sortedChildren.get(i);
			updateLeafListsRecursive(child);
			node.leaf_list_start = Math.min(node.leaf_list_start, child.leaf_list_start);
			node.leaf_list_end = Math.max(node.leaf_list_end, child.leaf_list_end);
		}
		if(node.getChildren().isEmpty()){
			leafListIndex_to_nodeId[leafIndex] = node.nodeId;
			nodeId_to_leafListIndex[node.nodeId] = leafIndex;
			node.leaf_list_start = leafIndex;
			node.leaf_list_end = leafIndex;
			leafIndex++;
		}
	}
	
	public void updateLeafListsIterative(){
		leafIndex = 0;
		ArrayList<SuffixTreeNodeAbstract> needs_to_get_updated = new ArrayList<SuffixTreeNodeAbstract>();
		Stack<SuffixTreeNodeAbstract> needs_to_look_at_children = new Stack<SuffixTreeNodeAbstract>();
		needs_to_look_at_children.push(root_node);
		needs_to_get_updated.add(root_node);
		
		while(!needs_to_look_at_children.isEmpty()){
			SuffixTreeNodeAbstract node = needs_to_look_at_children.pop();
			if(node.nodeId > 0){ // it is a leaf
				leafListIndex_to_nodeId[leafIndex] = node.nodeId;
				nodeId_to_leafListIndex[node.nodeId] = leafIndex;
				node.leaf_list_start = leafIndex;
				node.leaf_list_end = leafIndex;
				leafIndex++;
			}

			ArrayList<SuffixTreeNodeAbstract> children = new ArrayList<SuffixTreeNodeAbstract>(node.getChildren());
			Collections.sort(children);
			
			
			needs_to_get_updated.addAll(children);
			
			for(int i=children.size()-1; i >= 0; i--){
				SuffixTreeNodeAbstract child = children.get(i);
				needs_to_look_at_children.push(child);
			}
		}
		
		for(int i = needs_to_get_updated.size()-1; i>=0; i--){
			SuffixTreeNodeAbstract node = needs_to_get_updated.get(i);
			ArrayList<SuffixTreeNodeAbstract> children = new ArrayList<SuffixTreeNodeAbstract>(node.getChildren());
			
			for(int j=0; j<children.size(); j++){ // This is an inner node
				SuffixTreeNodeAbstract child = children.get(j);
				node.leaf_list_start = Math.min(node.leaf_list_start, child.leaf_list_start);
				node.leaf_list_end = Math.max(node.leaf_list_end, child.leaf_list_end);
			}
		}
	}
	
	private SuffixTreeNodeAbstract createSuffixTreeNode(int nodeId, int edge_start, int edge_end, char edge_char){
		switch (children_structure) {
	        case LINEAR: return new SuffixTreeNodeLinear(nodeId, edge_start, edge_end, edge_char);
	        case BINARY: return new SuffixTreeNodeBinary(nodeId, edge_start, edge_end, edge_char);
	        case HASHMAP: return new SuffixTreeNodeHashMap(nodeId, edge_start, edge_end, edge_char);
	        default: return null;
		}
	}
	
	private SuffixTreeNodeAbstract createSuffixTreeNode(int nodeId, SuffixTreeNodeAbstract parent, int edge_start, int edge_end, char edge_char, int depth){
		switch (children_structure) {
	        case LINEAR: return new SuffixTreeNodeLinear(nodeId, parent, edge_start, edge_end, edge_char, depth);
	        case BINARY: return new SuffixTreeNodeBinary(nodeId, parent, edge_start, edge_end, edge_char, depth);
	        case HASHMAP: return new SuffixTreeNodeHashMap(nodeId, parent, edge_start, edge_end, edge_char, depth);
	        default: return null;
		}
	}
	
	public void printLeafLists(){
		for(int i = 0; i<leafListIndex_to_nodeId.length; i++){
			System.out.println("Index: " + i + " IS NODEID: " + leafListIndex_to_nodeId[i]);
		}
	}
	
	public void printNodeIdToLeafLists(){
		for(int i = 0; i<nodeId_to_leafListIndex.length; i++){
			System.out.println("Index: " + i + " IS NODEID: " + nodeId_to_leafListIndex[i]);
		}
	}
	
	public void toDot(boolean printToFile){
		if(printToFile){
			PrintWriter writer = null;
			try {
				writer = new PrintWriter("SuffixTree.dot", "UTF-8");
			} catch (Exception e) {
				e.printStackTrace();
			}
			writer.println("digraph SuffixTree {");
			Queue<SuffixTreeNodeAbstract> jobQueue = new LinkedList<SuffixTreeNodeAbstract>();
			jobQueue.add(root_node);
			while(!jobQueue.isEmpty()){
				SuffixTreeNodeAbstract node = jobQueue.poll();
			    List<SuffixTreeNodeAbstract> sortedChildren = new ArrayList<SuffixTreeNodeAbstract>(node.getChildren());
				Collections.sort(sortedChildren);
				for(int i=0; i<sortedChildren.size(); i++){
					SuffixTreeNodeAbstract child = sortedChildren.get(i);
					writer.println(node + " -> " + child + " [label=\"" + text.substring(child.edge_start_index, child.edge_end_index) + "\"];");
//					writer.println(node.nodeId + " -> " + child.nodeId + " [label=\"" + child.edge_start_index + "-" + child.edge_end_index + "\"];");
//					writer.println(node + " -> " + child + ";");
					jobQueue.add(child);
				}
			}
			writer.println("}");
			writer.close();
		}else{
			System.out.println("digraph SuffixTree {");
			Queue<SuffixTreeNodeAbstract> jobQueue = new LinkedList<SuffixTreeNodeAbstract>();
			jobQueue.add(root_node);
			while(!jobQueue.isEmpty()){
				SuffixTreeNodeAbstract node = jobQueue.poll();
				for(SuffixTreeNodeAbstract child : node.getChildren()){
					System.out.println(node.nodeId + " -> " + child.nodeId + " [label=\"" + text.substring(child.edge_start_index, child.edge_end_index+1) + "\"];");
					jobQueue.add(child);
				}
			}
			System.out.println("}");
		}
	}
	
	// outcommented
	public void toDotParentTree(boolean printToFile){
//		if(printToFile){
//			PrintWriter writer = null;
//			try {
//				writer = new PrintWriter("SuffixTreeParents.dot", "UTF-8");
//			} catch (Exception e) {
//				e.printStackTrace();
//			}
//			writer.println("digraph SuffixTree {");
//			Queue<SuffixTreeNodeAbstract> jobQueue = new LinkedList<SuffixTreeNodeAbstract>();
//			jobQueue.add(root_node);
//			while(!jobQueue.isEmpty()){
//				SuffixTreeNodeAbstract node = jobQueue.poll();
//				writer.println(node + " -> " + node.parent + ";");
//			    List<SuffixTreeNodeAbstract> sortedChildren = new ArrayList<SuffixTreeNodeAbstract>(node.getChildren());
//				Collections.sort(sortedChildren);
//				for(int i=0; i<sortedChildren.size(); i++){
//					SuffixTreeNodeAbstract child = sortedChildren.get(i);
//					jobQueue.add(child);
//				}
//			}
//			writer.println("}");
//			writer.close();
//		}else{
//			System.out.println("digraph SuffixTree {");
//			Queue<SuffixTreeNodeAbstract> jobQueue = new LinkedList<SuffixTreeNodeAbstract>();
//			jobQueue.add(root_node);
//			while(!jobQueue.isEmpty()){
//				SuffixTreeNodeAbstract node = jobQueue.poll();
//				System.out.println(node + " -> " + node.parent + ";");
//			    List<SuffixTreeNodeAbstract> sortedChildren = new ArrayList<SuffixTreeNodeAbstract>(node.getChildren());
//				Collections.sort(sortedChildren);
//				for(int i=0; i<sortedChildren.size(); i++){
//					SuffixTreeNodeAbstract child = sortedChildren.get(i);
//					jobQueue.add(child);
//				}
//			}
//			System.out.println("}");
//		}
	}

	// outcommented
	public void toDotSuffixLinks(boolean printToFile){
//		if(printToFile){
//			PrintWriter writer = null;
//			try {
//				writer = new PrintWriter("SuffixLinkTree.dot", "UTF-8");
//			} catch (Exception e) {
//				e.printStackTrace();
//			}
//			writer.println("digraph SuffixLinkTree {");
//			Queue<SuffixTreeNodeAbstract> jobQueue = new LinkedList<SuffixTreeNodeAbstract>();
//			jobQueue.add(root_node);
//			while(!jobQueue.isEmpty()){
//				SuffixTreeNodeAbstract node = jobQueue.poll();
//				writer.println(node + " -> " + node.suffixLink + ";");
//			    List<SuffixTreeNodeAbstract> sortedChildren = new ArrayList<SuffixTreeNodeAbstract>(node.getChildren());
//				Collections.sort(sortedChildren);
//				for(int i=0; i<sortedChildren.size(); i++){
//					SuffixTreeNodeAbstract child = sortedChildren.get(i);
//					jobQueue.add(child);
//				}
//			}
//			writer.println("}");
//			writer.close();
//		}else{
//			System.out.println("digraph SuffixTree {");
//			Queue<SuffixTreeNodeAbstract> jobQueue = new LinkedList<SuffixTreeNodeAbstract>();
//			jobQueue.add(root_node);
//			while(!jobQueue.isEmpty()){
//				SuffixTreeNodeAbstract node = jobQueue.poll();
//				System.out.println(node + " -> " + node.suffixLink + ";");
//				for(SuffixTreeNodeAbstract child : node.getChildren()){
//					jobQueue.add(child);
//				}
//			}
//			System.out.println("}");
//		}
	}

	// O(n*depth) Starts from the root each time
	public void generateTreeWithoutMemory(){
		root_node = createSuffixTreeNode(0, -1, -1, ' ');
		int jL = 0;
		
		int max_depth  = 0;
		int total_depth = 0;
	
		// iteration 0 er bootstrapped
		char firstNode_char = text.charAt(0);
		SuffixTreeNodeAbstract firstNode = createSuffixTreeNode(1, 0, text_length, firstNode_char);
		root_node.addChild(firstNode, firstNode_char);
		
		for(int i=0; i<text_length-1; i++){
			char c = text.charAt(i+1);
			
			for(int j= jL+1; j<=i; j++){
				// find j...i suffixes og tilf�j i+1 (ved at starte fra jL+1 undg�r vi at se p� leafs)
				SuffixTreeNodeAbstract current = root_node;
				int searchStringLength = i-j+1;
				int looking_at_char = j;
				boolean found = false;
				
				int depth = 0;
				
				
				while(!found){
					depth++;
					total_depth++;
					SuffixTreeNodeAbstract matchingChild = current.findMatchingChild(text.charAt(looking_at_char));
					if(matchingChild != null){
						int edge_length = matchingChild.edge_end_index - matchingChild.edge_start_index;
						if(edge_length > searchStringLength){ // Ending on an edge
							if(c == text.charAt(matchingChild.edge_start_index + searchStringLength)){ 
								// Is allready in the tree!!!
								j = text_length+1; // The same as using jR, as we know the rest also exists.
								found = true;
							}else{
								// inds�t indernode her and change children on nodes
								SuffixTreeNodeAbstract newLeaf = createSuffixTreeNode(j+1, i+1, text_length, c);
								jL = j;
								SuffixTreeNodeAbstract innerNode = createSuffixTreeNode(innerNodeId, matchingChild.edge_start_index, matchingChild.edge_start_index+searchStringLength, matchingChild.edge_char);
								innerNodeId--;
								innerNode.addChild(newLeaf, c); // Tilf�jer ny knude som barn til nye innerNode
								matchingChild.edge_char = text.charAt(matchingChild.edge_start_index + searchStringLength);
								matchingChild.edge_start_index = innerNode.edge_end_index;
								innerNode.addChild(matchingChild, matchingChild.edge_char); // Tilf�jer det matchede child, til innerNoden
								current.removeChild(matchingChild, matchingChild.edge_char);   // Fjerner det matchede child fra dens gamle parrent
								current.addChild(innerNode, text.charAt(innerNode.edge_start_index)); // Tilf�jer den nye innerNode til den gamle parrent.
								found = true;
							}
						}else if(edge_length == searchStringLength){ // Ending in a node
							SuffixTreeNodeAbstract matchingGrandChild = matchingChild.findMatchingChild(c);
							if(matchingGrandChild == null){ // does not allready exists
								SuffixTreeNodeAbstract newLeaf = createSuffixTreeNode(j+1, i+1, text_length, c);
								jL = j;
								matchingChild.addChild(newLeaf, c);
								found = true;
							}else{ // allready exists in a grandchild node
								j = text_length+1; // The same as using jR, as we know the rest also exists.
								found = true;
							}
						}else{ // Has to search through the child nodes
							searchStringLength -= edge_length;
							current = matchingChild;
							looking_at_char = looking_at_char + edge_length;
						}
					}else{ // No matching child, -> does not exists and end on an node - should be hint in the case with "grandchild"
						SuffixTreeNodeAbstract newLeaf = createSuffixTreeNode(j+1, i+1, text_length, c);
						current.addChild(newLeaf, c);
						jL = j;
						found = true;
						System.out.println("THIS CODE SHOULD NEVER BE REACHED!!!!!!!!");
						throw new RuntimeException("THIS CODE SHOULD NEVER BE REACHED!!!!!!!!");
					}
				}
				
				max_depth = Math.max(max_depth, depth);
			}
			
			// append i+1 (allready i+1, as it has leaved the for loop
			SuffixTreeNodeAbstract matchingChildInRoot = root_node.findMatchingChild(c);
			if(matchingChildInRoot == null){
				SuffixTreeNodeAbstract newLeaf = createSuffixTreeNode(i+2, i+1, text_length, c);
				root_node.addChild(newLeaf, c);
				jL = i+1;
			}
			
		}
		
		System.out.println("MAX DEPTH:" + max_depth);
		System.out.println("TOTA DEPTH:" + total_depth);
	}	
	
	// Den helst simple, men gemmer, hvor den sidst har set en der ikke er et leaf
	public void generateTreeWithMemory(){
			root_node = createSuffixTreeNode(0, -1, -1, ' ');
			int jL = 0;
			
//			int max_depth  = 0;
//			int total_depth = 0;
		
			// iteration 0 er bootstrapped
			char firstNode_char = text.charAt(0);
			SuffixTreeNodeAbstract firstNode = createSuffixTreeNode(1, 0, text_length, firstNode_char);
			root_node.addChild(firstNode, firstNode_char);
			
			
			SuffixTreeNodeAbstract start_point = root_node;
			int looking_at_char_start_point = -1;
			int searchStringLength_start_point = -1;
			
			for(int i=0; i<text_length-1; i++){
				char c = text.charAt(i+1);
				
				boolean firstIterationInJ = true;
				
				for(int j= jL+1; j<=i; j++){
					// find j...i suffixes og tilf�j i+1 (ved at starte fra jL+1 undg�r vi at se p� leafs)
					SuffixTreeNodeAbstract current = null;
					
					int looking_at_char = j;
					int searchStringLength = i-j+1;
					boolean found = false;
					if(firstIterationInJ){ // not starting from the root again
						current = start_point;
						looking_at_char = looking_at_char_start_point;
						searchStringLength = searchStringLength_start_point;
					}
					if(!firstIterationInJ || start_point.nodeId == 0){
						current = root_node;
						looking_at_char = j;
						searchStringLength = i-j+1;
					}
						
					firstIterationInJ = false;
//					int depth = 0;
					
					while(!found){
//						depth++;
//						total_depth++;
						SuffixTreeNodeAbstract matchingChild = current.findMatchingChild(text.charAt(looking_at_char));
						if(matchingChild != null){
							int edge_length = matchingChild.edge_end_index - matchingChild.edge_start_index;
							if(edge_length > searchStringLength){ // Ending on an edge
								if(c == text.charAt(matchingChild.edge_start_index + searchStringLength)){ 
									// Is allready in the tree!!!
									j = text_length+1; // The same as using jR, as we know the rest also exists.
									found = true;
									start_point = current;
									looking_at_char_start_point = looking_at_char;
									searchStringLength_start_point = searchStringLength+1;
								}else{
									// inds�t indernode her and change children on nodes
									SuffixTreeNodeAbstract newLeaf = createSuffixTreeNode(j+1, i+1, text_length, c);
									jL = j;
									SuffixTreeNodeAbstract innerNode = createSuffixTreeNode(innerNodeId, matchingChild.edge_start_index, matchingChild.edge_start_index+searchStringLength, matchingChild.edge_char);
									innerNodeId--;
									innerNode.addChild(newLeaf, c); // Tilf�jer ny knude som barn til nye innerNode
									matchingChild.edge_char = text.charAt(matchingChild.edge_start_index + searchStringLength);
									matchingChild.edge_start_index = innerNode.edge_end_index;
									innerNode.addChild(matchingChild, matchingChild.edge_char); // Tilf�jer det matchede child, til innerNoden
									current.removeChild(matchingChild, matchingChild.edge_char);   // Fjerner det matchede child fra dens gamle parrent
									current.addChild(innerNode, text.charAt(innerNode.edge_start_index)); // Tilf�jer den nye innerNode til den gamle parrent.
									found = true;
									start_point = root_node;
								}
							}else if(edge_length == searchStringLength){ // Ending in a node
								SuffixTreeNodeAbstract matchingGrandChild = matchingChild.findMatchingChild(c);
								if(matchingGrandChild == null){ // does not allready exists
									SuffixTreeNodeAbstract newLeaf = createSuffixTreeNode(j+1, i+1, text_length, c);
									jL = j;
									matchingChild.addChild(newLeaf, c);
									found = true;
									start_point = root_node;
								}else{ // allready exists in a grandchild node
									j = text_length+1; // The same as using jR, as we know the rest also exists.
									found = true;
									start_point = current;
									looking_at_char_start_point = looking_at_char;
									searchStringLength_start_point = searchStringLength+1;
								}
							}else{ // Has to search through the child nodes
								searchStringLength -= edge_length;
								current = matchingChild;
								looking_at_char = looking_at_char + edge_length;
							}
						}else{ // No matching child, -> does not exists and end on an node - should be hint in the case with "grandchild"
							SuffixTreeNodeAbstract newLeaf = createSuffixTreeNode(j+1, i+1, text_length, c);
							current.addChild(newLeaf, c);
							jL = j;
							found = true;
							System.out.println("THIS CODE SHOULD NEVER BE REACHED!!!!!!!!");
							throw new RuntimeException("THIS CODE SHOULD NEVER BE REACHED!!!!!!!!");
						}
					}
					
//					max_depth = Math.max(max_depth, depth);
				}
				
				if(jL == i){ // i+1 can only be a leaf if i allready is.
					// append i+1
					SuffixTreeNodeAbstract matchingChildInRoot = root_node.findMatchingChild(c);
					if(matchingChildInRoot == null){
						SuffixTreeNodeAbstract newLeaf = createSuffixTreeNode(i+2, i+1, text_length, c);
						root_node.addChild(newLeaf, c);
						jL = i+1;
					}
					start_point = root_node;
				}
				
			}
			
//			System.out.println("MAX DEPTH:" + max_depth);
//			System.out.println("TOTA DEPTH:" + total_depth);
		}
}
