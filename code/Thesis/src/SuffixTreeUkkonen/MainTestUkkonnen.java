package SuffixTreeUkkonen;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import SuffixArray.SuffixArray;
import SuffixTreeNodes.NodeTypes.ChildrenStructures;

public class MainTestUkkonnen {
	public static void main(String args[]) throws IOException{
		String text = readInText("C:/Users/Jens/Desktop/Thesis/texts/genome1.fa");
//		text = "TTGTTGATATTCTGTTT";
		System.out.println("TEXT LENGTH" + text.length());
		
//		text = "MISSISSIPPI";
//		text = "AABBAAAB";
		
		long suffixConstruction_start = System.currentTimeMillis();
		SuffixTree suffix_tree = new SuffixTree(text, ChildrenStructures.LINEAR);
//		suffix_tree.toDotSuffixLinks(true);
//		suffix_tree.toDot(true);
//		System.out.println("COULD FIND: " + suffix_tree.search("PK"));
		long suffixConstruction_end = System.currentTimeMillis();
		System.out.println("TIME FOR SUFFIX CONSTRUCTION: " + (suffixConstruction_end - suffixConstruction_start));
//		suffix_tree.toDotParentTree(true);
//		System.out.println(suffix_tree.search("TGTTT"));
//		suffix_tree.printLeafLists();
//		suffix_tree.printLeafLists();
//		suffix_tree.printNodeIdToLeafLists();
		
		
		long arrayConstruction_start = System.currentTimeMillis();
		SuffixArray suffix_array = new SuffixArray();
		suffix_array.construct_SuffixArray_from_SuffixTree(suffix_tree);
//		suffix_array.printSuffixArray();
		long arrayConstruction_end = System.currentTimeMillis();
		System.out.println("TIME FOR SUFFIX ARRAY CONSTRUCTION: " + (arrayConstruction_end - arrayConstruction_start));
		
	}
	
	
	public static String readInText(String filename) throws IOException{
		  BufferedReader br = new BufferedReader(new FileReader(filename));
		  String result = "";
		    try {
		        StringBuilder sb = new StringBuilder();
		        String line = br.readLine();

		        while (line != null) {
		            sb.append(line);
		            line = br.readLine();
		        }
		        result = sb.toString();
		    } finally {
		        br.close();
		    }
		    
		    return result;
	}
}
