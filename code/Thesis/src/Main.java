import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

import SimpleScan.SimpleScan;
import SuffixArray.SuffixArray;
import SuffixTray.SuffixTray;
import SuffixTreeNodes.NodeTypes.ChildrenStructures;
import SuffixTreeUkkonen.SuffixTree;


public class Main {

	public static void main(String args[]) throws IOException{
		String text = readInText("C:/Users/Jens/Desktop/Thesis/texts/genome1.fa");
		System.out.println("TEXT LENGTH" + text.length());
		text = "BBBBAAAAABBBBBB";
////		String text = "TATAT$";
//		text = "TTGTTGATATTCTGTTT";
//		text = "MISSISSIPPI";
////		

//		long suffixConstruction_start = System.currentTimeMillis();
		SuffixTree suffix_tree =  new SuffixTree(text, ChildrenStructures.LINEAR);
		suffix_tree.toDot(true);
		
//
//		
//	    long suffixConstruction_old_new_start = System.currentTimeMillis();
//	    long suffixConstruction_old_start = System.currentTimeMillis();
//	    SuffixTreeOldVersion suffixTree_old = new SuffixTreeOldVersion(text); // old version appender selv $
//	    long suffixConstruction_old_end = System.currentTimeMillis();
//	    System.out.println("CONSTRUCTION TIME FOR SUFFIXTREE IN MILLIS: " + (suffixConstruction_old_end - suffixConstruction_old_start));
//	    SuffixTree suffix_tree_old = suffixTree_old.printSuffixTree();
//	    long suffixConstruction_old_new_end = System.currentTimeMillis();
//	    System.out.println("CONSTRUCTION TIME FOR SUFFIXTREE IN MILLIS: " + (suffixConstruction_old_new_end - suffixConstruction_old_new_start));
//		
//	    suffix_tree.toDot(true);
//	    
		SuffixArray suffix_array = new SuffixArray();
		suffix_array.construct_SuffixArray_from_SuffixTree(suffix_tree);
//		suffix_array.printSuffixArray();
//		
		SuffixTray suffix_tray = new SuffixTray(suffix_tree, suffix_array);
		suffix_tray.toDot();
	
//		String search_string = "AGAAGCT";
//			String search_string = "abaab";
		
//		ArrayList<Integer> tray_results = suffix_tray.searchAll(search_string);
//		Collections.sort(tray_results);
//		System.out.println("TRAY");
//		System.out.println(tray_results);
//		if(!suffix_tray.search(search_string)){
//		System.out.println("TRAY SEARCH FOR " + search_string + " FAILED");	
//		}			
		
		
//		suffix_tray.print_lcp_X_array(true);
//		suffix_tray.print_lcp_X_array(false);
//		
//		ArrayList<Integer> array_results = suffix_array.searchAll("I");
//		Collections.sort(array_results);
//		System.out.println(array_results);
		
		
//		
//		
//		
		
////		
//		SimpleScan sscan = new SimpleScan(text + "$");
//		System.out.println("SCAN");
//		System.out.println(sscan.searchAll(search_string));
//		
		System.out.println("MAIN DONE");
		
	}
	
	public static String readInText(String filename) throws IOException{
		  BufferedReader br = new BufferedReader(new FileReader(filename));
		  String result = "";
		    try {
		        StringBuilder sb = new StringBuilder();
		        String line = br.readLine();

		        while (line != null) {
		        	if(!line.startsWith(">"))
		        		sb.append(line);
		            line = br.readLine();
		        }
		        result = sb.toString();
		    } finally {
		        br.close();
		    }
		    
		    return result;
	}
	
}
