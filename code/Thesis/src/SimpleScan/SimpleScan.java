package SimpleScan;
import java.util.ArrayList;


public class SimpleScan {

	String text;
	
	public SimpleScan(String text){
		this.text = text + "$";
	}
	
	// TODO Dette er helt simpelt, det kan/skal muligvis laves smartere hvis tiden skal bruges til noget! (F.eks. trick med LCP)
	// Finder om search-strengen er et sted i texten
	public int search(String search_string){
		for(int i=0; i<text.length()-(search_string.length()-1); i++){
			for(int j=0; j<search_string.length(); j++){
				if(text.charAt(i+j) != search_string.charAt(j))
					break;
				if(j == search_string.length()-1)
					return i;
			}
		}
		
		return -1;
	}
	
	// Finder alle index hvor strengen er
	public ArrayList<Integer> searchAll(String search_string){
		ArrayList<Integer> results = new ArrayList<Integer>();
		
		for(int i=0; i<text.length()-(search_string.length()-1); i++){
			for(int j=0; j<search_string.length(); j++){
				if(text.charAt(i+j) != search_string.charAt(j))
					break;
				if(j == search_string.length()-1){
					results.add(i);
				}
			}
		}
		return results;
	}
}
