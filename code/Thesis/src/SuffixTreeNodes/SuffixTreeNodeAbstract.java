package SuffixTreeNodes;

public abstract class SuffixTreeNodeAbstract  implements SuffixTreeNode, Comparable<SuffixTreeNodeAbstract>{
	public int edge_start_index;
	public int edge_end_index;
	public int leaf_list_start = Integer.MAX_VALUE;
	public int leaf_list_end;
	public int nodeId;
	public char edge_char;
	public SuffixTreeNodeAbstract parent;
	public SuffixTreeNodeAbstract suffixLink;
	public int  depth = 0;
	
	public SuffixTreeNodeAbstract(int _nodeId, SuffixTreeNodeAbstract parent, int edge_start, int edge_end, char edge_char, int depth){
		nodeId = _nodeId;
		edge_start_index = edge_start;
		edge_end_index = edge_end;
		this.edge_char = edge_char;
		this.parent = parent;
		suffixLink = null;
		this.depth = depth;
	}
	public SuffixTreeNodeAbstract(int _nodeId, int edge_start, int edge_end, char edge_char){
		nodeId = _nodeId;
		edge_start_index = edge_start;
		edge_end_index = edge_end;
		this.edge_char = edge_char;
		suffixLink = null;
		parent = null;
	}
	
	public String toString(){
//		return "\"" + nodeId + "" + "(L" + leaf_list_start + "," + leaf_list_end+") C:" + edge_char +  " D:" + depth + "\"";
		return "" + nodeId;
	}

    @Override
    public int compareTo(SuffixTreeNodeAbstract node){
        if (this.edge_char > node.edge_char)
            return 1;
        else if (this.edge_char == node.edge_char)
            return 0;
        else 
            return -1;
    }
	
}

