package SuffixTreeNodes;

import java.util.ArrayList;
import java.util.Collection;


public class SuffixTreeNodeLinear extends SuffixTreeNodeAbstract{
	public ArrayList<SuffixTreeNodeAbstract> children;
	
	public SuffixTreeNodeLinear(int _nodeId, int edge_start, int edge_end, char edge_char){
		super(_nodeId, edge_start, edge_end, edge_char);
		children = new ArrayList<SuffixTreeNodeAbstract>();
	}
	
	public SuffixTreeNodeLinear(int _nodeId, SuffixTreeNodeAbstract parent, int edge_start, int edge_end, char edge_char, int depth){
		super(_nodeId, parent, edge_start, edge_end, edge_char, depth);
		children = new ArrayList<SuffixTreeNodeAbstract>();
	}
		
	public void addChild(SuffixTreeNodeAbstract node, Character firstChar){
		children.add(node);
	}
	
	public void removeChild(SuffixTreeNodeAbstract node, Character c){
		children.remove(node);
	}
	
	public SuffixTreeNodeAbstract findMatchingChild(Character c){
		for(SuffixTreeNodeAbstract child : children)
			if(child.edge_char == c)
				return child;
		
		return null;
	}
	
	public Collection<SuffixTreeNodeAbstract> getChildren(){
		return children;
	};
	
}