package SuffixTreeNodes;

import java.util.Collection;
import java.util.TreeMap;

public class SuffixTreeNodeBinary extends SuffixTreeNodeAbstract{
	public TreeMap<Character, SuffixTreeNodeAbstract> children;
	
	public SuffixTreeNodeBinary(int _nodeId, int edge_start, int edge_end, char edge_char){
		super(_nodeId, edge_start, edge_end, edge_char);
		children = new TreeMap<Character, SuffixTreeNodeAbstract>();
	}
	
	public SuffixTreeNodeBinary(int _nodeId, SuffixTreeNodeAbstract parent, int edge_start, int edge_end, char edge_char, int depth){
		super(_nodeId, parent, edge_start, edge_end, edge_char, depth);
		children = new TreeMap<Character, SuffixTreeNodeAbstract>();
	}
	
	public void addChild(SuffixTreeNodeAbstract node, Character firstChar){
		children.put(firstChar, node);
	}

	public void removeChild(SuffixTreeNodeAbstract node, Character c){
		// Ved map beh�ves ikke at slette b�rn, da der altid i samme omgang vil blive sat en ny knude ind, med samme key,- som vil overskrive den der skulle slettes (keys er unikke)
	}
	
	public SuffixTreeNodeAbstract findMatchingChild(Character c){
		return children.get(c);
	}
	
	public Collection<SuffixTreeNodeAbstract> getChildren(){
		return children.values();
	};
	
}
