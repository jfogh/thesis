package SuffixTreeNodes;

import java.util.Collection;

public interface SuffixTreeNode {
	public void addChild(SuffixTreeNodeAbstract node, Character firstChar);
	public void removeChild(SuffixTreeNodeAbstract node, Character c);
	public Collection<SuffixTreeNodeAbstract> getChildren();
	public SuffixTreeNodeAbstract findMatchingChild(Character c);
	public int compareTo(SuffixTreeNodeAbstract node);
}
