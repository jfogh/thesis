package SuffixArray;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Stack;

import SuffixTray.SuffixTrayNodeAbstract;
import SuffixTreeNodes.SuffixTreeNodeAbstract;


public class SuffixArray {

	public int[] suffix_array;
	public int[] lcp_L_array;
	public int[] lcp_R_array;
	public String text;
	
	public void construct_SuffixArray_from_SuffixTree(SuffixTreeUkkonen.SuffixTree tree){
		text = tree.text;
		suffix_array = new int[text.length()];
		Stack<SuffixTreeNodes.SuffixTreeNodeAbstract> dfsStack = new Stack<SuffixTreeNodes.SuffixTreeNodeAbstract>();
		dfsStack.push(tree.root_node);
		// DFS search i suffixTr�et, for at inds�tte indeksne i suffix arrayet
		while(!dfsStack.isEmpty()){
			SuffixTreeNodes.SuffixTreeNodeAbstract node = dfsStack.pop();
			if(node.getChildren().isEmpty()){
				// Da noderne i SuffixTr�et er 1 indekseret, og vi her �nsker det er 0 indekseret
				suffix_array[tree.nodeId_to_leafListIndex[node.nodeId]] = node.nodeId-1;
			}else{
				Collection<SuffixTreeNodes.SuffixTreeNodeAbstract> children = node.getChildren();
				for(SuffixTreeNodes.SuffixTreeNodeAbstract child : children)
					dfsStack.add(child);
			}
		}
		// Disse arrays bruges til search mekanismen
		lcp_L_array = new int[text.length()];
		lcp_R_array = new int[text.length()];
		initialize_lcp_X_arrays(0, text.length()-1);

	}
	
	public int search(String search_string){
		return searchInArray(search_string, 0, text.length()-1, 0);
	}
	
	// Bruger find_RW men returnere blot, f�rste gang et total match findes.
		public int searchInArray(String search_string, int L, int R, int start_index){
			int r = 0;
			int M = 0;
			int m = 0;

			// LCP mellem suffix_array indeks 0 og search_string
			int lcp_value_init = start_index;
			while(text.charAt(suffix_array[L] + lcp_value_init) == search_string.charAt(lcp_value_init))
			{
				lcp_value_init++;
				if(lcp_value_init == search_string.length())
					return suffix_array[L];
				if(suffix_array[L] + lcp_value_init == suffix_array.length)
					break;
			}
			int l = lcp_value_init;
			
			if(search_string.charAt(l) <= text.charAt(suffix_array[L]+l))
				return -1;
			
			// LCP mellem suffix_array indeks 0 og search_string // SLUT
			
			if(R>L){
				// LCP mellem suffix_array indeks text.length()-1 og search_string
				lcp_value_init = start_index;
				while(text.charAt(suffix_array[R] + lcp_value_init) == search_string.charAt(lcp_value_init))
				{
					lcp_value_init++;
					if(lcp_value_init == search_string.length())
						return suffix_array[R];
					if(suffix_array[R] + lcp_value_init == suffix_array.length)
						break;
				}
				r = lcp_value_init;
		
				if(search_string.charAt(r) > text.charAt(suffix_array[R]+r))
					return -1;
				
				// LCP mellem suffix_array indeks text.length()-1 og search_string // SLUT
			}

			while(R-L > 1){
				M = (R+L)/2;
				if(l >= r){
					if(lcp_L_array[M] == l){
						int lcp_value = 0;
						if(!(suffix_array[M] + l + lcp_value == suffix_array.length)){
							while(text.charAt(suffix_array[M] + l + lcp_value) == search_string.charAt(l+lcp_value))
							{
								lcp_value++;
								// Hvis der har v�ret |search_string| matches, returneres true
								if(l + lcp_value == search_string.length())
									return suffix_array[M];
								if(suffix_array[M] + l + lcp_value == suffix_array.length)
									break;
							}					
						}
						m = l + lcp_value;
					}else if(lcp_L_array[M] > l){
						m = l;
					}else if(lcp_L_array[M] < l){
						m = lcp_L_array[M];
					}
				}else{
					if(lcp_R_array[M] == r){
						int lcp_value = 0;
						if(!(suffix_array[M] + r + lcp_value == suffix_array.length)){
						while(text.charAt(suffix_array[M] + r + lcp_value) == search_string.charAt(r+lcp_value))
						{
							lcp_value++;
							// Hvis der har v�ret |search_string| matches, returneres true
							if(r + lcp_value == search_string.length())
								return suffix_array[M];
							else if(suffix_array[M] + r + lcp_value == suffix_array.length)
								break;
						}}
						m = r + lcp_value;
					}else if(lcp_R_array[M] > r){
						m = r;
					}else if(lcp_R_array[M] < r){
						m = lcp_R_array[M];
					}
				}
				if(search_string.charAt(m) <= text.charAt(suffix_array[M]+m)){
					R = M;
					r = m;
				}else{
					L = M;
					l = m;
				}
			}
			return -1;
		
		}
	
	// Finder alle matches via find_RW og find_LW
	public ArrayList<Integer> searchAll(String search_string){
		ArrayList<Integer> results = new ArrayList<Integer>();
		
		// LCP mellem suffix_array indeks 0 og search_string
		int lcp_value_l = 0;
		while(text.charAt(suffix_array[0] + lcp_value_l) == search_string.charAt(lcp_value_l))
		{
			lcp_value_l++;
			if(lcp_value_l == search_string.length()|| suffix_array[0] + lcp_value_l == suffix_array.length)
				break;
		}
		
		// LCP mellem suffix_array indeks text.length()-1 og search_string
		int lcp_value_r = 0;
		while(text.charAt(suffix_array[text.length()-1] + lcp_value_r) == search_string.charAt(lcp_value_r))
		{
			lcp_value_r++;
			if(lcp_value_r == search_string.length()|| suffix_array[text.length()-1] + lcp_value_r == suffix_array.length)
				break;
		}
		
		int lw = find_LW(search_string, lcp_value_l, lcp_value_r);
		int lr = find_RW(search_string, lcp_value_l, lcp_value_r);
		for(int i = lw; i<=lr; i++){
			results.add(suffix_array[i]);
		}
		
		return results;
	}
	
	// Finder mindste indeks [k] i suffix_array hvor search_text <= suffix[k]
	public int find_LW(String search_string, int l, int r){
	
		if(l == search_string.length() || search_string.charAt(l) <= text.charAt(suffix_array[0]+l))
			return 0;
		
	    if(r < search_string.length() && search_string.charAt(r) > text.charAt(suffix_array[text.length()-1]+r))
			return text.length();


		// Her bliver den bin�re s�gning efter indekset lavet, dette g�res "smart" ved at bruge L_array og R_array til at f� det ned p� O(log n + m) istedet for O(log n * m)
		int L = 0;
		int R = text.length()-1;
		int M = 0;
		int m = 0;
		while(R-L > 1){
			M = (R+L)/2;
			if(l >= r){
				if(lcp_L_array[M] >= l){
					int lcp_value = 0;
					if(!(l+lcp_value == search_string.length()|| suffix_array[M] + l + lcp_value == suffix_array.length)){
						while(text.charAt(suffix_array[M] + l + lcp_value) == search_string.charAt(l+lcp_value))
						{
							lcp_value++;
							if(l+lcp_value == search_string.length()|| suffix_array[M] + l + lcp_value == suffix_array.length)
								break;
						}					
					}
					m = l + lcp_value;
				}else{
					m = lcp_L_array[M];
				}
			}else{
				if(lcp_R_array[M] >= r){
					int lcp_value = 0;
					if(!(r+lcp_value == search_string.length()|| suffix_array[M] + r + lcp_value == suffix_array.length)){
					while(text.charAt(suffix_array[M] + r + lcp_value) == search_string.charAt(r+lcp_value))
					{
						lcp_value++;
						if(r+lcp_value == search_string.length()|| suffix_array[M] + r + lcp_value == suffix_array.length)
							break;
					}}
					m = r + lcp_value;
				}else{
					m = lcp_R_array[M];
				}
			}
			if(m == search_string.length() || search_string.charAt(m) <= text.charAt(suffix_array[M]+m)){
				R = M;
				r = m;
			}else{
				L = M;
				l = m;
			}
		}
		return R;
		
	}
	
	
	// Finder mindste indeks [k] i suffix_array hvor  suffix[k] <= search_text
	public int find_RW(String search_string, int l, int r){

		if(l < search_string.length() && text.charAt(suffix_array[0]+l) > search_string.charAt(l))
			return -1;
		
		if(r == search_string.length() || text.charAt(suffix_array[text.length()-1]+r) <= search_string.charAt(r))
			return text.length()-1;


		// Her bliver den bin�re s�gning efter indekset lavet, dette g�res "smart" ved at bruge L_array og R_array til at f� det ned p� O(log n + m) istedet for O(log n * m)
		int L = 0;
		int R = text.length()-1;
		int M = 0;
		int m = 0;
		while(R-L > 1){
			M = (R+L)/2;
			if(l >= r){
				if(lcp_L_array[M] >= l){
					int lcp_value = 0;
					if(!(l+lcp_value == search_string.length()|| suffix_array[M] + l + lcp_value == suffix_array.length)){
						while(text.charAt(suffix_array[M] + l + lcp_value) == search_string.charAt(l+lcp_value))
						{
							lcp_value++;
							if(l+lcp_value == search_string.length()|| suffix_array[M] + l + lcp_value == suffix_array.length)
								break;
						}					
					}
					m = l + lcp_value;
				}else{
					m = lcp_L_array[M];
				}
			}else{
				if(lcp_R_array[M] >= r){
					int lcp_value = 0;
					if(!(r+lcp_value == search_string.length()|| suffix_array[M] + r + lcp_value == suffix_array.length)){
					while(text.charAt(suffix_array[M] + r + lcp_value) == search_string.charAt(r+lcp_value))
					{
						lcp_value++;
						if(r+lcp_value == search_string.length()|| suffix_array[M] + r + lcp_value == suffix_array.length)
							break;
					}}
					m = r + lcp_value;
				}else{
					m = lcp_R_array[M];
				}
			}
			if(m < search_string.length() && search_string.charAt(m) < text.charAt(suffix_array[M]+m)){
				R = M;
				r = m;
			}else{
				L = M;
				l = m;
			}
		}
		return L;
	
	}
	
	public void initialize_lcp_X_arrays(int L, int R){
		Stack<int[]> endPoints = new Stack<int[]>();
		endPoints.push(new int[]{L,R,0});
		while(!endPoints.isEmpty()){
			int[] points = endPoints.pop();
			L = points[0];
			R = points[1];		
			int already_matched = points[2];
			
			if(R-L > 1){
				int M = (R+L)/2;
				int lcp_value = already_matched;
				int suffix_pos_L = suffix_array[L];
				int suffix_pos_M = suffix_array[M];
				int suffix_pos_R = suffix_array[R];
				
				// Finder lcp for L og M
				while(text.charAt(suffix_pos_L + lcp_value) == text.charAt(suffix_pos_M + lcp_value))
				{
					lcp_value++;
					// Sikre mod out of bounds, da begge suffixes jo stammer fra "hoved" texten
					if(suffix_pos_L + lcp_value == suffix_array.length || suffix_pos_M + lcp_value == suffix_array.length)
						break;
				}
				lcp_L_array[M] = lcp_value;
				endPoints.push(new int[]{L,M, lcp_value});
				
				// Finder lcp for R og M
				lcp_value = already_matched;
				while(text.charAt(suffix_pos_R + lcp_value) == text.charAt(suffix_pos_M + lcp_value))
				{
					lcp_value++;
					// Sikre mod out of bounds, da begge suffixes jo stammer fra "hoved" texten
					if(suffix_pos_R + lcp_value == suffix_array.length || suffix_pos_M + lcp_value == suffix_array.length)
						break;
				}
				lcp_R_array[M] = lcp_value;
				endPoints.push(new int[]{M,R, lcp_value});
				
			}
		}
	}

	// Tager en bool, true = left_array, false = right_array
	public void print_lcp_X_array(boolean left){
		if(left){
			System.out.println("---- LEFT LCP ----");
			for(int i=0; i<lcp_L_array.length; i++)
				System.out.println("M: " + i + " L:" + lcp_L_array[i]);
		}else{
			System.out.println("---- RIGHT LCP ----");
			for(int i=0; i<lcp_R_array.length; i++)
				System.out.println("M: " + i + " R:" + lcp_R_array[i]);
		}
	}
	
	public void printSuffixArray(){
		for(int i=0; i<suffix_array.length; i++)
			System.out.print(suffix_array[i] + ", ");
		System.out.println();
	}
		
}
