package SuffixTray;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.Queue;

import SuffixArray.SuffixArray;
import SuffixTreeUkkonen.*;
import SuffixTreeNodes.*;

public class SuffixTray {

	public int alphabet_size;
	private SuffixTree suffix_tree;
	public SuffixTrayNodeAbstract root_node;
	public String text;
	public int[] alphabet_array;
	private int[] suffix_array;
	private int[] lcp_L_array;
	private int[] lcp_R_array;
	private SuffixArray suffix_array_full;
	
	private int tray_node_id = 0;
	
	public SuffixTray(SuffixTree suffix_tree, SuffixArray suffix_array){
		text = suffix_tree.text;
		readInAlphabet();
		this.suffix_array = suffix_array.suffix_array;
		// Disse arrays bruges til search mekanismen i suffix array
		lcp_L_array = new int[text.length()];
		lcp_R_array = new int[text.length()];
		this.suffix_tree = suffix_tree;
		
		for(int i=0; i<lcp_L_array.length; i++){
			lcp_L_array[i]= -1;
			lcp_R_array[i]= -1;
		}
		
		construct_suffixTray(suffix_tree.root_node);
	}

	public void construct_suffixTray(SuffixTreeNodeAbstract tree_root_node){
		root_node = recursive_construction(tree_root_node);
	}
	
	private SuffixTrayNodeAbstract recursive_construction(SuffixTreeNodeAbstract tree_node){
		SuffixTrayNodeAbstract tray_node = null;
		boolean branching = false;
		boolean non_branching = false;
		ArrayList<SuffixTreeNodeAbstract> childrenInSuffixTree = new ArrayList<SuffixTreeNodeAbstract>(tree_node.getChildren());
		Collections.sort(childrenInSuffixTree);
		
		int suffixTrayNodeChildren = 0;
		
		for(int i=0; i<childrenInSuffixTree.size(); i++){
			SuffixTreeNodeAbstract child = childrenInSuffixTree.get(i);
			if(child.leaf_list_end - child.leaf_list_start + 1 >= alphabet_size-1){ // alphabet_size-1 as the article splits as if $ was not in the alphabet
				suffixTrayNodeChildren++;
			}
		}
		
		SuffixTrayNodeAbstract[] branching_array = null;
		ArrayList<Character> chars_in_interval = new ArrayList<Character>();
		SuffixTrayNodeAbstract non_branching_child = null;
		SuffixTrayNodeAbstract non_branching_left = null;
		SuffixTrayNodeAbstract non_branching_right = null;
		char sep_char = ' ';
		boolean right_interval_child = false;
		int interval_start = -1;
		int interval_end = -1;
		int edge_start_interval = -1;
		int edge_end_interval = -1;
		
		if(suffixTrayNodeChildren == 1){
			non_branching = true;
		}else if(suffixTrayNodeChildren > 1){
			branching = true;
			branching_array = new SuffixTrayNodeAbstract[alphabet_size]; 
		}
		
		for(int i=0; i<childrenInSuffixTree.size(); i++){
			SuffixTreeNodeAbstract child = childrenInSuffixTree.get(i);
			
			if(child.leaf_list_end - child.leaf_list_start + 1 >= alphabet_size-1){ // alphabet_size-1 as the article splits as if $ was not in the alphabet

				if(interval_start != -1){ // an interval has been build
					SuffixTrayNodeAbstract interval_child;
					if(chars_in_interval.size() > 1){
						interval_child = new SuffixTrayInterval(-1, interval_start, interval_end, 0, 0);	
					}else{
						interval_child = new SuffixTrayInterval(-1, interval_start, interval_end, edge_start_interval, edge_end_interval);
					}
			
					if(non_branching){
						if(!right_interval_child){
							non_branching_left = interval_child;
							right_interval_child = true;
						}else{
							non_branching_right = interval_child;
						}
						initialize_lcp_X_arrays(interval_start, interval_end);
						interval_start = -1;
					}
				}
				
				SuffixTrayNodeAbstract tray_child = recursive_construction(child);
				right_interval_child = true;
				if(branching){
					branching_array[alphabet_array[(int) text.charAt(child.edge_start_index)]] = tray_child;
				}else if(non_branching){
					non_branching_child = tray_child;
					sep_char = text.charAt(child.edge_start_index);
				}
			}else{
				if(interval_start == -1){
					interval_start = child.leaf_list_start;
				}
				interval_end = child.leaf_list_end;
				edge_start_interval = child.edge_start_index;
				edge_end_interval = child.edge_end_index;
				chars_in_interval.add(text.charAt(child.edge_start_index));
				
				if(branching){
					SuffixTrayNodeAbstract interval_child = new SuffixTrayInterval(-1, child.leaf_list_start, child.leaf_list_end, 0, 0);
					branching_array[alphabet_array[(int) text.charAt(child.edge_start_index)]] = interval_child;
					initialize_lcp_X_arrays(child.leaf_list_start, child.leaf_list_end);
				}
			}
		}
		
		
		if(interval_start != -1 && non_branching){ // the most right children was forming and interval
			SuffixTrayNodeAbstract interval_child;
			if(chars_in_interval.size() > 1){
				interval_child = new SuffixTrayInterval(-1, interval_start, interval_end, 0, 0);
			}else{
				interval_child = new SuffixTrayInterval(-1, interval_start, interval_end, edge_start_interval, edge_end_interval);
			}
			if(non_branching){ // always right as it is the right most children
				non_branching_right = interval_child;
				initialize_lcp_X_arrays(interval_start, interval_end);
			}
		}
		
		if(branching){
			tray_node = new SuffixTrayBranchingNode(tray_node_id, branching_array, tree_node.edge_start_index, tree_node.edge_end_index, tree_node.leaf_list_start, tree_node.leaf_list_end);
		}else if(non_branching){
//			tray_node = new SuffixTrayNoneBranchingNode(tray_node_id, non_branching_left, non_branching_child, non_branching_right, sep_char, alphabet_map.get(sep_char), tree_node.edge_start_index, tree_node.edge_end_index);
			tray_node = new SuffixTrayNoneBranchingNode(tray_node_id, non_branching_left, non_branching_child, non_branching_right, sep_char, alphabet_array[sep_char], tree_node.edge_start_index, tree_node.edge_end_index, tree_node.leaf_list_start, tree_node.leaf_list_end);
		}else{ // The node from the suffix tree I am looking at is either a o-leaf, or the root_node in the suffix tree. As suffix tree edge_start=edge_end=-1, we can just set the interval edges to the suffix tree nodes edges.
			tray_node = new SuffixTrayInterval(-1, childrenInSuffixTree.get(0).leaf_list_start, childrenInSuffixTree.get(childrenInSuffixTree.size()-1).leaf_list_end, tree_node.edge_start_index, tree_node.edge_end_index);
			initialize_lcp_X_arrays(interval_start, interval_end);
		}
		
		tray_node_id++;
		
		return tray_node;
	}
	
	public ArrayList<Integer> searchAll(String search_string){
		ArrayList<Integer> results = new ArrayList<Integer>();
		SuffixTrayNodeAbstract current = root_node;
		int search_string_index = 0;
		while(true){
			for(int i=0; i<current.edge_end - current.edge_start; i++){
				if(search_string.charAt(search_string_index) == text.charAt(current.edge_start + i)){
					search_string_index++;
					if(search_string_index == search_string.length()){
						for(int leaf = current.leaf_list_start; leaf<=current.leaf_list_end; leaf++){
							results.add(suffix_tree.leafListIndex_to_nodeId[leaf]-1);
						}
						return results;
					}
				}else{
					return results;
				}
				
			}
			
			if(current.id < 0){ // an interval
				return searchAllInArray(search_string, current.interval_start, current.interval_end, search_string_index);
			}
			current = current.findMatchingChild(alphabet_array[search_string.charAt(search_string_index)]);
				
			if(current == null){
				return results;
			}
			
		}
	}
	
	public int search(String search_string){
		SuffixTrayNodeAbstract current = root_node;
		int search_string_index = 0;
		while(true){
			for(int i=0; i<current.edge_end - current.edge_start; i++){
				if(search_string.charAt(search_string_index) == text.charAt(current.edge_start + i)){
					search_string_index++;
					if(search_string_index == search_string.length()){
						// Da noderne i SuffixTr�et er 1 indekseret, og vi her �nsker det er 0 indekseret
						return suffix_tree.leafListIndex_to_nodeId[current.leaf_list_start]-1; 
					}
				}else{
					return -1;
				}
				
			}
			
			if(current.id < 0){ // an interval
				return searchInArray(search_string, current.interval_start, current.interval_end, search_string_index);
			}
			
//			System.out.println(current.getClass());
			current = current.findMatchingChild(alphabet_array[search_string.charAt(search_string_index)]);
				
			if(current == null){
				return -1;
			}
			
		}
	}
	
	// Bruger find_RW men returnere blot, f�rste gang et total match findes.
	public int searchInArray(String search_string, int L, int R, int start_index){
		int r = 0;
		int M = 0;
		int m = 0;

		// LCP mellem suffix_array indeks 0 og search_string
		int lcp_value_init = start_index;
		while(text.charAt(suffix_array[L] + lcp_value_init) == search_string.charAt(lcp_value_init))
		{
			lcp_value_init++;
			if(lcp_value_init == search_string.length())
				return suffix_array[L];
			if(suffix_array[L] + lcp_value_init == suffix_array.length)
				break;
		}
		int l = lcp_value_init;
		
		if(search_string.charAt(l) <= text.charAt(suffix_array[L]+l))
			return -1;
		
		// LCP mellem suffix_array indeks 0 og search_string // SLUT
		
		if(R>L){
			// LCP mellem suffix_array indeks text.length()-1 og search_string
			lcp_value_init = start_index;
			while(text.charAt(suffix_array[R] + lcp_value_init) == search_string.charAt(lcp_value_init))
			{
				lcp_value_init++;
				if(lcp_value_init ==
						search_string.length())
					return suffix_array[R];
				if(suffix_array[R] + lcp_value_init == suffix_array.length)
					break;
			}
			r = lcp_value_init;
	
			if(search_string.charAt(r) > text.charAt(suffix_array[R]+r))
				return -1;
			
			// LCP mellem suffix_array indeks text.length()-1 og search_string // SLUT
		}

		while(R-L > 1){
			M = (R+L)/2;
			if(l >= r){
				if(lcp_L_array[M] == l){
					int lcp_value = 0;
					if(!(suffix_array[M] + l + lcp_value == suffix_array.length)){
						while(text.charAt(suffix_array[M] + l + lcp_value) == search_string.charAt(l+lcp_value))
						{
							lcp_value++;
							// Hvis der har v�ret |search_string| matches, returneres true
							if(l + lcp_value == search_string.length())
								return suffix_array[M];
							if(suffix_array[M] + l + lcp_value == suffix_array.length)
								break;
						}					
					}
					m = l + lcp_value;
				}else if(lcp_L_array[M] > l){
					m = l;
				}else if(lcp_L_array[M] < l){
					m = lcp_L_array[M];
				}
			}else{
				if(lcp_R_array[M] == r){
					int lcp_value = 0;
					if(!(suffix_array[M] + r + lcp_value == suffix_array.length)){
					while(text.charAt(suffix_array[M] + r + lcp_value) == search_string.charAt(r+lcp_value))
					{
						lcp_value++;
						// Hvis der har v�ret |search_string| matches, returneres true
						if(r + lcp_value == search_string.length())
							return suffix_array[M];
						else if(suffix_array[M] + r + lcp_value == suffix_array.length)
							break;
					}}
					m = r + lcp_value;
				}else if(lcp_R_array[M] > r){
					m = r;
				}else if(lcp_R_array[M] < r){
					m = lcp_R_array[M];
				}
			}
			if(search_string.charAt(m) <= text.charAt(suffix_array[M]+m)){
				R = M;
				r = m;
			}else{
				L = M;
				l = m;
			}
		}
		return -1;
	
	}
	
	private ArrayList<Integer> searchAllInArray(String search_string, int L, int R, int start_index){
		ArrayList<Integer> results = new ArrayList<Integer>();
		
		// LCP mellem suffix_array indeks 0 og search_string
		int lcp_value_l = start_index;
		while(text.charAt(suffix_array[L] + lcp_value_l) == search_string.charAt(lcp_value_l))
		{
			lcp_value_l++;
			if(lcp_value_l == search_string.length()|| suffix_array[L] + lcp_value_l == suffix_array.length)
				break;
		}
		
		// LCP mellem suffix_array indeks text.length()-1 og search_string
		int lcp_value_r = start_index;
		while(text.charAt(suffix_array[R] + lcp_value_r) == search_string.charAt(lcp_value_r))
		{
			lcp_value_r++;
			if(lcp_value_r == search_string.length()|| suffix_array[R] + lcp_value_r == suffix_array.length)
				break;
		}
		
		
		int lw = find_LW(search_string, lcp_value_l, lcp_value_r, L, R);
		int lr = find_RW(search_string, lcp_value_l, lcp_value_r, L, R);

		for(int i = lw; i<=lr; i++){
			results.add(suffix_array[i]);
		}
		
		return results;
	}
	
	// Finder mindste indeks [k] i suffix_array hvor search_text <= suffix[k]
	public int find_LW(String search_string, int l, int r, int L, int R){
	
		if(l == search_string.length() || search_string.charAt(l) <= text.charAt(suffix_array[L]+l))
			return L;
		
	    if(r < search_string.length() && search_string.charAt(r) > text.charAt(suffix_array[R]+r))
			return text.length();


		// Her bliver den bin�re s�gning efter indekset lavet, dette g�res "smart" ved at bruge L_array og R_array til at f� det ned p� O(log n + m) istedet for O(log n * m)
		int M = 0;
		int m = 0;
		while(R-L > 1){
			M = (R+L)/2;
			if(l >= r){
				if(lcp_L_array[M] >= l){
					int lcp_value = 0;
					if(!(l+lcp_value == search_string.length()|| suffix_array[M] + l + lcp_value == suffix_array.length)){
						while(text.charAt(suffix_array[M] + l + lcp_value) == search_string.charAt(l+lcp_value))
						{
							lcp_value++;
							if(l+lcp_value == search_string.length()|| suffix_array[M] + l + lcp_value == suffix_array.length)
								break;
						}					
					}
					m = l + lcp_value;
				}else{
					m = lcp_L_array[M];
				}
			}else{
				if(lcp_R_array[M] >= r){
					int lcp_value = 0;
					if(!(r+lcp_value == search_string.length()|| suffix_array[M] + r + lcp_value == suffix_array.length)){
					while(text.charAt(suffix_array[M] + r + lcp_value) == search_string.charAt(r+lcp_value))
					{
						lcp_value++;
						if(r+lcp_value == search_string.length()|| suffix_array[M] + r + lcp_value == suffix_array.length)
							break;
					}}
					m = r + lcp_value;
				}else{
					m = lcp_R_array[M];
				}
			}
			if(m == search_string.length() || search_string.charAt(m) <= text.charAt(suffix_array[M]+m)){
				R = M;
				r = m;
			}else{
				L = M;
				l = m;
			}
		}
		return R;
		
	}
	
	
	// Finder mindste indeks [k] i suffix_array hvor  suffix[k] <= search_text
	public int find_RW(String search_string, int l, int r, int L, int R){
		
		if(l < search_string.length() && text.charAt(suffix_array[L]+l) > search_string.charAt(l))
			return -1;

		if(r == search_string.length() || text.charAt(suffix_array[R]+r) <= search_string.charAt(r))
			return R;

		
		// Her bliver den bin�re s�gning efter indekset lavet, dette g�res "smart" ved at bruge L_array og R_array til at f� det ned p� O(log n + m) istedet for O(log n * m)
		int M = 0;
		int m = 0;
		while(R-L > 1){
			M = (R+L)/2;
			if(l >= r){
				if(lcp_L_array[M] >= l){
					int lcp_value = 0;
					if(!(l+lcp_value == search_string.length()|| suffix_array[M] + l + lcp_value == suffix_array.length)){
						while(text.charAt(suffix_array[M] + l + lcp_value) == search_string.charAt(l+lcp_value))
						{
							lcp_value++;
							if(l+lcp_value == search_string.length()|| suffix_array[M] + l + lcp_value == suffix_array.length)
								break;
						}					
					}
					m = l + lcp_value;
				}else{
					m = lcp_L_array[M];
				}
			}else{
				if(lcp_R_array[M] >= r){
					int lcp_value = 0;
					if(!(r+lcp_value == search_string.length()|| suffix_array[M] + r + lcp_value == suffix_array.length)){
					while(text.charAt(suffix_array[M] + r + lcp_value) == search_string.charAt(r+lcp_value))
					{
						lcp_value++;
						if(r+lcp_value == search_string.length()|| suffix_array[M] + r + lcp_value == suffix_array.length)
							break;
					}}
					m = r + lcp_value;
				}else{
					m = lcp_R_array[M];
				}
			}
			if(m < search_string.length() && search_string.charAt(m) < text.charAt(suffix_array[M]+m)){
				R = M;
				r = m;
			}else{
				L = M;
				l = m;
			}
		}
		return L;
	
	}
	
	private void initialize_lcp_X_arrays(int L, int R){
		if(R-L > 1){
			int M = (R+L)/2;
			int lcp_value = 0;
			int suffix_pos_L = suffix_array[L];
			int suffix_pos_M = suffix_array[M];
			int suffix_pos_R = suffix_array[R];
			
			// Finder lcp for L og M
			while(text.charAt(suffix_pos_L + lcp_value) == text.charAt(suffix_pos_M + lcp_value))
			{
				lcp_value++;
				// Sikre mod out of bounds, da begge suffixes jo stammer fra "hoved" texten
				if(suffix_pos_L + lcp_value == suffix_array.length || suffix_pos_M + lcp_value == suffix_array.length)
					break;
			}
			lcp_L_array[M] = lcp_value;
			
			// Finder lcp for R og M
			lcp_value = 0;
			while(text.charAt(suffix_pos_R + lcp_value) == text.charAt(suffix_pos_M + lcp_value))
			{
				lcp_value++;
				// Sikre mod out of bounds, da begge suffixes jo stammer fra "hoved" texten
				if(suffix_pos_R + lcp_value == suffix_array.length || suffix_pos_M + lcp_value == suffix_array.length)
					break;
			}
			lcp_R_array[M] = lcp_value;
			
			// Kalder rekursivt for at initializere, de mindre arrays
			initialize_lcp_X_arrays(M, R);
			initialize_lcp_X_arrays(L, M);
		}
	}
	
	public void readInAlphabet(){
		alphabet_array = new int[128]; // Assumes only 7-bit ascii characters are used
		ArrayList<Character> chars = new ArrayList<Character>();
		char[] characters = text.toCharArray();
		int char_index = 0;
		for(char character : characters){
			if(alphabet_array[(int) character] != 1){
				alphabet_array[(int) character] = 1;
				chars.add(character);
			}
		}
//		chars.add('$'); // Is already added, as the text comes from the suffix tree, where $ is added
		Collections.sort(chars);
		for(char c: chars){
			alphabet_array[(int) c] = char_index;
			char_index++;
		}
		
		alphabet_size = chars.size();
		
	}
	
	public void toDot(){
		PrintWriter writer = null;
		try {
			writer = new PrintWriter("SuffixTray.dot", "UTF-8");
		} catch (Exception e) {
			e.printStackTrace();
		}
		writer.println("digraph SuffixTray {");
		Queue<SuffixTrayNodeAbstract> jobQueue = new LinkedList<SuffixTrayNodeAbstract>();
		jobQueue.add(root_node);
		
		if(root_node.id<0)
			writer.println("\"" + root_node + "\"" + ";"); // In case its just a big array
			
		while(!jobQueue.isEmpty()){
			SuffixTrayNodeAbstract node = jobQueue.poll();
			if(node instanceof SuffixTrayBranchingNode){
				for(int i=0; i<((SuffixTrayBranchingNode) node).child_array.length; i++){
					SuffixTrayNodeAbstract child = ((SuffixTrayBranchingNode) node).child_array[i];
					writer.println("\"" + node + "\"" + " -> " + "\"" + child + "\"" + ";");
					jobQueue.add(child);
				}
			}else if(node instanceof SuffixTrayNoneBranchingNode){
				writer.println("\"" + node + "\"" + " -> " + "\"" + ((SuffixTrayNoneBranchingNode) node).left + "\"" + ";");
				writer.println("\"" + node + "\"" + " -> " + "\"" + ((SuffixTrayNoneBranchingNode) node).tray_node + "\"" + " [label=" + ((SuffixTrayNoneBranchingNode) node).seperating_char + "]" + ";");
				writer.println("\"" + node + "\"" + " -> " + "\"" + ((SuffixTrayNoneBranchingNode) node).right + "\"" + ";");
				
				jobQueue.add(((SuffixTrayNoneBranchingNode) node).left);
				jobQueue.add(((SuffixTrayNoneBranchingNode) node).tray_node);
				jobQueue.add(((SuffixTrayNoneBranchingNode) node).right);
			}else{} // interval
		}
		writer.println("}");
		writer.close();
	}
	
	public void print_lcp_X_array(boolean left){
		if(left){
			System.out.println("---- LEFT LCP ----");
			for(int i=0; i<lcp_L_array.length; i++)
				System.out.println("M: " + i + " L:" + lcp_L_array[i]);
		}else{
			System.out.println("---- RIGHT LCP ----");
			for(int i=0; i<lcp_R_array.length; i++)
				System.out.println("M: " + i + " R:" + lcp_R_array[i]);
		}
	}
}
