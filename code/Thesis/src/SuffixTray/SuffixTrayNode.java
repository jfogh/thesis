package SuffixTray;

import java.util.ArrayList;

public interface SuffixTrayNode {
	public SuffixTrayNodeAbstract findMatchingChild(int index);
	public ArrayList<SuffixTrayNodeAbstract> getChildren();
}
