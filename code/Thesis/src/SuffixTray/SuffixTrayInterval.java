package SuffixTray;

import java.util.ArrayList;

public class SuffixTrayInterval extends SuffixTrayNodeAbstract{
	
	public SuffixTrayInterval(int id, int start, int end, int edge_start, int edge_end){
		super(id, edge_start, edge_end, start, end);
		super.interval_start = start;
		super.interval_end = end;
	}
	
	public String toString(){
		return "Interval: " + super.interval_start + "-" + super.interval_end;
	}

	public ArrayList<SuffixTrayNodeAbstract> getChildren(){
		ArrayList<SuffixTrayNodeAbstract> result = new ArrayList<SuffixTrayNodeAbstract>();
		return result;
	}
	
	@Override
	public SuffixTrayNodeAbstract findMatchingChild(int index) { // Not used, as id is used if interval, to search in interval
		return null;
	}
}
