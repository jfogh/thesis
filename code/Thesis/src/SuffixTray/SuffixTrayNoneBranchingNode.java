package SuffixTray;

import java.util.ArrayList;

public class SuffixTrayNoneBranchingNode extends SuffixTrayNodeAbstract{

	public char seperating_char;
	public int seperating_int;
	public SuffixTrayNodeAbstract left;
	public SuffixTrayNodeAbstract right;
	public SuffixTrayNodeAbstract tray_node;
	
	public SuffixTrayNoneBranchingNode(int id, SuffixTrayNodeAbstract non_branching_left, SuffixTrayNodeAbstract tray_node, SuffixTrayNodeAbstract non_branching_right, char sep_char, int seperating_int, int edge_start, int edge_end, int leaf_list_start, int leaf_list_end){
		super(id, edge_start, edge_end, leaf_list_start, leaf_list_end);
		seperating_char = sep_char;
		this.left = non_branching_left;
		this.right = non_branching_right;
		this.tray_node = tray_node;
		this.seperating_int = seperating_int;
	}
	
	public String toString(){
		return "NON-BRANCHING (" + seperating_char + ")" + "(id:" + id + ")";
	}

	public SuffixTrayNodeAbstract findMatchingChild(int index) {
		if(index == seperating_int)
			return tray_node;
		if(index < seperating_int)
			return left;

		return right;
	}
	
	public ArrayList<SuffixTrayNodeAbstract> getChildren(){
		ArrayList<SuffixTrayNodeAbstract> result = new ArrayList<SuffixTrayNodeAbstract>();
		result.add(left);
		result.add(right);
		result.add(tray_node);
		return result;
	}
	
}
