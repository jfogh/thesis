package SuffixTray;

import java.util.ArrayList;
import java.util.Arrays;

public class SuffixTrayBranchingNode extends SuffixTrayNodeAbstract {

	public SuffixTrayNodeAbstract[] child_array;
	
	public SuffixTrayBranchingNode(int id, SuffixTrayNodeAbstract[] child_array, int edge_start, int edge_end, int leaf_list_start, int leaf_list_end){
		super(id, edge_start, edge_end, leaf_list_start, leaf_list_end);
		this.child_array = child_array;
	}
	
	public String printChildArray(){
		String child_array_as_string = "";
		for(int i  = 0; i<child_array.length; i++)
			child_array_as_string += child_array[i] + ", ";
		
		return child_array_as_string;
	}
	
	public SuffixTrayNodeAbstract findMatchingChild(int index){
		return child_array[index];
	}
	
	public ArrayList<SuffixTrayNodeAbstract> getChildren(){
		ArrayList<SuffixTrayNodeAbstract> result = new ArrayList<SuffixTrayNodeAbstract>(Arrays.asList(child_array));
		return result;
	}
	
	public String toString(){
//		return "Branching "  + id + "\n array:" + printChildArray();
		return "Branching "  + id ;
	}
}
