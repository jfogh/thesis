package SuffixTray;

public abstract class SuffixTrayNodeAbstract implements SuffixTrayNode{
	public int id;
	public int interval_start;
	public int interval_end;
	public int edge_start;
	public int edge_end;
	public int leaf_list_start;
	public int leaf_list_end;
	
	public SuffixTrayNodeAbstract(int nodeId, int edge_start, int edge_end, int leaf_list_start, int leaf_list_end){
		id = nodeId;
		this.edge_end = edge_end;
		this.edge_start = edge_start;
		this.leaf_list_end = leaf_list_end;
		this.leaf_list_start = leaf_list_start;
	}
	
}
