import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;
import java.util.Stack;

import SimpleScan.SimpleScan;
import SuffixArray.SuffixArray;
import SuffixTray.*;
import SuffixTreeNodes.NodeTypes.ChildrenStructures;
import SuffixTreeUkkonen.SuffixTree;
import SuffixTreeNodes.SuffixTreeNodeAbstract;


public class CorrectnessModule {
	private static ArrayList<String> files = new ArrayList<String>();
	private static int correct_tests_per_file = 1000;
	private static int wrong_tests_per_file = 500;
	
	private static boolean testScan = true;
	private static boolean testSuffixArray = true;
	private static boolean testSuffixTray = true;
	private static boolean testSuffixTree = true;
	private static ChildrenStructures suffixTreeChildStructure = ChildrenStructures.BINARY;
	
	private static boolean testSearchFunction = true;
	private static boolean testSuffixArrayInvariants = true;
	private static boolean testSuffixTreeInvariants = true;
	private static boolean testSuffixTrayInvariants = true;
	
	public static void main(String[] args) throws IOException{	
		String file_with_filenames = "../../texts/fileNames.txt";
		readInFileNames(file_with_filenames);
		
		for(String file : files){
			String text = readInTextFromFile(file);
			String alphabet = getAlphabetFromText(text);
			test(file, text, alphabet);
		}
	}
	
	public static void test(String filename, String text, String alphabet){
		////////////////   Construction of structures     /////////////////////////
		SimpleScan simple_scan = new SimpleScan(text);
		SuffixTree suffix_tree = new SuffixTree(text, suffixTreeChildStructure);
		SuffixArray suffix_array = new SuffixArray();
		SuffixTray suffix_tray = null;
		
		if(testSuffixArray || testSuffixTray){
			suffix_array.construct_SuffixArray_from_SuffixTree(suffix_tree);
		}
		
		if(testSuffixTray){
			suffix_tray = new SuffixTray(suffix_tree, suffix_array);
		}
		
		/////////////////////////////////////////////////////////////////////////////
		
		if(testSearchFunction)
//			testSearchFunction(filename, text, alphabet, suffix_tree, suffix_array, suffix_tray, simple_scan);
		
		if(testSuffixArrayInvariants)
			testSuffixArayInvariants(suffix_array, filename);
		
		if(testSuffixTreeInvariants)
			testSuffixTreeInvariants(suffix_tree, alphabet, filename);
		
		if(testSuffixTrayInvariants)
			testSuffixTrayInvariants(suffix_tray, alphabet, filename);
		
//		suffix_tree.toDot(true);
//		suffix_tray.toDot();
	}
	
	public static void testSuffixTreeInvariants(SuffixTree suffix_tree, String alphabet, String filename){
		System.out.println("TESTING INVARIANTS ON SUFFIX TREE: " + filename);
		
		int text_length = suffix_tree.text.length();
		int numberOfLeafs = 0;
		int numberOfInnerNodes = 0;
		
		Stack<SuffixTreeNodeAbstract> nodes = new Stack<SuffixTreeNodeAbstract>();
		nodes.push(suffix_tree.root_node);
		
		while(!nodes.isEmpty()){
			SuffixTreeNodeAbstract node = nodes.pop();
			
			///////// TESTING FOR NO MORE THAN N-1 INNER NODES, AND EXACT N LEAFS  (1/2)//////
			if(node.getChildren().size() == 0)
				numberOfLeafs++;
			else
				numberOfInnerNodes++;
			//////////////////////////////////////////////////////////////////////////////////
			
			///////// CHECKS THAT NO NODE HAS MORE THAN |ALPHABET| CHILDREN ////////////
			if(node.getChildren().size() > alphabet.length())
				System.out.println("ERROR IN SUFFIX TREE INVARIANT - TO MANY CHILDREN: " + filename);
			////////////////////////////////////////////////////////////////////////////
			
			nodes.addAll(node.getChildren());
		}
		
		///////// TESTING FOR NO MORE THAN N-1 INNER NODES, AND EXACT N LEAFS (2/2)//////
		if(numberOfLeafs != text_length)
			System.out.println("ERROR IN SUFFIX TREE INVARIANT - INCORRECT NUMBER OF LEAFS: " + filename);
		
		if(numberOfInnerNodes >= text_length)
			System.out.println("ERROR IN SUFFIX TREE INVARIANT - TO MANY INNER NODES: " + filename);
		/////////////////////////////////////////////////////////////////////////////////
	}
	
	public static void testSuffixArayInvariants(SuffixArray suffix_array, String filename){
		System.out.println("TESTING INVARIANTS ON SUFFIX ARRAY: " + filename);
		String text = suffix_array.text;
		int text_length = text.length();
		
		// Checks that suffix at position sa[i+1] is lexicographically larger that the suffix at position sa[i]
		for(int i=0; i<suffix_array.suffix_array.length-1; i++){
			int first = suffix_array.suffix_array[i];
			int second = suffix_array.suffix_array[i+1];
			
			for(int j=0; (first+j)<text_length; j++){
				if(text.charAt(first+j) < text.charAt(second+j)){ break; }
				else if(text.charAt(first+j) > text.charAt(second+j)){
					System.out.println("ERROR IN ARRAY INVARIANT, AT INDEX(" + i + "," + i+1 + ") IN " + filename);
					break;
				}
				if(second+j+1 == text_length){
					System.out.println("ERROR IN ARRAY INVARIANT, AT INDEX(" + i + "," + i+1 + ") IN " + filename);
					break;
				}
			}
		}
		//////////////////////////////////////////////////////////////////////////////////////////////////////////
	}
	
	public static void testSuffixTrayInvariants(SuffixTray suffix_tray, String alphabet, String filename){
		System.out.println("TESTING INVARIANTS ON SUFFIX TRAY: " + filename);
		int text_length = suffix_tray.text.length();
		
		int numberOfBranchingNodes = 0;
		
		Stack<SuffixTrayNodeAbstract> nodes = new Stack<SuffixTrayNodeAbstract>();
		nodes.push(suffix_tray.root_node);
		
		while(!nodes.isEmpty()){
			SuffixTrayNodeAbstract node = nodes.pop();
			if(node == null)
				continue;
			
			if(node instanceof SuffixTrayBranchingNode){
				numberOfBranchingNodes++;
			}
			
			///////////////// ENSURES NO INTERVAL IS LARGER THAN |ALPHABET|^2 /////////////
			if(node.id < 0 && (node.interval_end - node.interval_start + 1) > Math.pow(alphabet.length(), 2)){ // Node is an interval
				System.out.println("ERROR IN TRAY INVARIANT - TO LARGE INTERVAL: " + filename);
			}
			///////////////////////////////////////////////////////////////////////////////
			nodes.addAll(node.getChildren());
		}
		
		////////////// ENSURES THAT NO MORE THAN X BRANCHINGNODES EXISTS //////////
		if(numberOfBranchingNodes > Math.ceil((double)text_length/alphabet.length()))
			System.out.println("ERROR IN TRAY INVARIANT - TO MANY BRANCHING NODES: " + filename);
		///////////////////////////////////////////////////////////////////////////
	}
	
	public static void testSearchFunction(String filename, String text, String alphabet, SuffixTree suffix_tree, SuffixArray suffix_array, SuffixTray suffix_tray, SimpleScan simple_scan){
		System.out.println("TESTING SEARCH FUNCTION ON: " + filename + " WITH ALPHABET: " + alphabet);
		
		Random rnd = new Random();
		/////////////     TEST OF STRINGS THAT ARE SUBSTRINGS OF THE TEXT ///////////
		for(int i=0; i<correct_tests_per_file; i++){
			int start = rnd.nextInt(text.length());
			int end = rnd.nextInt(text.length()-start)+1;
			String search_string = text.substring(start, start+end);
			int search_string_length = search_string.length();
			
			if(testSuffixTree){
				// Da noderne i SuffixTr�et er 1 indekseret, og vi her �nsker det er 0 indekseret
				int search_result = suffix_tree.search(search_string)-1;
				if(search_result<0 || !text.substring(search_result, search_result+search_string_length).equals(search_string))
				System.out.println("Error in tree search (true): START: " + start + " END: " + (start+end) + "");
			}
			if(testSuffixArray){
				int search_result = suffix_array.search(search_string);
				if(search_result<0 || !text.substring(search_result, search_result+search_string_length).equals(search_string))
					System.out.println("Error in array search (true): START: " + start + " END: " + (start+end) + "");
			}
			if(testSuffixTray){
				int search_result = suffix_tray.search(search_string);
				if(search_result<0 || !text.substring(search_result, search_result+search_string_length).equals(search_string))
					System.out.println("Error in tray search (true): START: " + start + " END: " + (start+end) + "");
			}
		}
		/////////////////////////////////////////////////////////////////////////////
		
		/////////////     TEST OF STRINGS THAT ARE NOT SUBSTRINGS OF THE TEXT ///////////
		for(int i=0; i<wrong_tests_per_file; i++){
			int start = rnd.nextInt(text.length());
			int end = rnd.nextInt(text.length()-start)+1;
			String substring = text.substring(start, start+end);
			char extra_char = alphabet.charAt(rnd.nextInt(alphabet.length()));
			String search_string = substring + extra_char;
			
			boolean isSubstring = simple_scan.search(search_string)>=0;
			
			if(!isSubstring){
				if(testSuffixTree){
					if(suffix_tree.search(search_string)>=0)
					System.out.println("Error in tree search (false): START: " + start + " END: " + (start+end) + "");
				}
				if(testSuffixArray){
					if(suffix_array.search(search_string)>=0)
						System.out.println("Error in array search (false): START: " + start + " END: " + (start+end) + "");
				}
				if(testSuffixTray != isSubstring){
					if(suffix_tray.search(search_string)>=0)
						System.out.println("Error in tray search (false): START: " + start + " END: " + (start+end) + "");
				}
			}else{
				i--; // Ensures there actually are performed #wrong_tests_per_file tests with strings that are not substrings.
			}
		}
		/////////////////////////////////////////////////////////////////////////////
		
	}
	
	public static String getAlphabetFromText(String file){
		ArrayList<Character> letters = new ArrayList<Character>();
		
		for(char c : file.toCharArray()){
			if(!letters.contains(c)){
				letters.add(c);
			}
		}
		letters.add('$');
		Collections.sort(letters);
		StringBuilder sb = new StringBuilder();
		for(int i=0; i<letters.size(); i++){
			sb.append(letters.get(i));
		}

		return sb.toString();
	}
	
	public static String readInFileNames(String filename){
		String result = "";
		try{
		  BufferedReader br = new BufferedReader(new FileReader(filename));
		    try {
		        String line = br.readLine();
		        while (line != null) {
		        	if(!line.startsWith("//"))
		        		files.add("../../texts/" + line);
		            line = br.readLine();
		        }
		    } finally {
		        br.close();
		    }
		}
		catch(Exception e){
			System.out.println("EXCEPTION THROWN IN REAN IN FILE NAMES!!!!!!!!!!!!!!!!!!!");
		}
		
		    return result;
	}

	public static String readInTextFromFile(String filename){
		String result = "";
		try{
		  BufferedReader br = new BufferedReader(new FileReader(filename));
		  
		    try {
		        StringBuilder sb = new StringBuilder();
		        String line = br.readLine();

		        while (line != null) {
		        	if(!line.startsWith(">"))
		        		sb.append(line);
		            line = br.readLine();
		        }
		        result = sb.toString();
		    } finally {
		        br.close();
		    }
		}catch(Exception e){
			System.out.println("EXCEPTION THROWN IN READ IN TEXT FROM FILE!!!!!!!!!!!!!!!!!!!");
		}
		
		return result;
	}
}
