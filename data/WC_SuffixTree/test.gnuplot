log2(x) = log(x)/log(2)

if (!exists("directory")) directory='./'

set terminal pdf enhanced font "Helvetica, 10" size 7,4
set output (directory . "WC_SuffixTree.pdf")

set xlabel "n"
set xtics 0, 5000 rotate
set offset 1, 1

set ylabel "Running time [microsecs]"

set ytics nomirror tc lt 1


set xrange [0:100000]
set yrange [0:0.02]

set grid mytics


set key vert left top reverse
set pointsize 2

set datafile separator ";"

plot (directory .  'WorstCase_SuffixTree_A(2)_N(195002)_AVERAGE.csv') using ($3):($4/($3*$2)) title 'withRandom (A2)'  with linespoints ls 2 , \
(directory .  'WorstCase_SuffixTree_A(4)_N(385006)_AVERAGE.csv') using ($3):($4/($3*$2)) title 'withRandom (A4)'  with linespoints ls 4 , \
(directory .  'WorstCase_SuffixTree_A(8)_N(765014)_AVERAGE.csv') using ($3):($4/($3*$2)) title 'withRandom (A8)'  with linespoints ls 5 , \
(directory .  'WorstCase_SuffixTree_A(16)_N(1525030)_AVERAGE.csv') using ($3):($4/($3*$2)) title 'withRandom (A16)'  with linespoints ls 6 , \
(directory .  'WorstCase_SuffixTree_A(32)_N(3045062)_AVERAGE.csv') using ($3):($4/($3*$2)) title 'withRandom (A32)'  with linespoints ls 7 , \
(directory .  'WorstCase_SuffixTree_A(64)_N(6085126)_AVERAGE.csv') using ($3):($4/($3*$2)) title 'withRandom (A64)'  with linespoints ls 9