log2(x) = log(x)/log(2)

if (!exists("directory")) directory='./'

set terminal pdf enhanced font "Helvetica, 10" size 7,4
set output (directory . "AvgTimes.pdf")

set xlabel "m"
set xtics 0, 5000 rotate
set offset 1, 1

set ylabel "Running time [milliseconds]"

set ytics nomirror tc lt 1


set xrange [5000:95000]
set yrange [0:60000]

set grid mytics


set key vert left top reverse
set pointsize 2

set datafile separator ";"

plot (directory .  'WorstCase_SuffixTree_A(2)_N(195002)_Sorted_Exp12Removed_avgANDstdDev.csv') using ($3):($7) title '|{/Symbol S}|=2'  with line 1 , \
(directory .  'WorstCase_SuffixTree_A(4)_N(385006)_Sorted_Exp12Removed_avgANDstdDev.csv') using ($3):($7) title '|{/Symbol S}|=4'  with line 2 , \
(directory .  'WorstCase_SuffixTree_A(8)_N(765014)_Sorted_Exp12Removed_avgANDstdDev.csv') using ($3):($7) title '|{/Symbol S}|=8'  with line 3 , \
(directory .  'WorstCase_SuffixTree_A(16)_N(1525030)_Sorted_Exp12Removed_avgANDstdDev.csv') using ($3):($7) title '|{/Symbol S}|=16'  with line 4 , \
(directory .  'WorstCase_SuffixTree_A(32)_N(3045062)_Sorted_Exp12Removed_avgANDstdDev.csv') using ($3):($7) title '|{/Symbol S}|=32'  with line 5 , \
(directory .  'WorstCase_SuffixTree_A(64)_N(6085126)_Sorted_Exp12Removed_avgANDstdDev.csv') using ($3):($7) title '|{/Symbol S}|=64'  with line 6, 
