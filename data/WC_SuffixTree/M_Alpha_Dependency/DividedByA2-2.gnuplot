log2(x) = log(x)/log(2)

if (!exists("directory")) directory='./'

set termoption dashed
set terminal pdf enhanced font "Helvetica, 10" size 7,4
set output (directory . "DividedByA2-2.pdf")

set xlabel "m"
set xtics 0, 5000 rotate
set offset 1, 1

set ylabel "Running time [milliseconds] relative to |{/Symbol S}|=2"

set ytics nomirror tc lt 1


set xrange [5000:95000]
set yrange [0:10]

set grid mytics


set key vert left top reverse
set pointsize 2

set datafile separator ";"

plot (directory .  'WorstCase_SuffixTree_A(8)_N(765014)_Sorted_Sorted_Exp12Removed_avgANDstdDev_dividedByA(2).csv') using ($3):($9) linetype 2 lc 1 title '|{/Symbol S}|=8' , \
(directory .  'WorstCase_SuffixTree_A(32)_N(3045062)_Sorted_Sorted_Exp12Removed_avgANDstdDev_dividedByA(2).csv') using ($3):($9) lt 3 lc 3  title '|{/Symbol S}|=32'  , \
(directory .  'WorstCase_SuffixTree_A(8)_N(765014)_Sorted_Sorted_Exp12Removed_avgANDstdDev_dividedByA(2).csv') using ($3):(3) title 'y=log(8)'  with line 1, \
(directory .  'WorstCase_SuffixTree_A(32)_N(3045062)_Sorted_Sorted_Exp12Removed_avgANDstdDev_dividedByA(2).csv') using ($3):(5) title 'y=log(32)'  with line 3, 
