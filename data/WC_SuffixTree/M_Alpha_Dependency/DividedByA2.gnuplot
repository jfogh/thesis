log2(x) = log(x)/log(2)

if (!exists("directory")) directory='./'

set termoption dashed
set terminal pdf enhanced font "Helvetica, 10" size 7,4
set output (directory . "DividedByA2.pdf")

set xlabel "m"
set xtics 0, 5000 rotate
set offset 1, 1

set ylabel "Running time [milliseconds] relative to |{/Symbol S}|=2"

set ytics nomirror tc lt 1


set xrange [5000:95000]
set yrange [0:10]

set grid mytics


set key vert left top reverse
set pointsize 2

set datafile separator ";"

plot (directory .  'WorstCase_SuffixTree_A(4)_N(385006)_Sorted_Sorted_Exp12Removed_avgANDstdDev_dividedByA(2).csv') using ($3):($9) linetype 2 lc 1 title '|{/Symbol S}|=4' , \
(directory .  'WorstCase_SuffixTree_A(16)_N(1525030)_Sorted_Sorted_Exp12Removed_avgANDstdDev_dividedByA(2).csv') using ($3):($9) lt 3 lc 3  title '|{/Symbol S}|=16'  , \
(directory .  'WorstCase_SuffixTree_A(64)_N(6085126)_Sorted_Sorted_Exp12Removed_avgANDstdDev_dividedByA(2).csv') using ($3):($9) lt 4 lc 5 title '|{/Symbol S}|=64' , \
(directory .  'WorstCase_SuffixTree_A(64)_N(6085126)_Sorted_Sorted_Exp12Removed_avgANDstdDev_dividedByA(2).csv') using ($3):(2) title 'y=log(4)'  with line 1, \
(directory .  'WorstCase_SuffixTree_A(64)_N(6085126)_Sorted_Sorted_Exp12Removed_avgANDstdDev_dividedByA(2).csv') using ($3):(4) title 'y=log(16)'  with line 3, \
(directory .  'WorstCase_SuffixTree_A(64)_N(6085126)_Sorted_Sorted_Exp12Removed_avgANDstdDev_dividedByA(2).csv') using ($3):(6) title 'y=log(64)'  with line 5, 
