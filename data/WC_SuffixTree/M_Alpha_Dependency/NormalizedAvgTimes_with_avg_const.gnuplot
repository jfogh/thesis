log2(x) = log(x)/log(2)

if (!exists("directory")) directory='./'

set terminal pdf enhanced font "Helvetica, 10" size 7,4
set output (directory . "NormalizedAvgTimes_with_avg_const.pdf")

set xlabel "m"
set xtics 0, 5000 rotate
set offset 1, 1

set ylabel "Running time [milliseconds]/(m*log(|{/Symbol S}|))"

set ytics nomirror tc lt 1 0.025


set xrange [5000:95000]
set yrange [0:0.2]

set grid mytics


set key vert left top reverse
set pointsize 2

set datafile separator ";"

plot (directory .  'Avg_constants_Sorted_Exp12Removed_avgANDstdDev.csv') using ($3):($10) title 'BOOM'  with line 7