log2(x) = log(x)/log(2)

if (!exists("directory")) directory='./'

set terminal pdf enhanced font "Helvetica, 10" size 7,4
set output (directory . "Gradients.pdf")

set xlabel "log(n)"
set xtics 0, 1


set ylabel "Gradient [milliseconds]"

set ytics nomirror tc lt 1 1


set xrange [7:21]
set yrange [45:55]

set grid mytics


set key vert left top reverse
set pointsize 2

set datafile separator ";"

plot (directory . 'fitting_data_to_graph_withAvg.csv') using ($2):($3) title 'Gradients from graph fittings'  with line 1,\
 (directory . 'fitting_data_to_graph_withAvg.csv') using ($2):($6) title 'Average=50.4'  with line 2, \
 (directory . '/Re_run/fitting_data_to_graph.csv') using ($2):($3) title 'Re-run'  with line 3, 