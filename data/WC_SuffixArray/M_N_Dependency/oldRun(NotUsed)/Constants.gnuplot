log2(x) = log(x)/log(2)

if (!exists("directory")) directory='./'

set terminal pdf enhanced font "Helvetica, 10" size 7,4
set output (directory . "Constants.pdf")

set xlabel "log(n)"
set xtics 0, 1


set ylabel "Constant [milliseconds]/log(n)"

set ytics nomirror tc lt 1


set xrange [7:21]
set yrange [150:250]

set grid mytics


set key vert left top reverse
set pointsize 2

set datafile separator ";"

plot (directory . 'fitting_data_to_graph_withAvg.csv') using ($2):($4/$2) title 'Constants from graph fittings divided by log(n)'  with line 1, \
(directory . 'fitting_data_to_graph_withAvg.csv') using ($2):($7) title 'Average=208.4'  with line 2, \
(directory . '/Re_run/fitting_data_to_graph.csv') using ($2):($4/$2) title 'Re-run'  with line 3, 