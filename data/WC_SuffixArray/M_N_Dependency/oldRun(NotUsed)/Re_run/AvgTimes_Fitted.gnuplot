log2(x) = log(x)/log(2)

if (!exists("directory")) directory='./'

set terminal pdf enhanced font "Helvetica, 10" size 7,4
set output (directory . "AvgTimes_fitted.pdf")

set xlabel "m"
set xtics 0, 10 rotate
set offset 1, 1

set ylabel "Running time [milliseconds]"

set ytics nomirror tc lt 1


set xrange [10:200]
set yrange [500:20000]

set grid mytics


set key vert left top reverse
set pointsize 2

set datafile separator ";"

a(x) = a*x + b
b(x) = c*x + d
c(x) = e*x + f
d(x) = g*x + h
e(x) = i*x + j
f(x) = k*x + l
g(x) = m*x + n
h(x) = o*x + p
i(x) = q*x + r
j(x) = s*x + t
k(x) = u*x + v
l(x) = tt*x + y
m(x) = z*x + aa
n(x) = ab*x + ac


fit a(x) directory . 'WorstCase_SuffixArray_N(128)_Sorted_Exp12Removed_avgANDstdDev.csv' using ($3):($7) via a,b
fit b(x) directory . 'WorstCase_SuffixArray_N(256)_Sorted_Exp12Removed_avgANDstdDev.csv' using ($3):($7) via c,d
#plot (directory .  'WorstCase_SuffixArray_N(128)_Sorted_Exp12Removed_avgANDstdDev.csv') using ($3):($7), a(x) , \
#(directory .  'WorstCase_SuffixArray_N(256)_Sorted_Exp12Removed_avgANDstdDev.csv') using ($3):($7), b(x), \
#(directory .  'WorstCase_SuffixArray_N(512)_Sorted_Exp12Removed_avgANDstdDev.csv') using ($3):($7), c(x) , \
#(directory .  'WorstCase_SuffixArray_N(1024)_Sorted_Exp12Removed_avgANDstdDev.csv') using ($3):($7), d(x), \
#(directory .  'WorstCase_SuffixArray_N(2048)_Sorted_Exp12Removed_avgANDstdDev.csv') using ($3):($7), e(x), \
#(directory .  'WorstCase_SuffixArray_N(8192)_Sorted_Exp12Removed_avgANDstdDev.csv') using ($3):($7), g(x) , \
#(directory .  'WorstCase_SuffixArray_N(16384)_Sorted_Exp12Removed_avgANDstdDev.csv') using ($3):($7), h(x), \
#(directory .  'WorstCase_SuffixArray_N(32768)_Sorted_Exp12Removed_avgANDstdDev.csv') using ($3):($7), i(x), \
#(directory .  'WorstCase_SuffixArray_N(65536)_Sorted_Exp12Removed_avgANDstdDev.csv') using ($3):($7), j(x) , \
#(directory .  'WorstCase_SuffixArray_N(131072)_Sorted_Exp12Removed_avgANDstdDev.csv') using ($3):($7), k(x) , \
#(directory .  'WorstCase_SuffixArray_N(262144)_Sorted_Exp12Removed_avgANDstdDev.csv') using ($3):($7), l(x), \
#(directory .  'WorstCase_SuffixArray_N(524288)_Sorted_Exp12Removed_avgANDstdDev.csv') using ($3):($7), m(x), \
#(directory .  'WorstCase_SuffixArray_N(1048576)_Sorted_Exp12Removed_avgANDstdDev.csv') using ($3):($7), n(x)
plot a(x) , \
b(x)