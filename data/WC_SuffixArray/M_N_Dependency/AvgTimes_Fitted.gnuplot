log2(x) = log(x)/log(2)

if (!exists("directory")) directory='./'

set terminal pdf enhanced font "Helvetica, 10" size 7,4
set output (directory . "AvgTimes_fitted.pdf")

set xlabel "m"
set xtics 0, 5 rotate


set ylabel "Running time [milliseconds]"

set ytics nomirror tc lt 1


set xrange [10:200]
set yrange [500:14000]

set grid mytics


set key vert left top reverse
set pointsize 2

set datafile separator ";"

a(x) = a*x + b
b(x) = c*x + d
c(x) = e*x + f
d(x) = g*x + h
e(x) = i*x + j
f(x) = k*x + l
g(x) = m*x + n
h(x) = o*x + p
i(x) = q*x + r
j(x) = s*x + t
k(x) = u*x + v
l(x) = tt*x + y
m(x) = z*x + aa
n(x) = ab*x + ac
o(x) = ad*x + ae


fit a(x) directory . 'WorstCase_SuffixArray_N(128)_Sorted_Exp12Removed_avgANDstdDev.csv' using ($3):($7) via a,b
fit b(x) directory . 'WorstCase_SuffixArray_N(256)_Sorted_Exp12Removed_avgANDstdDev.csv' using ($3):($7) via c,d
fit c(x) directory . 'WorstCase_SuffixArray_N(512)_Sorted_Exp12Removed_avgANDstdDev.csv' using ($3):($7) via e,f
fit d(x) directory . 'WorstCase_SuffixArray_N(1024)_Sorted_Exp12Removed_avgANDstdDev.csv' using ($3):($7) via g,h
fit e(x) directory . 'WorstCase_SuffixArray_N(2048)_Sorted_Exp12Removed_avgANDstdDev.csv' using ($3):($7) via i,j
fit f(x) directory . 'WorstCase_SuffixArray_N(4096)_Sorted_Exp12Removed_avgANDstdDev.csv' using ($3):($7) via k,l
fit g(x) directory . 'WorstCase_SuffixArray_N(8192)_Sorted_Exp12Removed_avgANDstdDev.csv' using ($3):($7) via m,n
fit h(x) directory . 'WorstCase_SuffixArray_N(16384)_Sorted_Exp12Removed_avgANDstdDev.csv' using ($3):($7) via o,p
fit i(x) directory . 'WorstCase_SuffixArray_N(32768)_Sorted_Exp12Removed_avgANDstdDev.csv' using ($3):($7) via q,r
fit j(x) directory . 'WorstCase_SuffixArray_N(65536)_Sorted_Exp12Removed_avgANDstdDev.csv' using ($3):($7) via s,t
fit k(x) directory . 'WorstCase_SuffixArray_N(131072)_Sorted_Exp12Removed_avgANDstdDev.csv' using ($3):($7) via u,v
fit l(x) directory . 'WorstCase_SuffixArray_N(262144)_Sorted_Exp12Removed_avgANDstdDev.csv' using ($3):($7) via tt,y
fit m(x) directory . 'WorstCase_SuffixArray_N(524288)_Sorted_Exp12Removed_avgANDstdDev.csv' using ($3):($7) via z,aa
fit n(x) directory . 'WorstCase_SuffixArray_N(1048576)_Sorted_Exp12Removed_avgANDstdDev.csv' using ($3):($7) via ab,ac
fit o(x) directory . 'WorstCase_SuffixArray_N(2097152)_Sorted_Exp12Removed_avgANDstdDev.csv' using ($3):($7) via ad,ae
#plot (directory .  'WorstCase_SuffixArray_N(128)_Sorted_Exp12Removed_avgANDstdDev.csv') using ($3):($7), a(x) , \
#(directory .  'WorstCase_SuffixArray_N(256)_Sorted_Exp12Removed_avgANDstdDev.csv') using ($3):($7), b(x), \
#(directory .  'WorstCase_SuffixArray_N(512)_Sorted_Exp12Removed_avgANDstdDev.csv') using ($3):($7), c(x) , \
#(directory .  'WorstCase_SuffixArray_N(1024)_Sorted_Exp12Removed_avgANDstdDev.csv') using ($3):($7), d(x), \
#(directory .  'WorstCase_SuffixArray_N(2048)_Sorted_Exp12Removed_avgANDstdDev.csv') using ($3):($7), e(x), \
#(directory .  'WorstCase_SuffixArray_N(8192)_Sorted_Exp12Removed_avgANDstdDev.csv') using ($3):($7), g(x) , \
#(directory .  'WorstCase_SuffixArray_N(16384)_Sorted_Exp12Removed_avgANDstdDev.csv') using ($3):($7), h(x), \
#(directory .  'WorstCase_SuffixArray_N(32768)_Sorted_Exp12Removed_avgANDstdDev.csv') using ($3):($7), i(x), \
#(directory .  'WorstCase_SuffixArray_N(65536)_Sorted_Exp12Removed_avgANDstdDev.csv') using ($3):($7), j(x) , \
#(directory .  'WorstCase_SuffixArray_N(131072)_Sorted_Exp12Removed_avgANDstdDev.csv') using ($3):($7), k(x) , \
#(directory .  'WorstCase_SuffixArray_N(262144)_Sorted_Exp12Removed_avgANDstdDev.csv') using ($3):($7), l(x), \
#(directory .  'WorstCase_SuffixArray_N(524288)_Sorted_Exp12Removed_avgANDstdDev.csv') using ($3):($7), m(x), \
#(directory .  'WorstCase_SuffixArray_N(1048576)_Sorted_Exp12Removed_avgANDstdDev.csv') using ($3):($7), n(x)
plot b(x) title 'n=2^{8}', \
c(x) title 'n=2^{9}' , \
d(x) title 'n=2^{10}', \
e(x) title 'n=2^{11}', \
f(x) title 'n=2^{12}' , \
g(x) title 'n=2^{13}' , \
h(x) title 'n=2^{14}', \
i(x) title 'n=2^{15}', \
j(x) title 'n=2^{16}', \
k(x) title 'n=2^{17}' , \
l(x) title 'n=2^{18}', \
m(x) title 'n=2^{19}', \
n(x) title 'n=2^{20}', \
o(x) title 'n=2^{21}'
#a(x) title 'n=2^{7}' , \