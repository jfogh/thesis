log2(x) = log(x)/log(2)

if (!exists("directory")) directory='./'

set terminal pdf enhanced font "Helvetica, 10" size 7,4
set output (directory . "AvgTimes.pdf")

set xlabel "m"
set xtics 0, 500 rotate


set ylabel "Running time [milliseconds]"

set ytics nomirror tc lt 1


set xrange [500:16000]
set yrange [1000:30000]

set grid mytics


set key vert left top reverse
set pointsize 2

set datafile separator ";"

plot (directory .  'WorstCase_SuffixArray_N(16384)_Sorted_avgANDstdDev.csv') using ($3):($7) title 'n=2^{14}'  with line 8, 
