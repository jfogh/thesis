log2(x) = log(x)/log(2)

if (!exists("directory")) directory='./'

set terminal pdf enhanced font "Helvetica, 10" size 7,4
set output (directory . "AvgTimes.pdf")

set xlabel "m"
set xtics 0, 5 rotate


set ylabel "Running time [milliseconds]"

set ytics nomirror tc lt 1


set xrange [10:200]
set yrange [500:14000]

set grid mytics


set key vert left top reverse
set pointsize 2

set datafile separator ";"

plot (directory .  'WorstCase_SuffixArray_N(256)_Sorted_Exp12Removed_avgANDstdDev.csv') using ($3):($7) title 'n=2^{8}'  with line 2 , \
(directory .  'WorstCase_SuffixArray_N(512)_Sorted_Exp12Removed_avgANDstdDev.csv') using ($3):($7) title 'n=2^{9}'  with line 3 , \
(directory .  'WorstCase_SuffixArray_N(1024)_Sorted_Exp12Removed_avgANDstdDev.csv') using ($3):($7) title 'n=2^{10}'  with line 4, \
(directory .  'WorstCase_SuffixArray_N(2048)_Sorted_Exp12Removed_avgANDstdDev.csv') using ($3):($7) title 'n=2^{11}'  with line 5, \
(directory .  'WorstCase_SuffixArray_N(4096)_Sorted_Exp12Removed_avgANDstdDev.csv') using ($3):($7) title 'n=2^{12}'  with line 6 , \
(directory .  'WorstCase_SuffixArray_N(8192)_Sorted_Exp12Removed_avgANDstdDev.csv') using ($3):($7) title 'n=2^{13}'  with line 7 , \
(directory .  'WorstCase_SuffixArray_N(16384)_Sorted_Exp12Removed_avgANDstdDev.csv') using ($3):($7) title 'n=2^{14}'  with line 8, \
(directory .  'WorstCase_SuffixArray_N(32768)_Sorted_Exp12Removed_avgANDstdDev.csv') using ($3):($7) title 'n=2^{15}'  with line 9, \
(directory .  'WorstCase_SuffixArray_N(65536)_Sorted_Exp12Removed_avgANDstdDev.csv') using ($3):($7) title 'n=2^{16}'  with line 10 , \
(directory .  'WorstCase_SuffixArray_N(131072)_Sorted_Exp12Removed_avgANDstdDev.csv') using ($3):($7) title 'n=2^{17}'  with line 11 , \
(directory .  'WorstCase_SuffixArray_N(262144)_Sorted_Exp12Removed_avgANDstdDev.csv') using ($3):($7) title 'n=2^{18}'  with line 12, \
(directory .  'WorstCase_SuffixArray_N(524288)_Sorted_Exp12Removed_avgANDstdDev.csv') using ($3):($7) title 'n=2^{19}'  with line 13, \
(directory .  'WorstCase_SuffixArray_N(1048576)_Sorted_Exp12Removed_avgANDstdDev.csv') using ($3):($7) title 'n=2^{20}'  with line 14, \
(directory .  'WorstCase_SuffixArray_N(2097152)_Sorted_Exp12Removed_avgANDstdDev.csv') using ($3):($7) title 'n=2^{21}'  with line 15

 #(directory . 'WorstCase_SuffixArray_N(128)_Sorted_Exp12Removed_avgANDstdDev.csv') using ($3):($7) title 'n=2^{7}'  with line 1 , \