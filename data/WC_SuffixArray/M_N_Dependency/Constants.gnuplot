log2(x) = log(x)/log(2)

if (!exists("directory")) directory='./'

set terminal pdf enhanced font "Helvetica, 10" size 7,4
set output (directory . "Constants.pdf")

set xlabel "log(n)"
set xtics 0, 1


set ylabel "Constant [milliseconds]/log(n)"

set ytics nomirror tc lt 1


set xrange [8:21]
set yrange [100:200]

set grid mytics


set key vert left top reverse
set pointsize 2

set datafile separator ";"

plot (directory . 'fitting_data_to_graph.csv') using ($2):($4/$2) title 'Constants from graph fittings divided by log(n)'  with line 1, 