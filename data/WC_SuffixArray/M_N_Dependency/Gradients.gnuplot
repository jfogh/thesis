log2(x) = log(x)/log(2)

if (!exists("directory")) directory='./'

set terminal pdf enhanced font "Helvetica, 10" size 7,4
set output (directory . "Gradients.pdf")

set xlabel "log(n)"
set xtics 0, 1


set ylabel "Gradient [milliseconds]"

set ytics nomirror tc lt 1 1


set xrange [8:21]
set yrange [30:36]

set grid mytics


set key vert left top reverse
set pointsize 2

set datafile separator ";"

plot (directory . 'fitting_data_to_graph.csv') using ($2):($3) title 'Gradients from graph fittings'  with line 1, \
(directory . 'fitting_data_to_graph_withAvg.csv') using ($2):($6) title 'Average = 32.84'  with line 2