log2(x) = log(x)/log(2)

if (!exists("directory")) directory='./'

set terminal pdf enhanced font "Helvetica, 10" size 7,4
set output (directory . "Alpha_Dependency.pdf")

set xlabel "|{/Symbol S}|"
set xtics  2 rotate


set ylabel "Running time [microsecs]"

set ytics nomirror tc lt 1


set xrange [2:64]
set yrange [3500:5500]

set grid mytics


set key vert left top reverse
set pointsize 2

set datafile separator ";"

plot (directory .  'Alpha_Dependency_SuffixArray_Sorted_Exp12Removed_avgANDstdDev.csv') using ($2):($7) title 'Suffix Array'  with linespoints ls 1