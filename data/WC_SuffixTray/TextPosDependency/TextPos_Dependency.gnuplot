log2(x) = log(x)/log(2)

if (!exists("directory")) directory='./'

set terminal pdf enhanced font "Helvetica, 10" size 7,4
set output (directory . "TextPos_Dependency.pdf")

set xlabel "Index of first occurrence in T"
set xtics 0, 10000 rotate
set offset 1, 1

set ylabel "Running time [milliseconds]"

set ytics nomirror tc lt 1


set xrange [0:190000]
set yrange [8000:11000]

set grid mytics


set key vert left top reverse
set pointsize 2

set datafile separator ";"

plot (directory .  'Position_Dependency_BranchingNode_Sorted_Exp12Removed_avgANDstdDev.csv') using ($7):($8) title 'Branching'  with linespoints ls 1, \
(directory .  'Position_Dependency_None_BranchingNode_Sorted_Exp12Removed_avgANDstdDev.csv') using ($7):($8) title 'Non-Branching'  with linespoints ls 2
