log2(x) = log(x)/log(2)

if (!exists("directory")) directory='./'

set terminal pdf enhanced font "Helvetica, 10" size 7,4
set output (directory . "N_Dependency_Non_Branching.pdf")

set xlabel "n"
set xtics 0, 10000 rotate
set offset 1, 1

set ylabel "Running time [milliseconds]"

set ytics nomirror tc lt 1


set xrange [10000:200000]
set yrange [8000:11000]

set grid mytics


set key vert left top reverse
set pointsize 2

set datafile separator ";"

plot (directory .  'N_Dependency_None_BranchingNode_Sorted_Exp12Removed_avgANDstdDev.csv') using ($1):($7) title 'Non-Branching'  with linespoints ls 2,
