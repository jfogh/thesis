log2(x) = log(x)/log(2)

if (!exists("directory")) directory='./'

set terminal pdf enhanced font "Helvetica, 10" size 7,4
set output (directory . "M_Dependency.pdf")

set xlabel "m"
set xtics 0, 50 rotate
set offset 1, 1

set ylabel "Running time [milliseconds]"

set ytics nomirror tc lt 1


set xrange [50:1500]
set yrange [1000:60000]

set grid mytics


set key vert left top reverse
set pointsize 2

set datafile separator ";"

plot (directory .  'M_Dependency_BranchingNode_Sorted_Exp12Removed_avgANDstdDev.csv') using ($3):($7) title 'Branching'  with linespoints ls 1, \
(directory .  'M_Dependency_None_BranchingNode_Sorted_Exp12Removed_avgANDstdDev.csv') using ($3):($7) title 'Non-Branching'  with linespoints ls 2