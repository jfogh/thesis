log2(x) = log(x)/log(2)

if (!exists("directory")) directory='./'

set terminal pdf enhanced font "Helvetica, 10" size 7,4
set output (directory . "M_Dependency_normalized.pdf")

set xlabel "m"
set xtics 0, 50 rotate
set offset 1, 1

set ylabel "Running time / m [milliseconds]"

set ytics nomirror tc lt 1


set xrange [50:1500]
set yrange [0:100]

set grid mytics


set key vert left top reverse
set pointsize 2

set datafile separator ";"

plot (directory .  'M_Dependency_None_BranchingNode_Sorted_Exp12Removed_avgANDstdDev.csv') using ($3):($7/$3) title 'Non-Branching'  with points ls 2, \
(directory .  'M_Dependency_BranchingNode_Sorted_Exp12Removed_avgANDstdDev.csv') using ($3):($7/$3) title 'Branching'  with points ls 1, \
(directory .  'M_Dependency_None_BranchingNode_Sorted_Exp12Removed_avgANDstdDev_withColumnAvg.csv') using ($3):($10) title 'Non-Branching AVG = 35.77'  with line ls 2, \
(directory .  'M_Dependency_BranchingNode_Sorted_Exp12Removed_avgANDstdDev_withColumnAvg.csv') using ($3):($10) title 'Branching AVG = 42.59'  with line ls 1,


