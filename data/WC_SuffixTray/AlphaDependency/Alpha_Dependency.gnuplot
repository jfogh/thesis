log2(x) = log(x)/log(2)

if (!exists("directory")) directory='./'

set terminal pdf enhanced font "Helvetica, 10" size 7,4
set output (directory . "Alpha_Dependency.pdf")

set xlabel "|{/Symbol S}|"
set xtics 0, 2 rotate
set offset 1, 1

set ylabel "Running time [milliseconds]"

set ytics nomirror tc lt 1


set xrange [2:64]
set yrange [8000:11000]

set grid mytics


set key vert left top reverse
set pointsize 2

set datafile separator ";"

plot (directory .  'Alpha_Dependency_BranchingNode_Sorted_Exp12Removed_avgANDstdDev.csv') using ($2):($7) title 'Branching'  with linespoints ls 1, \
(directory .  'Alpha_Dependency_None_BranchingNode_Sorted_Exp12Removed_avgANDstdDev.csv') using ($2):($7) title 'Non-Branching'  with linespoints ls 2
