log2(x) = log(x)/log(2)

if (!exists("directory")) directory='./'

set terminal pdf enhanced font "Helvetica, 10" size 7,4
set output (directory . "plot_sigma_all_tray_18_large.pdf")

set xlabel "m"
set xtics 0, 4000 rotate

set ylabel "Running time [milliseconds]"

set ytics nomirror tc lt 1

set xrange [4000:60000]
set yrange [1000:20000]

set grid mytics


set key vert left top reverse
set pointsize 2

set datafile separator ";"


plot (directory .  '/Bible/Compare2_bible(4000-60000)/FirstPart/AV1611Bible_2pow21.txt_2_compare_262144_Exp12Removed_avgANDstdDev.csv') using ($3):($15) title 'Bib18'  with linespoints ls 1,\
(directory .  '/BibleLowerCase/Compare2_bible_lower(4000-60000)/FirstPart/AV1611Bible_2pow21_lowerCase.txt_2_compare_262144_Exp12Removed_avgANDstdDev.csv') using ($3):($15) title 'BibLow18'  with linespoints ls 2,\
(directory .  '/Genomes/Compare2_gen(4000-60000)/Genome1/genome1.fa_2_compare_262144_Exp12Removed_avgANDstdDev.csv') using ($3):($15) title 'Sgen18'  with linespoints ls 3
