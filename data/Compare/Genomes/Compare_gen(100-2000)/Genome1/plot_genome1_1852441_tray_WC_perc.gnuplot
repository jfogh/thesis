log2(x) = log(x)/log(2)

if (!exists("directory")) directory='./'

set terminal pdf enhanced font "Helvetica, 10" size 7,4
set output (directory . "plot_genome1_1852441_tray_WC_perc.pdf")

set xlabel "m"
set xtics 0, 100 rotate

set ylabel "Running time [milliseconds]"

set ytics nomirror tc lt 1


set xrange [100:2000]
set yrange [20:110]

set grid mytics


set key vert left top reverse
set pointsize 2

set datafile separator ";"


plot (directory .  'genome1.fa_compare_1852441_Exp12Removed_avgANDstdDev.csv') using ($3):($15/((1.06*10**-5*$3+2.86*10**-5*log2(4))*250000)*100) title '% of WC-time'  with linespoints ls 1,\
