log2(x) = log(x)/log(2)

if (!exists("directory")) directory='./'

set terminal pdf enhanced font "Helvetica, 10" size 7,4
set output (directory . "plot_genome1_1852441_tray_WC.pdf")

set xlabel "m"
set xtics 0, 100 rotate

set ylabel "Running time [milliseconds]"

set ytics nomirror tc lt 1


set xrange [100:2000]
set yrange [0:4000]

set grid mytics


set key vert left top reverse
set pointsize 2

set datafile separator ";"


f(x)=(1.06*10**-5*x+2.86*10**-5*log2(4))*250000
plot (directory .  'genome1.fa_compare_1852441_Exp12Removed_avgANDstdDev.csv') using ($3):($15) title 'Actual running time'  with linespoints ls 1,\
f(x)  title 'WC: 1.06*10^{-5}*x+2.86*10^{-5}*log(4)' with linespoints ls 2

