log2(x) = log(x)/log(2)

if (!exists("directory")) directory='./'

set terminal pdf enhanced font "Helvetica, 10" size 7,4
set output (directory . "plot_genome1_all_array.pdf")

set xlabel "m"
set xtics 0, 100 rotate
set offset 1, 1

set ylabel "Running time [milliseconds]"

set ytics nomirror tc lt 1


set xrange [100:2000]
set yrange [0:2500]

set grid mytics


set key vert left top reverse
set pointsize 2

set datafile separator ";"


plot (directory .  'genome1.fa_compare_65536_Exp12Removed_avgANDstdDev.csv') using ($3):($12) title 'Sgen16'  with linespoints ls 1,\
(directory .  'genome1.fa_compare_262144_Exp12Removed_avgANDstdDev.csv') using ($3):($12) title 'Sgen18'  with linespoints ls 2,\
(directory .  'genome1.fa_compare_1852441_Exp12Removed_avgANDstdDev.csv') using ($3):($12) title 'Sgen'  with linespoints ls 3
