log2(x) = log(x)/log(2)

if (!exists("directory")) directory='./'

set terminal pdf enhanced font "Helvetica, 10" size 7,4
set output (directory . "plot_genome2_2211485.pdf")

set xlabel "Genome2 (n=2211485 full length)"
set xtics 0, 100 rotate
set offset 1, 1

set ylabel "Running time [milliseconds]"

set ytics nomirror tc lt 1


set xrange [100:2000]
set yrange [200:2500]

set grid mytics


set key vert left top reverse
set pointsize 2

set datafile separator ";"


plot (directory .  'genome2.fa_compare_2211485_Exp12Removed_avgANDstdDev.csv') using ($3):($9) title 'Tree'  with linespoints ls 1,\
(directory .  'genome2.fa_compare_2211485_Exp12Removed_avgANDstdDev.csv') using ($3):($12) title 'Array'  with linespoints ls 2,\
(directory .  'genome2.fa_compare_2211485_Exp12Removed_avgANDstdDev.csv') using ($3):($15) title 'Tray'  with linespoints ls 3
