log2(x) = log(x)/log(2)

if (!exists("directory")) directory='./'

set terminal pdf enhanced font "Helvetica, 10" size 7,4
set output (directory . "plot_genome1_all_array.pdf")

set xlabel "Genome1-Suffix Array"
set xtics 0, 4000 rotate
set offset 1, 1

set ylabel "Running time [milliseconds]"

set ytics nomirror tc lt 1


set xrange [4000:60000]
set yrange [500:15000]

set grid mytics


set key vert left top reverse
set pointsize 2

set datafile separator ";"


plot (directory .  'genome1.fa_2_compare_65536_Exp12Removed_avgANDstdDev.csv') using ($3):($12) title 'Array (n=2^{16})'  with linespoints ls 1,\
(directory .  'genome1.fa_2_compare_262144_Exp12Removed_avgANDstdDev.csv') using ($3):($12) title 'Array (n=2^{18})'  with linespoints ls 2,\
(directory .  'genome1.fa_2_compare_1852441_Exp12Removed_avgANDstdDev.csv') using ($3):($12) title 'Array (n=1852441)'  with linespoints ls 3
