log2(x) = log(x)/log(2)

if (!exists("directory")) directory='./'

set terminal pdf enhanced font "Helvetica, 10" size 7,4
set output (directory . "plot_genome1_1852441.pdf")

set xlabel "m"
set xtics 0, 4000 rotate
set offset 1, 1

set ylabel "Running time [milliseconds]"

set ytics nomirror tc lt 1


set xrange [4000:60000]
set yrange [0:15000]

set grid mytics


set key vert left top reverse
set pointsize 2

set datafile separator ";"


plot (directory .  'genome1.fa_2_compare_1852441_Exp12Removed_avgANDstdDev.csv') using ($3):($9) title 'Tree'  with linespoints ls 1,\
(directory .  'genome1.fa_2_compare_1852441_Exp12Removed_avgANDstdDev.csv') using ($3):($12) title 'Array'  with linespoints ls 2,\
(directory .  'genome1.fa_2_compare_1852441_Exp12Removed_avgANDstdDev.csv') using ($3):($15) title 'Tray'  with linespoints ls 3
