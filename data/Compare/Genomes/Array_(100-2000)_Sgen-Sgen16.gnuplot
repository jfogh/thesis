log2(x) = log(x)/log(2)

if (!exists("directory")) directory='./'

set terminal pdf enhanced font "Helvetica, 10" size 7,4
set output (directory . "Array_(100-2000)_Sgen-Sgen16.pdf")

set xlabel "m"
set xtics 0, 100 rotate
set offset 1, 1

set ylabel "Running time [milliseconds]"

set ytics nomirror tc lt 1


set xrange [100:2000]
set yrange [0:2500]

set grid mytics


set key vert left top reverse
set pointsize 2

set datafile separator ";"


plot (directory .  'NotIn_Compare_gen(100-2000)/Genome1/genome1.fa_compare_NotIn_1852441_Exp12Removed_avgANDstdDev.csv') using ($3):($8) title 'Sgen (Negative queries)'  with linespoints ls 1,\
(directory .  'Compare_gen(100-2000)/Genome1/genome1.fa_compare_1852441_Exp12Removed_avgANDstdDev.csv') using ($3):($12) title 'Sgen (Positive queries)'  with linespoints ls 2,\
(directory .  'NotIn_Compare_gen(100-2000)/Genome1/genome1.fa_compare_NotIn_65536_Exp12Removed_avgANDstdDev.csv') using ($3):($8) title 'Sgen16 (Negative queries)'  with linespoints ls 3,\
(directory .  'Compare_gen(100-2000)/Genome1/genome1.fa_compare_65536_Exp12Removed_avgANDstdDev.csv') using ($3):($12) title 'Sgen16 (Positive queries)'  with linespoints ls 4
