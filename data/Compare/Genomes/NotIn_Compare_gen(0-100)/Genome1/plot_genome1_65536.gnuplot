log2(x) = log(x)/log(2)

if (!exists("directory")) directory='./'

set terminal pdf enhanced font "Helvetica, 10" size 7,4
set output (directory . "plot_genome1_65536.pdf")

set xlabel "m"
set xtics 0, 5 rotate


set ylabel "Running time [milliseconds]"

set ytics nomirror tc lt 1


set xrange [10:100]
set yrange [0:1500]

set grid mytics


set key vert left top reverse
set pointsize 2

set datafile separator ";"


plot (directory .  'genome1.fa_0_compare_NotIn_65536_Exp12Removed_avgANDstdDev.csv') using ($3):($8) title 'Array'  with linespoints ls 2,\
(directory .  'genome1.fa_0_compare_NotIn_65536_Exp12Removed_avgANDstdDev.csv') using ($3):($11) title 'Tray'  with linespoints ls 3
