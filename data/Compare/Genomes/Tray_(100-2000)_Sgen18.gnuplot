log2(x) = log(x)/log(2)

if (!exists("directory")) directory='./'

set terminal pdf enhanced font "Helvetica, 10" size 7,4
set output (directory . "Tray_(100-2000)_Sgen18.pdf")

set xlabel "m"
set xtics 0, 100 rotate
set offset 1, 1

set ylabel "Running time [milliseconds]"

set ytics nomirror tc lt 1


set xrange [100:2000]
set yrange [0:2500]

set grid mytics


set key vert left top reverse
set pointsize 2

set datafile separator ";"


plot (directory .  'NotIn_Compare_gen(100-2000)/Genome1/genome1.fa_compare_NotIn_262144_Exp12Removed_avgANDstdDev.csv') using ($3):($11) title 'Sgen18 (Negative queries)'  with linespoints ls 3, \
(directory .  'Compare_gen(100-2000)/Genome1/genome1.fa_compare_262144_Exp12Removed_avgANDstdDev.csv') using ($3):($15) title 'Sgen18 (Positive queries)'  with linespoints ls 6
