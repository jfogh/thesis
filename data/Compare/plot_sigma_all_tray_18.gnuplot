log2(x) = log(x)/log(2)

if (!exists("directory")) directory='./'

set terminal pdf enhanced font "Helvetica, 10" size 7,4
set output (directory . "plot_sigma_all_tray_18.pdf")

set xlabel "m"
set xtics 0, 100 rotate

set ylabel "Running time [milliseconds]"

set ytics nomirror tc lt 1

set xrange [100:2000]
set yrange [0:2000]

set grid mytics


set key vert left top reverse
set pointsize 2

set datafile separator ";"


plot (directory .  '/Bible/Compare_bible(100-2000)/FirstPart/AV1611Bible_2pow21.txt_compare_262144_Exp12Removed_avgANDstdDev.csv') using ($3):($15) title 'Bib18'  with linespoints ls 1,\
(directory .  '/BibleLowerCase/Compare_bible_lower(100-2000)/FirstPart/AV1611Bible_2pow21_lowerCase.txt_compare_262144_Exp12Removed_avgANDstdDev.csv') using ($3):($15) title 'BibLow18'  with linespoints ls 2,\
(directory .  '/Genomes/Compare_gen(100-2000)/Genome1/genome1.fa_compare_262144_Exp12Removed_avgANDstdDev.csv') using ($3):($15) title 'Sgen18'  with linespoints ls 3
