log2(x) = log(x)/log(2)

if (!exists("directory")) directory='./'

set terminal pdf enhanced font "Helvetica, 10" size 7,4
set output (directory . "plot_bibleLowerCase_65536.pdf")

set xlabel "m"
set xtics 0, 100 rotate
set offset 1, 1

set ylabel "Running time [milliseconds]"

set ytics nomirror tc lt 1


set xrange [100:2000]
set yrange [0:2500]

set grid mytics


set key vert left top reverse
set pointsize 2

set datafile separator ";"


plot (directory .  'AV1611Bible_2pow21_lowerCase.txt_compare_NotIn_65536_Exp12Removed_avgANDstdDev.csv') using ($3):($8) title 'Array'  with linespoints ls 2,\
(directory .  'AV1611Bible_2pow21_lowerCase.txt_compare_NotIn_65536_Exp12Removed_avgANDstdDev.csv') using ($3):($11) title 'Tray'  with linespoints ls 3

