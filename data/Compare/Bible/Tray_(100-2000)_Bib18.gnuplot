log2(x) = log(x)/log(2)

if (!exists("directory")) directory='./'

set terminal pdf enhanced font "Helvetica, 10" size 7,4
set output (directory . "Tray_(100-2000)_Bib18.pdf")

set xlabel "m"
set xtics 0, 100 rotate


set ylabel "Running time [milliseconds]"

set ytics nomirror tc lt 1


set xrange [100:2000]
set yrange [0:2500]

set grid mytics


set key vert left top reverse
set pointsize 2

set datafile separator ";"


plot (directory .  'NotIn_Compare_bible(100-2000)/FirstPart/AV1611Bible_2pow21.txt_compare_NotIn_262144_Exp12Removed_avgANDstdDev.csv') using ($3):($11) title 'Bib18 (Negative queries)'  with linespoints ls 3,\
(directory .  'Compare_bible(100-2000)/FirstPart/AV1611Bible_2pow21.txt_compare_262144_Exp12Removed_avgANDstdDev.csv') using ($3):($15) title 'Bib18 (Positive queries)'  with linespoints ls 6,\
