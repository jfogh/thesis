log2(x) = log(x)/log(2)

if (!exists("directory")) directory='./'

set terminal pdf enhanced font "Helvetica, 10" size 7,4
set output (directory . "Tray_(4000-60000)_Bib18.pdf")

set xlabel "m"
set xtics 0, 4000 rotate


set ylabel "Running time [milliseconds]"

set ytics nomirror tc lt 1


set xrange [4000:60000]
set yrange [0:20000]

set grid mytics


set key vert left top reverse
set pointsize 2

set datafile separator ";"


plot (directory .  'NotIn_Compare_bible(4000-60000)/FirstPart/AV1611Bible_2pow21.txt_2_compare_NotIn_262144_Exp12Removed_avgANDstdDev.csv') using ($3):($11) title 'Tray (NotIn)'  with linespoints ls 2,\
(directory .  'Compare2_bible(4000-60000)/FirstPart/AV1611Bible_2pow21.txt_2_compare_262144_Exp12Removed_avgANDstdDev.csv') using ($3):($15) title 'Tray'  with linespoints ls 5,\
